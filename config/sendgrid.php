<?php

return
    [
        'templates' =>
            [
                'confirm_email' => 'd-1c09bec2c1074180872b7383159c63a4',
                'password_reset_request' => 'd-b22918add3bd4a8687cecc01e84c4ade',
                'password_reset_success' => 'd-0cd2ac4901c44aefa9e8c7233003d52c',
                'request_product_measure' => 'd-612eb4c5edb74ea4942e9c48ea88f6f3'
            ]

    ];
