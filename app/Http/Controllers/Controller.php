<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use OpenApi\Annotations\Info;
use OpenApi\Annotations\OpenApi;
use OpenApi\Annotations\Response;
use OpenApi\Annotations\SecurityScheme;
use OpenApi\Annotations\Server;
use OpenApi\Annotations\Tag;

/**
 * @Info(
 *   title="Dezigned API",
 *   version="1.0"
 * )
 * @OpenApi(
 *
 * )
 * @Tag(
 *     name="Auth",
 *     description="Authorization and password management endpoints"
 * )
 * @Tag(
 *     name="Client",
 *     description="Client management endpoints"
 * )
 * @Tag(
 *     name="Product",
 *     description="Product management endpoints"
 * )
 *
 * @Tag(
 *     name="Project",
 *     description="Project management endpoints"
 * )
 * @Tag(
 *     name="Project Gallery",
 *     description="User certifications management endpoints"
 * )
 * @Tag(
 *     name="Project area",
 *     description="Project area management endpoints"
 * )
 *
 * @Tag(
 *     name="Project area gallery",
 *     description="Project area gallery management endpoints"
 * )
 *
 * @Tag(
 *     name="Project area notes",
 *     description="Project area notes management endpoints"
 * )
 *
 * @Tag(
 *     name="Project area products",
 *     description="Project area product management endpoints"
 * )
 *
 * @Tag(
 *     name="Project area product notes",
 *     description="Project area product notes management endpoints"
 * )
 *
 * @Tag(
 *     name="Project area product measure",
 *     description="Project area product measurement management endpoints"
 * )
 * @Tag(
 *     name="Project area product measure notes",
 *     description="Project area product measurement notes management endpoints"
 * )
 *
 * @Tag(
 *     name="Project area product measure dates",
 *     description="Project area product measurement dates management endpoints"
 * )
 *
 * @Tag(
 *     name="User",
 *     description="User management endpoints"
 * )
 * @Tag(
 *     name="User Certificates",
 *     description="User certifications management endpoints"
 * )
 *
 * @Tag(
 *     name="User Experience",
 *     description="User experience management endpoints"
 * )
 * @Tag(
 *     name="UserGallery",
 *     description="User gallery management endpoints"
 * )
 *
 * @Tag(
 *     name="User Mission Statement",
 *     description="User Mission Statement management endpoints"
 * )
 *
 * @Server(
 *     url=L5_SWAGGER_CONST_HOST,
 *     description="API server",
 * )
 *
 * @SecurityScheme(
 *     securityScheme="Auth",
 *     type="apiKey",
 *     in="header",
 *     name="Authorization"
 *
 * )
 */
class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
}
