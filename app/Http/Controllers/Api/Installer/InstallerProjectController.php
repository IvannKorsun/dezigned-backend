<?php

namespace App\Http\Controllers\Api\Installer;

use App\Http\Requests\Installer\UpdateInstallerProjectAndDateRequest;
use App\Http\Requests\Installer\InstallerProjectListRequest;
use App\Http\Controllers\Controller;
use App\Services\Installer\InstallerProjectsService;
use App\Services\Project\ProductMeasureDatesService;
use OpenApi\Annotations\Get;
use OpenApi\Annotations\JsonContent;
use OpenApi\Annotations\MediaType;
use OpenApi\Annotations\Parameter;
use OpenApi\Annotations\Property;
use OpenApi\Annotations\Put;
use OpenApi\Annotations\RequestBody;
use OpenApi\Annotations\Response;
use OpenApi\Annotations\Schema;

class InstallerProjectController extends Controller
{
    /**
     * @var InstallerProjectsService
     * @var ProductMeasureDatesService
     */
    private $installerProjectService;
    private $dateService;

    /**
     * InstallerProjectController constructor.
     *
     * @param \App\Services\Installer\InstallerProjectsService $installerProjectService
     * @param \App\Services\Project\ProductMeasureDatesService $dateService
     */
    public function __construct(InstallerProjectsService $installerProjectService, ProductMeasureDatesService $dateService)
    {
        $this->dateService = $dateService;
        $this->installerProjectService = $installerProjectService;
    }

    /**
     * @Get(
     *     path="/installer-projects",
     *     description="Get list of installer projects",
     *     tags={"Installer Project"},
     *     security={{"Auth": {}}},
     *     @Parameter(
     *          name="sorting",
     *          in="query",
     *          description="Sorting by (status),(ASC),(DESC)",
     *          example="ASC",
     *          required=false,
     *          allowEmptyValue=true,
     *          @Schema(type="string")
     *     ),
     *     @Parameter(
     *          name="page",
     *          in="query",
     *          description="Number of projects page",
     *          example="1",
     *          required=false,
     *          allowEmptyValue=true,
     *          @Schema(type="int")
     *     ),
     *     @Parameter(
     *          name="limit",
     *          in="query",
     *          description="Number of projects per page",
     *          example=16,
     *          required=false,
     *          allowEmptyValue=true,
     *          @Schema(type="int")
     *     ),
     *     @Response(response="403", description="User does not have the right roles."),
     *     @Response(
     *          response="200",
     *          description="Example project list response",
     *          @JsonContent(
     *                  allOf={
     *                      @Schema(
     *                          title="data",
     *                          @Property(type="array", property="data", @\OpenApi\Annotations\Items(ref="#/components/schemas/InstallerProjectListEntryResponse")),
     *                      ),
     *                      @Schema(
     *                          title="total",
     *                          @Property(title="total", property="total", type="integer", example="10")
     *                      )
     *                  }
     *          )
     *     ),
     *     @Response(response="401", description="Error: Unauthorized"),
     *     @Response(response="404", description="User not found."),
     *     @Response(response="500", description="Internal Server Error.")
     * )
     * @param \App\Http\Requests\Installer\InstallerProjectListRequest $installerProjectListRequest
     *
     * @return \Illuminate\Http\JsonResponse
     */

    public function index(InstallerProjectListRequest $installerProjectListRequest)
    {
        return response()->json($this->installerProjectService->all($installerProjectListRequest));
    }

    /**
     * @Get(
     *     path="/installer-projects/{id}",
     *     description="Get one installer project details by project id",
     *     tags={"Installer Project"},
     *     security={{"Auth": {}}},
     *     @Parameter(
     *          name="id",
     *          in="path",
     *          description="Get installer project details by id",
     *          example=1,
     *          required=true,
     *          allowEmptyValue=false,
     *          @Schema(type="integer")
     *     ),
     *     @Response(
     *          response="200",
     *          description="Installer Project details by project id",
     *          @JsonContent(ref="#/components/schemas/InstallerProjectResponse")
     *     ),
     *     @Response(response="401", description="Error: Unauthorized"),
     *     @Response(response="404", description="Project not found."),
     * )
     */

    public function show($id)
    {
        $installerProject = $this->installerProjectService->getInstallerProject($id);
        return $installerProject ?
            response()->json($installerProject):
            response()->json(['message' => 'Not found'], 404);
    }

    /**
     * @Put(
     *     path="/installer-projects/{id}",
     *     description="Update installer project by id",
     *     tags={"Installer Project"},
     *     security={{"Auth": {}}},
     *     @Parameter(
     *          name="id",
     *          in="path",
     *          description="Update installer project by id",
     *          example=1,
     *          required=true,
     *          allowEmptyValue=false,
     *          @Schema(type="integer")
     *     ),
     *     @RequestBody(
     *          @MediaType(
     *              mediaType="application/json",
     *              @Schema(ref="#/components/schemas/InstallerProjectUpdateRequest")
     *          )
     *     ),
     *     @Response(response="401", description="Error: Unauthorized"),
     *     @Response(response="404", description="Project not found."),
     *     @Response(response="500", description="Internal Server Error.")
     * )
     * @param \App\Http\Requests\Installer\UpdateInstallerProjectAndDateRequest $request
     * @param                                                            $id
     *
     * @return \Illuminate\Http\JsonResponse
     */

    public function update(UpdateInstallerProjectAndDateRequest $request, $id)
    {
        $updatedInstallerProject = $this->installerProjectService->updateInstallerProject($request, $id);
        $updatedDate = $this->dateService->updateDate($request,$request->get('date_id'));

        return $updatedInstallerProject && $updatedDate ?
            response()->json(['message'=> 'Accepted'], 200):
            response()->json(['message' => 'Internal Server Error.'], 500);
    }
}
