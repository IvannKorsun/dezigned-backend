<?php

namespace App\Http\Controllers\Api\User;

use App\Http\Controllers\Controller;

use App\Http\Requests\User\PostMissionStatementRequest;
use App\Http\Resources\User\MissionStatementResource;
use App\Models\User\MissionStatement;
use Illuminate\Support\Facades\Auth;
use OpenApi\Annotations\Get;
use OpenApi\Annotations\JsonContent;
use OpenApi\Annotations\Post;
use OpenApi\Annotations\Property;
use OpenApi\Annotations\Put;
use OpenApi\Annotations\RequestBody;
use OpenApi\Annotations\Response;
use OpenApi\Annotations\Schema;

class MissionStatementController extends Controller
{

    /**
     * @Get(
     *     path="/user/mission-statement",
     *     description="Get user mission statement",
     *     tags={"User Mission Statement"},
     *     security={{"Auth": {}}},
     *     @Response(
     *          response="200",
     *          description="Example user mission statement response",
     *          @JsonContent(
     *              allOf={
     *                  @Schema(title="title", @Property(property="title", type="string", example="Title")),
     *                  @Schema(title="description", @Property(property="description", title="description", type="string", example="Description"))
     *              }
     *          )
     *     ),
     *     @Response(response="401", description="Error: Unauthorized"),
     *     @Response(response="500", description="Internal Server Error.")
     * )
     */
    public function index()
    {
        $missionStatement = $missionStatement = MissionStatement::where('user_id', Auth::id())->first();

        return $missionStatement
            ? response()->json(MissionStatementResource::make($missionStatement), 200)
            : response()->json([], 200);
    }

    /**
     * @Post(
     *     path="/user/mission-statement",
     *     description="Update user mission statement",
     *     tags={"User Mission Statement"},
     *     security={{"Auth": {}}},
     *     @RequestBody(
     *         description="Update gallery image",
     *          @JsonContent(
     *              allOf={
     *                  @Schema(title="title", @Property(property="title", type="string", example="Title")),
     *                  @Schema(title="description", @Property(property="description", title="description", type="string", example="Description"))
     *              }
     *          )
     *     ),
     *     @Response(
     *          response="200",
     *          description="Example user mission statement response",
     *          @JsonContent(
     *              allOf={
     *                  @Schema(title="title", @Property(property="title", type="string", example="Title")),
     *                  @Schema(title="description", @Property(property="description", title="description", type="string", example="Description"))
     *              }
     *          )
     *     ),
     *     @Response(response=422, description="Error: Unprocessable Entity"),
     *     @Response(response=401, description="Error: Unauthorized")
     * )
     */
    public function update(PostMissionStatementRequest $request)
    {
        $missionStatement = MissionStatement::updateOrCreate(['user_id' => $request->user()->id], $request->all());

        return response()->json(MissionStatementResource::make($missionStatement), 200);
    }

}
