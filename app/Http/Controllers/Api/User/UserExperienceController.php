<?php

namespace App\Http\Controllers\Api\User;

use App\Http\Requests\User\AddExperienceRequest;
use App\Http\Requests\User\UpdateExperienceRequest;
use App\Services\User\UsersExperienceService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use OpenApi\Annotations\Delete;
use OpenApi\Annotations\Get;
use OpenApi\Annotations\Items;
use OpenApi\Annotations\JsonContent;
use OpenApi\Annotations\Parameter;
use OpenApi\Annotations\Post;
use OpenApi\Annotations\Property;
use OpenApi\Annotations\Put;
use OpenApi\Annotations\RequestBody;
use OpenApi\Annotations\Response;
use OpenApi\Annotations\Schema;

class UserExperienceController extends Controller
{
    /**
     * @var UsersExperienceService
     */
    private $usersExperienceService;

    public function __construct(UsersExperienceService $usersExperienceService)
    {
        $this->usersExperienceService = $usersExperienceService;
    }

    /**
     * @Get(
     *     path="/user/{userId?}/experience",
     *     description="Get list of user experience",
     *     tags={"User Experience"},
     *     security={{"Auth": {}}},
     *     @Parameter(
     *          name="userId",
     *          in="path",
     *          description="Get experience by user (installer) id",
     *          example=1,
     *          required=false,
     *          allowEmptyValue=true,
     *          @Schema(type="integer")
     *     ),
     *     @Response(
     *          response="200",
     *          description="Example list of user experience",
     *         @JsonContent(@Property(property="data", type="array", @Items(ref="#/components/schemas/UserExperienceResponse")))
     *     ),
     *     @Response(response="401", description="Error: Unauthorized"),
     *     @Response(response="404", description="Not found."),
     *     @Response(response="500", description="Internal Server Error.")
     * )
     */
    public function index(Request $request)
    {
        $experience = $this->usersExperienceService->getExperienceList($request);

        return $experience ? response()->json($experience) : response()->json(['message' => 'Not found'], 404);
    }

    /**
     * @Post(
     *     path="/user/experience",
     *     description="Add new user experience",
     *     tags={"User Experience"},
     *     security={{"Auth": {}}},
     *     @RequestBody(
     *         description="Add new user experience",
     *       @JsonContent(ref="#/components/schemas/AddOrUpdateUserExperienceRequest")
     *     ),
     *     @Response(
     *          response="201",
     *          description="Example of user experience create response",
     *          @JsonContent(ref="#/components/schemas/UserExperienceResponse")
     *     ),
     *     @Response(response=422, description="Error: Unprocessable Entity"),
     *     @Response(response=401, description="Error: Unauthorized")
     * )
     */
    public function store(AddExperienceRequest $request)
    {
        return response()->json($this->usersExperienceService->addExperience($request));
    }

    /**
     * @Put(
     *     path="/user/experience/{experienceId}",
     *     description="Update user experience",
     *     tags={"User Experience"},
     *     security={{"Auth": {}}},
     *     @Parameter(
     *          name="experienceId",
     *          in="path",
     *          description="Update experience by user id",
     *          example=1,
     *          required=true,
     *          allowEmptyValue=false,
     *          @Schema(type="integer")
     *     ),
     *     @RequestBody(
     *         description="Update user experience",
     *       @JsonContent(ref="#/components/schemas/AddOrUpdateUserExperienceRequest")
     *     ),
     *     @Response(
     *          response="200",
     *          description="Example of user experience create response",
     *          @JsonContent(ref="#/components/schemas/UserExperienceResponse")
     *     ),
     *     @Response(response=422, description="Error: Unprocessable Entity"),
     *     @Response(response=401, description="Error: Unauthorized")
     * )
     */
    public function update(UpdateExperienceRequest $request, int $id)
    {
        $updated = $this->usersExperienceService->updateExperience($request, $id);

        return $updated ? response()->json($updated) : response()->json(['message' => 'Not found'], 404);
    }

    /**
     * @Delete(
     *     path="/user/experience/{experienceId}",
     *     description="Delete images",
     *     tags={"User Experience"},
     *     security={{"Auth": {}}},
     *     @Parameter(
     *          name="experienceId",
     *          in="path",
     *          description="Delete experience by id",
     *          example=1,
     *          required=true,
     *          allowEmptyValue=false,
     *          @Schema(type="integer")
     *     ),
     *     @Response(response=204, description="No content"),
     *     @Response(response=401, description="Error: Unauthorized")
     * )
     */
    public function destroy(int $id)
    {
        $deleted = $this->usersExperienceService->delete($id);

        return $deleted ? response()->json([], 204) : response()->json(['message' => 'Not found'], 404);
    }
}
