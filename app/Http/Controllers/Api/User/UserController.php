<?php

namespace App\Http\Controllers\Api\User;

use App\Http\Requests\Password\PasswordChangeRequest;
use App\Http\Requests\User\UserUpdateRequest;
use App\Http\Resources\User\UserResource;
use App\Models\User;
use App\Http\Controllers\Controller;
use App\Services\UserService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use OpenApi\Annotations\Get;
use OpenApi\Annotations\JsonContent;
use OpenApi\Annotations\MediaType;
use OpenApi\Annotations\Parameter;
use OpenApi\Annotations\Post;
use OpenApi\Annotations\Property;
use OpenApi\Annotations\Put;
use OpenApi\Annotations\RequestBody;
use OpenApi\Annotations\Response;
use OpenApi\Annotations\Schema;

class UserController extends Controller
{
    /**
     * @var UserService
     */
    private $userService;

    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    /**
     * @Get(
     *     path="/user",
     *     description="Get authorized user details or by user id",
     *     tags={"User"},
     *     security={{"Auth": {}}},
     *     @Response(
     *          response="200",
     *          description="Authorized user details or by user id",
     *          @JsonContent(ref="#/components/schemas/UserResponse")
     *     ),
     *     @Response(response="401", description="Error: Unauthorized"),
     *     @Response(response="404", description="User not found."),
     *     @Response(response="500", description="Internal Server Error.")
     * )
     */
    public function profile()
    {
        return response()->json(UserResource::make(Auth::user()));
    }

    /**
     * @Put(
     *     path="/user",
     *     description="Get authorized user details or by user id",
     *     tags={"User"},
     *     security={{"Auth": {}}},
     *     @RequestBody(
     *          @MediaType(
     *              mediaType="application/json",
     *              @Schema(ref="#/components/schemas/UserProfileUpdateRequest")
     *          )
     *     ),
     *     @Response(
     *          response="200",
     *          description="Authorized user details or by user id",
     *          @JsonContent(ref="#/components/schemas/UserResponse")
     *     ),
     *     @Response(response="401", description="Error: Unauthorized"),
     *     @Response(response="404", description="User not found."),
     *     @Response(response="500", description="Internal Server Error.")
     * )
     */
    public function update(UserUpdateRequest $request)
    {
        $user = $this->userService->updateProfile($request);

        return $user
            ? response()->json($user)
            : response()->json(['message' => 'User not found.'], 404);
    }
    /**
     * @Post(
     *     path="/user/change-password",
     *     description="Change user password",
     *     tags={"User"},
     *     security={{"Auth": {}}},
     *     @RequestBody(
     *         description="Change current user password",
     *          @MediaType(
     *              mediaType="application/json",
     *              @JsonContent(ref="#/components/schemas/PasswordChangeRequest")
     *          )
     *     ),
     *     @Response(response=200, description="Password changed", @JsonContent()),
     *     @Response(response=422, description="Error: Unprocessable Entity"),
     *     @Response(response=401, description="Error: Unauthorized")
     * )
     */
    public function changePassword(PasswordChangeRequest $request)
    {
        if (!$this->userService->changePassword(Auth::user(), $request)){
            return response()->json(['message' => 'Wrong current password!'], 422);
        }
        return response()->json(['message' => 'Password changed'], 200);
    }
    /**
     * @Get(
     *     path="/user/profile/share",
     *     description="Create token for sharing profile",
     *     tags={"User"},
     *     security={{"Auth": {}}},
     *     @Response(
     *          response="201",
     *          description="Created token response",
     *          @JsonContent(
     *              @Property(
     *                  property="token",
     *                  type="string",
     *                  example="uwPw5Xh5iN7nQlLq5H7yw6sPjBAj2IS..."
     *              )
     *          )
     *     )
     * )
     */
    public function share()
    {
        return response()->json(['token' => $this->userService->createSharedProfile()], 201);
    }

    /**
     * @Get(
     *     path="/user/profile/public/{token}",
     *     description="Get user shared profile",
     *     tags={"User"},
     *     @Parameter(
     *          name="token",
     *          in="path",
     *          description="Token for get user profile",
     *          example="uwPw5Xh5iN7nQlLq5H7yw6sPjBAj2IS...",
     *          required=true,
     *          allowEmptyValue=false,
     *          @Schema(type="string")
     *     ),
     *     @Response(
     *          response="200",
     *          description="User profile details by product token",
     *          @JsonContent(ref="#/components/schemas/SharedProfileResponse")
     *     ),
     *     @Response(response="404", description="Shared profile not found."),
     * )
     */
    public function publicProfile(string $token)
    {
        $profile = $this->userService->getSharedProfile($token);

        return $profile ? response()->json($profile) : response()->json(['message' => 'Shared profile not found.'], 404);

    }


}
