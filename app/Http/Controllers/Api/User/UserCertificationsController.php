<?php

namespace App\Http\Controllers\Api\User;

use App\Http\Requests\AddGaleryImagesRequest;
use App\Http\Requests\GalleryImageUpdateRequest;
use App\Http\Requests\GetGalleryImagesRequest;
use App\Http\Controllers\Controller;
use App\Services\User\UserCertificationsService;
use Illuminate\Support\Facades\Auth;
use OpenApi\Annotations\Delete;
use OpenApi\Annotations\Get;
use OpenApi\Annotations\Items;
use OpenApi\Annotations\JsonContent;
use OpenApi\Annotations\Parameter;
use OpenApi\Annotations\Post;
use OpenApi\Annotations\Property;
use OpenApi\Annotations\Put;
use OpenApi\Annotations\RequestBody;
use OpenApi\Annotations\Response;
use OpenApi\Annotations\Schema;

class UserCertificationsController extends Controller
{
    /**
     * @var UserCertificationsService
     */
    private $userCertificationsService;

    public function __construct(UserCertificationsService $userCertificationsService)
    {
        $this->userCertificationsService = $userCertificationsService;
    }


    /**
     * @Get(
     *     path="/user/certificates",
     *     description="Get list of user certificates",
     *     tags={"User Certificates"},
     *     security={{"Auth": {}}},
     *     @Parameter(
     *          name="page",
     *          in="query",
     *          description="Certificates page number",
     *          example=1,
     *          required=false,
     *          allowEmptyValue=true,
     *          @Schema(type="integer")
     *     ),
     *     @Parameter(
     *          name="limit",
     *          in="query",
     *          description="Number of certificates per page",
     *          example=1,
     *          required=false,
     *          allowEmptyValue=true,
     *          @Schema(type="integer")
     *     ),
     *     @Parameter(
     *          name="search",
     *          in="query",
     *          description="Search certificates by name and created date in format YYYY-MM-DD.",
     *          example="Image name or date like as 2019-11-11",
     *          required=false,
     *          allowEmptyValue=true,
     *          @Schema(type="string")
     *     ),
     *     @Parameter(
     *          name="sort",
     *          in="query",
     *          description="Sort certificates by created date and name asc, desc",
     *          @Schema(type="string", enum={{"name,asc"}, {"name,desc"}, {"created_at,asc"}, {"created_at,asc"}}),
     *          required=false,
     *          allowEmptyValue=true,
     *          @Schema(type="string")
     *     ),
     *     @Response(
     *          response="200",
     *          description="Example list of user certificates",
     *          @JsonContent(
     *                  allOf={
     *                      @Schema(title="data", @Property(property="data", type="array", @Items(ref="#/components/schemas/GalleryImage"))),
     *                      @Schema(title="total", @Property(property="total", title="total", type="integer", example="200"))
     *                  }
     *          )
     *     ),
     *     @Response(response="401", description="Error: Unauthorized"),
     *     @Response(response="500", description="Internal Server Error.")
     * )
     */
    public function index(GetGalleryImagesRequest $request)
    {
        return response()->json($this->userCertificationsService->getCertificates($request));
    }

    /**
     * @Post(
     *     path="/user/certificates",
     *     description="Add a new certificate",
     *     tags={"User Certificates"},
     *     security={{"Auth": {}}},
     *     @RequestBody(
     *         description="Add a new certificate",
     *       @JsonContent(
     *          @Property(type="array", property="images", @\OpenApi\Annotations\Items(ref="#/components/schemas/AddGalleryImage")),
     *       )
     *     ),
     *     @Response(
     *          response="201",
     *          description="Example list of user certificates",
     *          @JsonContent(
     *                  allOf={
     *                      @Schema(title="data", @Property(property="data", type="array", @Items(ref="#/components/schemas/GalleryImage"))),
     *                      @Schema(title="total", @Property(property="total", title="total", type="integer", example="200"))
     *                  }
     *          )
     *     ),
     *     @Response(response=422, description="Error: Unprocessable Entity"),
     *     @Response(response=401, description="Error: Unauthorized")
     * )
     */
    public function store(AddGaleryImagesRequest $request)
    {
        return response()->json($this->userCertificationsService->addCertificates($request), 201);
    }

    /**
     * @Delete(
     *     path="/user/certificates/{certificateID}",
     *     description="Delete certificate by ID",
     *     tags={"User Certificates"},
     *     security={{"Auth": {}}},
     *     @Parameter(
     *          name="certificateID",
     *          in="path",
     *          description="Certificate ID",
     *          example=1,
     *          required=true,
     *          allowEmptyValue=false,
     *          @Schema(type="integer")
     *     ),
     *     @Response(response=204, description="No content"),
     *     @Response(response=401, description="Error: Unauthorized")
     * )
     */
    public function destroy(int $id)
    {
        $user_id = Auth::user()->id;

        $deleted = $this->userCertificationsService->deleteCertificate($user_id, $id);

        if ($deleted){
            return response()->json(['message' => 'Image successfully deleted'], 204);
        }

        return response()->json(['message' => 'Not found'], 404);
    }

    /**
     * @Put(
     *     path="/user/certificates/{certificateID}",
     *     description="Update certificate",
     *     tags={"User Certificates"},
     *     security={{"Auth": {}}},
     *     @Parameter(
     *          name="certificateID",
     *          in="path",
     *          description="Certificate ID",
     *          example=1,
     *          required=true,
     *          allowEmptyValue=false,
     *          @Schema(type="integer")
     *     ),
     *     @RequestBody(
     *         description="Update certificate",
     *       @JsonContent(ref="#/components/schemas/AddGalleryImage")
     *     ),
     *     @Response(
     *          response="201",
     *          description="Example list of user certificates",
     *          @JsonContent(ref="#/components/schemas/GalleryImage")
     *     ),
     *     @Response(response=422, description="Error: Unprocessable Entity"),
     *     @Response(response=401, description="Error: Unauthorized")
     * )
     */
    public function update(int $id, GalleryImageUpdateRequest $request)
    {
        $updated = $this->userCertificationsService->updateCertificate($id, $request);

        return $updated ? response()->json($updated) : response()->json(['message' => 'Not found'], 404);
    }
}
