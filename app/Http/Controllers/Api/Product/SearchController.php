<?php

namespace App\Http\Controllers\Api\Product;

use App\Http\Controllers\Controller;
use App\Http\Requests\SearchRequest;
use App\Services\Product\ProductSearchService;

class SearchController extends Controller
{
    /**
     * @var ProductSearchService
     */
    private $productSearchService;

    public function __construct(ProductSearchService $productSearchService)
    {
        $this->productSearchService = $productSearchService;
    }

    public function search(SearchRequest $request)
    {
        return response()->json($this->productSearchService->search($request->get('text')));
    }
}
