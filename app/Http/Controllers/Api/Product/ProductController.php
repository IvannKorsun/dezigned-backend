<?php

namespace App\Http\Controllers\Api\Product;

use App\Http\Requests\FilterRequest;
use App\Http\Requests\ProductCreateRequest;
use App\Services\Product\ProductService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use OpenApi\Annotations\Get;
use OpenApi\Annotations\Items;
use OpenApi\Annotations\JsonContent;
use OpenApi\Annotations\MediaType;
use OpenApi\Annotations\Parameter;
use OpenApi\Annotations\Post;
use OpenApi\Annotations\Property;
use OpenApi\Annotations\RequestBody;
use OpenApi\Annotations\Response;
use OpenApi\Annotations\Schema;

class ProductController extends Controller
{
    /**
     * @var ProductService
     */
    private $productService;

    public function __construct(ProductService $productService)
    {
        $this->productService = $productService;
    }

    /**
     * @Get(
     *     path="/product",
     *     description="Get list of products",
     *     tags={"Product"},
     *     security={{"Auth": {}}},
     *     @Parameter(
     *          name="price",
     *          in="query",
     *          description="Min Max price comma separated",
     *          example="10,50",
     *          required=false,
     *          allowEmptyValue=true,
     *          @Schema(type="string")
     *     ),
     *     @Parameter(
     *          name="manufacturer",
     *          in="query",
     *          description="Product brands one value or multiple comma separated",
     *          example="1,2",
     *          required=false,
     *          allowEmptyValue=true,
     *          @Schema(type="string")
     *     ),
     *     @Parameter(
     *          name="category",
     *          in="query",
     *          description="Product category or sub category id comma separated",
     *          example="1,2",
     *          required=false,
     *          allowEmptyValue=true,
     *          @Schema(type="string")
     *     ),
     *     @Parameter(
     *          name="type",
     *          in="query",
     *          description="Product types one value or multiple comma separated",
     *          example="3,4",
     *          required=false,
     *          allowEmptyValue=true,
     *          @Schema(type="string")
     *     ),
     *     @Parameter(
     *          name="style",
     *          in="query",
     *          description="Product styles one value or multiple comma separated",
     *          example="5,6",
     *          required=false,
     *          allowEmptyValue=true,
     *          @Schema(type="string")
     *     ),
     *     @Parameter(
     *          name="search",
     *          in="query",
     *          description="Text search word. Found in the name, description, or sku fields, or in the brand name",
     *          example="product name",
     *          required=false,
     *          allowEmptyValue=true,
     *          @Schema(type="string")
     *     ),
     *     @Parameter(
     *          name="page",
     *          in="query",
     *          description="Number of product page",
     *          example="1",
     *          required=false,
     *          allowEmptyValue=true,
     *          @Schema(type="int")
     *     ),
     *     @Parameter(
     *          name="limit",
     *          in="query",
     *          description="Number of products per page",
     *          example=16,
     *          required=false,
     *          allowEmptyValue=true,
     *          @Schema(type="int")
     *     ),
     *     @Response(response="403", description="User does not have the right roles."),
     *     @Response(
     *          response="200",
     *          description="Example product list response",
     *          @JsonContent(
     *                  allOf={
     *                      @Schema(
     *                          title="products",
     *                          @Property(type="array", property="products", @Items(ref="#/components/schemas/ProductResponse")),
     *                      ),
     *                      @Schema(
     *                          title="meta",
     *                          type="object",
     *                          @Property(title="meta", property="meta", type="object", ref="#/components/schemas/BcMetaResponse")
     *                      ),
     *                      @Schema(
     *                          title="category",
     *                          @Property(property="category", ref="#/components/schemas/BcCategoryResponse"),
     *                      )
     *                  }
     *          )
     *     ),
     *     @Response(response="401", description="Error: Unauthorized"),
     *     @Response(response="404", description="User not found."),
     *     @Response(response="500", description="Internal Server Error.")
     * )
     */
    public function index(FilterRequest $request)
    {
        return response()->json($this->productService->getProducts($request));
    }

    /**
     * @Post(
     *     path="/product",
     *     description="Create new product",
     *     tags={"Product"},
     *     security={{"Auth": {}}},
     *     @RequestBody(
     *          @MediaType(
     *              mediaType="application/json",
     *              @Property(ref="#/components/schemas/ProductCreateRequest")
     *          ),
     *         description="Create new product",
     *         required=true
     *     ),
     *     @Response(
     *          response="201",
     *          description="Product successfully created!",
     *          @JsonContent(ref="#/components/schemas/ProductResponse")
     *      ),
     *     @Response(response="422", description="Error: Unprocessable Entity"),
     *     @Response(response="401", description="Error: Unauthorized")
     * )
     */
    public function store(ProductCreateRequest $request)
    {
        return response()->json($this->productService->saveProduct($request), 201);
    }

    /**
     * @Get(
     *     path="/product/{id}",
     *     description="Get one product details by product id",
     *     tags={"Product"},
     *     security={{"Auth": {}}},
     *     @Parameter(
     *          name="id",
     *          in="path",
     *          description="Get product details by id",
     *          example=1,
     *          required=true,
     *          allowEmptyValue=false,
     *          @Schema(type="integer")
     *     ),
     *     @Response(
     *          response="200",
     *          description="Product details by product id",
     *          @JsonContent(ref="#/components/schemas/ProductResponse")
     *     ),
     *     @Response(response="401", description="Error: Unauthorized"),
     *     @Response(response="404", description="Product not found."),
     * )
     */
    public function show($id)
    {
        $product = $this->productService->getProduct($id);

        if ($product){
            return response()->json($product);
        }

        return response()->json(['message' => 'Not found'], 404);
    }


    /**
     * @Get(
     *     path="/product-attributes",
     *     description="Get selectable product attributes",
     *     tags={"Product"},
     *     security={{"Auth": {}}},
     *     @Response(
     *          response="200",
     *          description="Selectable product attributes (brand, style, type)",
     *          @JsonContent(ref="#/components/schemas/ProductAttributes")
     *     ),
     *     @Response(response="401", description="Error: Unauthorized"),
     *     @Response(response="404", description="Product not found."),
     * )
     */

    public function getSelectableAttributes()
    {
        return response()->json($this->productService->getProductSelectableAttributes());
    }

    /**
     * @Get(
     *     path="product/categories/tree",
     *     description="Get categories tree",
     *     tags={"Product"},
     *     security={{"Auth": {}}},
     *     @Response(
     *          response="200",
     *          description="Get categories tree from bigcommerce",
     *          @JsonContent(ref="#/components/schemas/BcCategoriesTreeResponse")
     *     ),
     *     @Response(response="401", description="Error: Unauthorized"),
     *     @Response(response="404", description="Product not found."),
     * )
     */
    public function categoriesTree()
    {
        return response()->json($this->productService->categoriesTree());
    }
}
