<?php

namespace App\Http\Controllers\Api\Product;

use App\Http\Controllers\Controller;
use App\Services\Product\FilterService;
use Illuminate\Http\Request;
use OpenApi\Annotations\Get;
use OpenApi\Annotations\JsonContent;
use OpenApi\Annotations\Response;


class FilterController extends Controller
{
    /**
     * @var FilterService
     */
    private $filterService;

    public function __construct(FilterService $filterService)
    {
        $this->filterService = $filterService;
    }
    /**
     * @Get(
     *     path="/filters",
     *     description="Get list of available product filters",
     *     tags={"Product"},
     *     security={{"Auth": {}}},
     *     @Response(
     *          response="200",
     *          description="Example product filters list response",
     *          @JsonContent(ref="#/components/schemas/FiltersResponse")
     *     ),
     *     @Response(response="403", description="User does not have the right roles."),
     * )
     */
    public function getFilters(Request $request): array
    {
        return $this->filterService->getFilters($request);
    }
}
