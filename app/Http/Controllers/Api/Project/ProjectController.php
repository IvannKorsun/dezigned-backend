<?php

namespace App\Http\Controllers\Api\Project;

use App\Http\Requests\Project\ProjectCreateRequest;
use App\Http\Requests\Project\ProjectListRequest;
use App\Http\Requests\Project\ProjectUpdateRequest;
use App\Services\Project\ProjectService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use OpenApi\Annotations\Delete;
use OpenApi\Annotations\Get;
use OpenApi\Annotations\Items;
use OpenApi\Annotations\JsonContent;
use OpenApi\Annotations\MediaType;
use OpenApi\Annotations\Parameter;
use OpenApi\Annotations\Post;
use OpenApi\Annotations\Property;
use OpenApi\Annotations\Put;
use OpenApi\Annotations\RequestBody;
use OpenApi\Annotations\Response;
use OpenApi\Annotations\Schema;

class ProjectController extends Controller
{
    /**
     * @var ProjectService
     */
    private $projectService;

    public function __construct(ProjectService $projectService)
    {
        $this->projectService = $projectService;
    }

    /**
     * @Get(
     *     path="/project",
     *     description="Get list of projects",
     *     tags={"Project"},
     *     security={{"Auth": {}}},
     *     @Parameter(
     *          name="active",
     *          in="query",
     *          description="Active (1) and archived(0) project filter",
     *          example="1",
     *          required=false,
     *          allowEmptyValue=true,
     *          @Schema(type="string")
     *     ),
     *     @Parameter(
     *          name="search",
     *          in="query",
     *          description="Text search word. Searhc by project name",
     *          example="project name",
     *          required=false,
     *          allowEmptyValue=true,
     *          @Schema(type="string")
     *     ),
     *     @Parameter(
     *          name="page",
     *          in="query",
     *          description="Number of projects page",
     *          example="1",
     *          required=false,
     *          allowEmptyValue=true,
     *          @Schema(type="int")
     *     ),
     *     @Parameter(
     *          name="limit",
     *          in="query",
     *          description="Number of projects per page",
     *          example=16,
     *          required=false,
     *          allowEmptyValue=true,
     *          @Schema(type="int")
     *     ),
     *     @Response(response="403", description="User does not have the right roles."),
     *     @Response(
     *          response="200",
     *          description="Example project list response",
     *          @JsonContent(
     *                  allOf={
     *                      @Schema(
     *                          title="data",
     *                          @Property(type="array", property="data", @\OpenApi\Annotations\Items(ref="#/components/schemas/ProjectListEntryResponse")),
     *                      ),
     *                      @Schema(
     *                          title="total",
     *                          @Property(title="total", property="total", type="integer", example="10")
     *                      )
     *                  }
     *          )
     *     ),
     *     @Response(response="401", description="Error: Unauthorized"),
     *     @Response(response="404", description="User not found."),
     *     @Response(response="500", description="Internal Server Error.")
     * )
     */
    public function index(ProjectListRequest $request)
    {
        return response()->json($this->projectService->all($request));
    }

    /**
     * @Post(
     *     path="/project",
     *     description="Add a new project",
     *     tags={"Project"},
     *     security={{"Auth": {}}},
     *     @RequestBody(
     *         description="Add a new project",
     *          @MediaType(
     *              mediaType="application/json",
     *              @JsonContent(ref="#/components/schemas/ProjectCreateRequest")
     *          )
     *     ),
     *     @Response(response=201, description="Project created successfully", @JsonContent(ref="#/components/schemas/ProjectResponse")),
     *     @Response(response=422, description="Error: Unprocessable Entity"),
     *     @Response(response=401, description="Error: Unauthorized")
     * )
     */
    public function store(ProjectCreateRequest $request)
    {
        return response()->json($this->projectService->createProject($request));
    }

    /**
     * @Get(
     *     path="/project/{id}",
     *     description="Get one project details by project id",
     *     tags={"Project"},
     *     security={{"Auth": {}}},
     *     @Parameter(
     *          name="id",
     *          in="path",
     *          description="Get project details by id",
     *          example=1,
     *          required=true,
     *          allowEmptyValue=false,
     *          @Schema(type="integer")
     *     ),
     *     @Response(
     *          response="200",
     *          description="Project details by project id",
     *          @JsonContent(ref="#/components/schemas/ProjectResponse")
     *     ),
     *     @Response(response="401", description="Error: Unauthorized"),
     *     @Response(response="404", description="Project not found."),
     * )
     */
    public function show($id)
    {
        $project = $this->projectService->getProject($id);

        if ($project){
            return response()->json($project);
        }

        return response()->json(['message' => 'Not found'], 404);
    }

    /**
     * @Put(
     *     path="/project/{id}",
     *     description="Update project by id",
     *     tags={"Project"},
     *     security={{"Auth": {}}},
     *     @Parameter(
     *          name="id",
     *          in="path",
     *          description="Update project by id",
     *          example=1,
     *          required=true,
     *          allowEmptyValue=false,
     *          @Schema(type="integer")
     *     ),
     *     @RequestBody(
     *          @MediaType(
     *              mediaType="application/json",
     *              @Schema(ref="#/components/schemas/ProjectCreateRequest")
     *          )
     *     ),
     *     @Response(
     *          response="200",
     *          description="Updated project details by project id",
     *          @JsonContent(ref="#/components/schemas/ProjectResponse")
     *     ),
     *     @Response(response="401", description="Error: Unauthorized"),
     *     @Response(response="404", description="Project not found."),
     *     @Response(response="500", description="Internal Server Error.")
     * )
     */
    public function update(ProjectUpdateRequest $request, $id)
    {
        $updated = $this->projectService->updateProject($request, $id);

        if ($updated){
            return response()->json($updated);
        }

        return response()->json(['message' => 'Not found'], 404);
    }

    /**
     * @Delete(
     *     path="/project/{id}",
     *     description="Delete project",
     *     tags={"Project"},
     *     security={{"Auth": {}}},
     *     @Parameter(
     *          name="id",
     *          in="path",
     *          description="Delete project by id",
     *          example=1,
     *          required=true,
     *          allowEmptyValue=false,
     *          @Schema(type="integer")
     *     ),
     *     @Response(response=204, description="No content"),
     *     @Response(response=422, description="Error: Unprocessable Entity", @JsonContent()),
     *     @Response(response=401, description="Error: Unauthorized")
     * )
     */
    public function destroy($id)
    {
        $deleted = $this->projectService->deleteProject($id);

        if ($deleted){
            return response()->json(['message' => 'Project successfully deleted'], 204);
        }

        return response()->json(['message' => 'Not found'], 404);
    }
}
