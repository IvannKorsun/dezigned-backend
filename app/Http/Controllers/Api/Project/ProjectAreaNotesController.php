<?php

namespace App\Http\Controllers\Api\Project;

use App\Http\Requests\AddUpdateNoteRequest;
use App\Services\Project\ProjectAreaNotesService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use OpenApi\Annotations\Delete;
use OpenApi\Annotations\Get;
use OpenApi\Annotations\Items;
use OpenApi\Annotations\JsonContent;
use OpenApi\Annotations\Parameter;
use OpenApi\Annotations\Post;
use OpenApi\Annotations\Property;
use OpenApi\Annotations\Put;
use OpenApi\Annotations\RequestBody;
use OpenApi\Annotations\Response;
use OpenApi\Annotations\Schema;

class ProjectAreaNotesController extends Controller
{
    /**
     * @var ProjectAreaNotesService
     */
    private $areaNotesService;

    /**
     * ProjectAreaNotesController constructor.
     */
    public function __construct(ProjectAreaNotesService $areaNotesService)
    {
        $this->areaNotesService = $areaNotesService;
    }

    /**
     * @Get(
     *     path="/project/{projectId}/area/{areaId}/notes",
     *     description="Get list of project area notes",
     *     tags={"Project area notes"},
     *     security={{"Auth": {}}},
     *     @Parameter(
     *          name="projectId",
     *          in="path",
     *          description="Project ID",
     *          example=1,
     *          required=true,
     *          allowEmptyValue=false,
     *          @Schema(type="integer")
     *     ),
     *     @Parameter(
     *          name="areaId",
     *          in="path",
     *          description="Project Area ID",
     *          example=1,
     *          required=true,
     *          allowEmptyValue=false,
     *          @Schema(type="integer")
     *     ),
     *     @Parameter(
     *          name="page",
     *          in="query",
     *          description="Notes page number",
     *          example=1,
     *          required=false,
     *          allowEmptyValue=true,
     *          @Schema(type="integer")
     *     ),
     *     @Parameter(
     *          name="limit",
     *          in="query",
     *          description="Number of notes per page",
     *          example=1,
     *          required=false,
     *          allowEmptyValue=true,
     *          @Schema(type="integer")
     *     ),
     *     @Response(
     *          response="200",
     *          description="Example list of project area notes",
     *          @JsonContent(
     *                  allOf={
     *                      @Schema(title="title", @Property(property="title", title="title", type="string", example="Area name")),
     *                      @Schema(title="data", @Property(property="data", type="array", @Items(ref="#/components/schemas/ProjectAreaNoteResponse"))),
     *                      @Schema(title="total", @Property(property="total", title="total", type="integer", example="200"))
     *                  }
     *          )
     *     ),
     *     @Response(response="401", description="Error: Unauthorized"),
     *     @Response(response="500", description="Internal Server Error.")
     * )
     */
    public function index(Request $request, int $projectId, int $areaId)
    {
        return response()->json($this->areaNotesService->getNotes($projectId, $areaId, $request));
    }

    /**
     * @Post(
     *     path="/project/{projectId}/area/{areaId}/notes",
     *     description="Add a new project area note",
     *     tags={"Project area notes"},
     *     security={{"Auth": {}}},
     *     @Parameter(
     *          name="projectId",
     *          in="path",
     *          description="Project ID",
     *          example=1,
     *          required=true,
     *          allowEmptyValue=false,
     *          @Schema(type="integer")
     *     ),
     *     @Parameter(
     *          name="areaId",
     *          in="path",
     *          description="Project Area ID",
     *          example=1,
     *          required=true,
     *          allowEmptyValue=false,
     *          @Schema(type="integer")
     *     ),
     *     @RequestBody(
     *         description="Add a new project area note",
     *         @JsonContent(@Property(property="note", title="note", type="string", example="Project area note"))
     *     ),
     *     @Response(
     *          response="201",
     *          description="Example of add a new project area note response",
     *          @JsonContent(ref="#/components/schemas/ProjectAreaNoteResponse")
     *     ),
     *     @Response(response=422, description="Error: Unprocessable Entity"),
     *     @Response(response=401, description="Error: Unauthorized")
     * )
     */
    public function store(AddUpdateNoteRequest $request, int $projectId, int $areaId)
    {
        $notes = $this->areaNotesService->updateOrCreateNote($request, $projectId, $areaId, null);

        return $notes ? response()->json($notes) : response()->json(['message' => 'Not found'], 404);
    }

    /**
     * @Put(
     *     path="/project/{projectId}/area/{areaId}/notes/{noteId}",
     *     description="Update project area note by id",
     *     tags={"Project area notes"},
     *     security={{"Auth": {}}},
     *     @Parameter(
     *          name="projectId",
     *          in="path",
     *          description="Project ID",
     *          example=1,
     *          required=true,
     *          allowEmptyValue=false,
     *          @Schema(type="integer")
     *     ),
     *     @Parameter(
     *          name="areaId",
     *          in="path",
     *          description="Project Area ID",
     *          example=1,
     *          required=true,
     *          allowEmptyValue=false,
     *          @Schema(type="integer")
     *     ),
     *     @Parameter(
     *          name="noteId",
     *          in="path",
     *          description="Project Area note ID",
     *          example=1,
     *          required=true,
     *          allowEmptyValue=false,
     *          @Schema(type="integer")
     *     ),
     *     @RequestBody(
     *         description="Update project area note by id",
     *         @JsonContent(@Schema(title="note", @Property(property="note", title="note", type="dtring", example="Project area note")))
     *     ),
     *     @Response(
     *          response="201",
     *          description="Example of add a new project area note response",
     *          @JsonContent(ref="#/components/schemas/ProjectAreaNoteResponse")
     *     ),
     *     @Response(response=422, description="Error: Unprocessable Entity"),
     *     @Response(response=401, description="Error: Unauthorized")
     * )
     */
    public function update(AddUpdateNoteRequest $request, int $projectId, int $areaId, int $noteId)
    {
        $notes = $this->areaNotesService->updateOrCreateNote($request, $projectId, $areaId, $noteId);

        return $notes ? response()->json($notes) : response()->json(['message' => 'Not found'], 404);
    }

    /**
     * @Delete(
     *     path="/project/{projectId}/area/{areaId}/notes/{noteId}",
     *     description="Delete project area note by id",
     *     tags={"Project area notes"},
     *     security={{"Auth": {}}},
     *     @Parameter(
     *          name="projectId",
     *          in="path",
     *          description="Project ID",
     *          example=1,
     *          required=true,
     *          allowEmptyValue=false,
     *          @Schema(type="integer")
     *     ),
     *     @Parameter(
     *          name="areaId",
     *          in="path",
     *          description="Project Area ID",
     *          example=1,
     *          required=true,
     *          allowEmptyValue=false,
     *          @Schema(type="integer")
     *     ),
     *     @Parameter(
     *          name="noteId",
     *          in="path",
     *          description="Project Area note ID",
     *          example=1,
     *          required=true,
     *          allowEmptyValue=false,
     *          @Schema(type="integer")
     *     ),
     *     @Response(response=204, description="No content"),
     *     @Response(response=401, description="Error: Unauthorized"),
     *     @Response(response=422, description="Error: Unprocessable Entity"),
     * )
     */
    public function destroy(int $projectId, int $areaId, int $noteId)
    {
        $deleted = $this->areaNotesService->deleteNote($projectId, $areaId, $noteId);

        return $deleted ? response()->json([], 204) : response()->json(['message' => 'Not found'], 404);
    }
}
