<?php

namespace App\Http\Controllers\Api\Project;

use App\Http\Requests\GalleryImageUpdateRequest;
use App\Http\Requests\GetGalleryImagesRequest;
use App\Http\Requests\AddGaleryImagesRequest;
use App\Services\Project\ProjectAreaGalleryService;
use App\Http\Controllers\Controller;
use OpenApi\Annotations\Delete;
use OpenApi\Annotations\Get;
use OpenApi\Annotations\Items;
use OpenApi\Annotations\JsonContent;
use OpenApi\Annotations\Parameter;
use OpenApi\Annotations\Post;
use OpenApi\Annotations\Property;
use OpenApi\Annotations\Put;
use OpenApi\Annotations\RequestBody;
use OpenApi\Annotations\Response;
use OpenApi\Annotations\Schema;

class ProjectAreaGalleryController extends Controller
{
    /**
     * @var ProjectAreaGalleryService
     */
    private $galleryService;

    public function __construct(ProjectAreaGalleryService $galleryService)
    {
        $this->galleryService = $galleryService;
    }

    /**
     * @Get(
     *     path="/project/{projectId}/area/{areaId}/images",
     *     description="Get list of project area gallery images",
     *     tags={"Project area gallery"},
     *     security={{"Auth": {}}},
     *     @Parameter(
     *          name="projectId",
     *          in="path",
     *          description="Project ID",
     *          example=1,
     *          required=true,
     *          allowEmptyValue=false,
     *          @Schema(type="integer")
     *     ),
     *     @Parameter(
     *          name="areaId",
     *          in="path",
     *          description="Project Area ID",
     *          example=1,
     *          required=true,
     *          allowEmptyValue=false,
     *          @Schema(type="integer")
     *     ),
     *     @Parameter(
     *          name="page",
     *          in="query",
     *          description="Gallery page number",
     *          example=1,
     *          required=false,
     *          allowEmptyValue=true,
     *          @Schema(type="integer")
     *     ),
     *     @Parameter(
     *          name="limit",
     *          in="query",
     *          description="Number of images per page",
     *          example=1,
     *          required=false,
     *          allowEmptyValue=true,
     *          @Schema(type="integer")
     *     ),
     *     @Parameter(
     *          name="search",
     *          in="query",
     *          description="Search images by name and created date in format YYYY-MM-DD.",
     *          example="Image name or date like as 2019-11-11",
     *          required=false,
     *          allowEmptyValue=true,
     *          @Schema(type="string")
     *     ),
     *     @Parameter(
     *          name="sort",
     *          in="query",
     *          description="Sort images by created date and name asc, desc",
     *          @Schema(type="string", enum={{"name,asc"}, {"name,desc"}, {"created_at,asc"}, {"created_at,asc"}}),
     *          required=false,
     *          allowEmptyValue=true,
     *          @Schema(type="string")
     *     ),
     *     @Response(
     *          response="200",
     *          description="Example list of project area gallery images",
     *          @JsonContent(
     *                  allOf={
     *                      @Schema(title="data", @Property(property="data", type="array", @Items(ref="#/components/schemas/GalleryImage"))),
     *                      @Schema(title="total", @Property(property="total", title="total", type="integer", example="200"))
     *                  }
     *          )
     *     ),
     *     @Response(response="401", description="Error: Unauthorized"),
     *     @Response(response="500", description="Internal Server Error.")
     * )
     */
    public function index(GetGalleryImagesRequest $request)
    {
        $images = $this->galleryService->getGalleryImages($request);

        return $images ? response()->json($images) : response()->json(['message' => 'Not found'], 404);
    }

    /**
     * @Post(
     *     path="/project/{projectId}/area/{areaId}/images",
     *     description="Add gallery images",
     *     tags={"Project area gallery"},
     *     security={{"Auth": {}}},
     *     @Parameter(
     *          name="projectId",
     *          in="path",
     *          description="Project ID",
     *          example=1,
     *          required=true,
     *          allowEmptyValue=false,
     *          @Schema(type="integer")
     *     ),
     *     @Parameter(
     *          name="areaId",
     *          in="path",
     *          description="Project Area ID",
     *          example=1,
     *          required=true,
     *          allowEmptyValue=false,
     *          @Schema(type="integer")
     *     ),
     *     @RequestBody(
     *         description="Add gallery images",
     *       @JsonContent(
     *          @Property(type="array", property="images", @\OpenApi\Annotations\Items(ref="#/components/schemas/AddGalleryImage")),
     *       )
     *     ),
     *     @Response(
     *          response="201",
     *          description="Example list of user gallery images",
     *          @JsonContent(
     *              type="array",
     *              @Items(
     *                  allOf={
     *                      @Schema(title="data", @Property(property="data", type="array", @Items(ref="#/components/schemas/GalleryImage"))),
     *                      @Schema(title="total", @Property(property="total", title="total", type="integer", example="200"))
     *                  }
     *              )
     *          )
     *     ),
     *     @Response(response=422, description="Error: Unprocessable Entity"),
     *     @Response(response=401, description="Error: Unauthorized")
     * )
     */
    public function store(AddGaleryImagesRequest $request)
    {
        $images = $this->galleryService->addGalleryImages($request);

        return $images ? response()->json($images) : response()->json(['message' => 'Not found'], 404);
    }

    /**
     * @Put(
     *     path="/project/{projectId}/area/{areaId}/images/{imageId}",
     *     description="Update gallery image",
     *     tags={"Project area gallery"},
     *     security={{"Auth": {}}},
     *     @Parameter(
     *          name="projectId",
     *          in="path",
     *          description="Project ID",
     *          example=1,
     *          required=true,
     *          allowEmptyValue=false,
     *          @Schema(type="integer")
     *     ),
     *     @Parameter(
     *          name="areaId",
     *          in="path",
     *          description="Project Area ID",
     *          example=1,
     *          required=true,
     *          allowEmptyValue=false,
     *          @Schema(type="integer")
     *     ),
     *     @Parameter(
     *          name="imageId",
     *          in="path",
     *          description="Project Area image ID",
     *          example=1,
     *          required=true,
     *          allowEmptyValue=false,
     *          @Schema(type="integer")
     *     ),
     *     @RequestBody(
     *         description="Update gallery image",
     *       @JsonContent(ref="#/components/schemas/AddGalleryImage")
     *     ),
     *     @Response(
     *          response="201",
     *          description="Example list of user gallery images",
     *          @JsonContent(
     *              type="array",
     *              @Items(
     *                  allOf={
     *                      @Schema(title="data", @Property(property="data", type="array", @Items(ref="#/components/schemas/GalleryImage"))),
     *                      @Schema(title="total", @Property(property="total", title="total", type="integer", example="200"))
     *                  }
     *              )
     *          )
     *     ),
     *     @Response(response=422, description="Error: Unprocessable Entity"),
     *     @Response(response=401, description="Error: Unauthorized")
     * )
     */
    public function update(int $projectId, int $areaId, int $imageId, GalleryImageUpdateRequest $request)
    {
        $updated = $this->galleryService->updateImage($projectId, $areaId, $imageId, $request);

        return $updated ? response()->json($updated) : response()->json(['message' => 'Not found'], 404);
    }

    /**
     * @Delete(
     *     path="/project/{projectId}/area/{areaId}/images/{imageId}",
     *     description="Delete gallery image",
     *     tags={"Project area gallery"},
     *     security={{"Auth": {}}},
     *     @Parameter(
     *          name="projectId",
     *          in="path",
     *          description="Project ID",
     *          example=1,
     *          required=true,
     *          allowEmptyValue=false,
     *          @Schema(type="integer")
     *     ),
     *     @Parameter(
     *          name="areaId",
     *          in="path",
     *          description="Project Area ID",
     *          example=1,
     *          required=true,
     *          allowEmptyValue=false,
     *          @Schema(type="integer")
     *     ),
     *     @Parameter(
     *          name="imageId",
     *          in="path",
     *          description="Project Area image ID",
     *          example=1,
     *          required=true,
     *          allowEmptyValue=false,
     *          @Schema(type="integer")
     *     ),
     *     @Response(response=204, description="No content"),
     *     @Response(response=401, description="Error: Unauthorized"),
     *     @Response(response=422, description="Error: Unprocessable Entity"),
     * )
     */
    public function destroy(int $projectId, int $areaId, int $id)
    {
        $deleted = $this->galleryService->deleteImage($projectId, $areaId, $id);

        return $deleted ? response()->json([],204) : response()->json(['message' => 'Not found'], 404);
    }
}
