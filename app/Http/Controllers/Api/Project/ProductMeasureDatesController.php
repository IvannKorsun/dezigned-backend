<?php

namespace App\Http\Controllers\Api\Project;

use App\Http\Controllers\Controller;
use App\Http\Requests\Date\DateRequest;
use App\Http\Requests\Project\ProductMeasureAddDatesRequest;
use App\Services\Project\ProductMeasureDatesService;
use OpenApi\Annotations\Get;
use OpenApi\Annotations\Items;
use OpenApi\Annotations\JsonContent;
use OpenApi\Annotations\MediaType;
use OpenApi\Annotations\Parameter;
use OpenApi\Annotations\Post;
use OpenApi\Annotations\Property;
use OpenApi\Annotations\Put;
use OpenApi\Annotations\RequestBody;
use OpenApi\Annotations\Response;
use OpenApi\Annotations\Schema;

class ProductMeasureDatesController extends Controller
{
    /**
     * @var ProductMeasureDatesService
     */
    private $measureDatesService;

    /**
     * ProductMeasureDatesController constructor.
     */
    public function __construct(ProductMeasureDatesService $measureDatesService)
    {
        $this->measureDatesService = $measureDatesService;
    }

    /**
     * @Get(
     *     path="/project/{projectId}/area/{areaId}/products/{productId}/measurements/{measureId}/dates",
     *     description="Get list of measure dates",
     *     tags={"Project area product measure dates"},
     *     security={{"Auth": {}}},
     *     @Parameter(
     *          name="projectId",
     *          in="path",
     *          description="Project ID",
     *          example=1,
     *          required=true,
     *          allowEmptyValue=false,
     *          @Schema(type="integer")
     *     ),
     *     @Parameter(
     *          name="areaId",
     *          in="path",
     *          description="Project Area ID",
     *          example=1,
     *          required=true,
     *          allowEmptyValue=false,
     *          @Schema(type="integer")
     *     ),
     *     @Parameter(
     *          name="productId",
     *          in="path",
     *          description="Project Area product ID",
     *          example=1,
     *          required=true,
     *          allowEmptyValue=false,
     *          @Schema(type="integer")
     *     ),
     *     @Parameter(
     *          name="measureId",
     *          in="path",
     *          description="Project Area product measure ID",
     *          example=1,
     *          required=true,
     *          allowEmptyValue=false,
     *          @Schema(type="integer")
     *     ),
     *     @Response(
     *          response="200",
     *          description="Example list of dates",
     *          @JsonContent(@Property(property="data", type="array", @Items(ref="#/components/schemas/ProductMeasureDatesResponse")))
     *     ),
     *     @Response(response="401", description="Error: Unauthorized"),
     *     @Response(response="500", description="Internal Server Error.")
     * )
     */
    public function index(int $projectId, int $areaId, int $productId, int $measureId)
    {
        $dates = $this->measureDatesService->getDates($projectId, $areaId, $productId, $measureId);

        return $dates ? response()->json($dates) : response()->json(['message' => 'Not found'], 404);
    }

    /**
     * @Post(
     *     path="/project/{projectId}/area/{areaId}/products/{productId}/measurements/{measureId}/dates",
     *     description="Add measure dates",
     *     tags={"Project area product measure dates"},
     *     security={{"Auth": {}}},
     *     @Parameter(
     *          name="projectId",
     *          in="path",
     *          description="Project ID",
     *          example=1,
     *          required=true,
     *          allowEmptyValue=false,
     *          @Schema(type="integer")
     *     ),
     *     @Parameter(
     *          name="areaId",
     *          in="path",
     *          description="Project Area ID",
     *          example=1,
     *          required=true,
     *          allowEmptyValue=false,
     *          @Schema(type="integer")
     *     ),
     *     @Parameter(
     *          name="productId",
     *          in="path",
     *          description="Project Area product ID",
     *          example=1,
     *          required=true,
     *          allowEmptyValue=false,
     *          @Schema(type="integer")
     *     ),
     *     @Parameter(
     *          name="measureId",
     *          in="path",
     *          description="Project Area product measure ID",
     *          example=1,
     *          required=true,
     *          allowEmptyValue=false,
     *          @Schema(type="integer")
     *     ),
     *     @RequestBody(
     *         description="Add peoduct measure dates",
     *       @JsonContent(
     *          allOf={
     *              @Schema(@Property(type="array", property="dates", @\OpenApi\Annotations\Items(ref="#/components/schemas/ProductMeasureDateRequest"))),
     *              @Schema(@Property(property="action", type="string", format="string", example="'save' or 'request'"))
     *
     *          }
     *       )
     *     ),
     *     @Response(
     *          response="200",
     *          description="Example list of measure dates",
     *          @JsonContent(
     *              type="array",
     *              @Items(
     *                  allOf={
     *                      @Schema(title="data", @Property(property="data", type="array", @Items(ref="#/components/schemas/ProductMeasureDatesResponse"))),
     *                  }
     *              )
     *          )
     *     ),
     *     @Response(response=422, description="Error: Unprocessable Entity"),
     *     @Response(response=401, description="Error: Unauthorized")
     * )
     */
    public function store(int $projectId, int $areaId, int $productId, int $measureId,ProductMeasureAddDatesRequest $request)
    {
        $saved = $this->measureDatesService->storeDates($projectId, $areaId, $productId, $measureId,$request);

        return $saved ? response()->json($saved) : response()->json(['message' => 'Not found'], 404);
    }

    /**
     * @Get(
     *     path="/dates/{measureId}",
     *     description="Get list of measure dates",
     *     tags={"Project area product measure dates"},
     *     security={{"Auth": {}}},
     *     @Parameter(
     *          name="measureId",
     *          in="path",
     *          description="Project Area product measure ID",
     *          example=1,
     *          required=true,
     *          allowEmptyValue=false,
     *          @Schema(type="integer")
     *     ),
     *     @Response(
     *          response="200",
     *          description="Example list of dates",
     *          @JsonContent(@Property(property="data", type="array", @Items(ref="#/components/schemas/ProductMeasureDatesResponse")))
     *     ),
     *     @Response(response="401", description="Error: Unauthorized"),
     *     @Response(response="500", description="Internal Server Error.")
     * )
     * @param int $measureId
     *
     * @return \Illuminate\Http\JsonResponse
     */

    public function getDatesByMeasureId(int $measureId)
    {
        $dates = $this->measureDatesService->getDatesByMeasureId($measureId);

        return $dates ? response()->json($dates) : response()->json(['message' => 'Not found'], 404);
    }

    /**
     * @Put(
     *     path="/dates/{id}",
     *     description="Update date by id",
     *     tags={"Project area product measure dates"},
     *     security={{"Auth": {}}},
     *     @Parameter(
     *          name="id",
     *          in="path",
     *          description="Update date by id",
     *          example=1,
     *          required=true,
     *          allowEmptyValue=false,
     *          @Schema(type="integer")
     *     ),
     *     @RequestBody(
     *          @MediaType(
     *              mediaType="application/json",
     *              @Schema(ref="#/components/schemas/DatesUpdateStatusRequest")
     *          )
     *     ),
     *     @Response(
     *          response="200",
     *          description="Updated date by id",
     *     ),
     *     @Response(response="401", description="Error: Unauthorized"),
     *     @Response(response="404", description="Date not found."),
     *     @Response(response="500", description="Internal Server Error.")
     * )
     * $id
     *
     * $id
     *
     * @param \App\Http\Requests\Date\DateRequest $request
     * @param int                                 $id
     *
     * @return \Illuminate\Http\JsonResponse
     */

    public function update(DateRequest $request, int $id)
    {
        $dates = $this->measureDatesService->updateDate($request,$id);

        return $dates ? response()->json($dates) : response()->json(['message' => 'Not found'], 404);
    }
}
