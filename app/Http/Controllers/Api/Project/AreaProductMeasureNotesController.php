<?php

namespace App\Http\Controllers\Api\Project;

use App\Http\Requests\AddUpdateNoteRequest;
use App\Http\Requests\Project\GetProjectAreaProductNote;
use App\Services\Project\AreaProductMeasureNotesService;
use App\Http\Controllers\Controller;
use OpenApi\Annotations\Delete;
use OpenApi\Annotations\Get;
use OpenApi\Annotations\Items;
use OpenApi\Annotations\JsonContent;
use OpenApi\Annotations\Parameter;
use OpenApi\Annotations\Post;
use OpenApi\Annotations\Property;
use OpenApi\Annotations\Put;
use OpenApi\Annotations\RequestBody;
use OpenApi\Annotations\Response;
use OpenApi\Annotations\Schema;

class AreaProductMeasureNotesController extends Controller
{
    /**
     * @var AreaProductMeasureNotesService
     */
    private $areaProductMeasureNotes;

    /**
     * AreaProductNotesController constructor.
     */
    public function __construct(AreaProductMeasureNotesService $areaProductMeasureNotes)
    {
        $this->areaProductMeasureNotes = $areaProductMeasureNotes;
    }

    /**
     * @Get(
     *     path="/project/{projectId}/area/{areaId}/products/{productId}/measurements/{measureId}/notes",
     *     description="Get list of notes",
     *     tags={"Project area product measure notes"},
     *     security={{"Auth": {}}},
     *     @Parameter(
     *          name="projectId",
     *          in="path",
     *          description="Project ID",
     *          example=1,
     *          required=true,
     *          allowEmptyValue=false,
     *          @Schema(type="integer")
     *     ),
     *     @Parameter(
     *          name="areaId",
     *          in="path",
     *          description="Project Area ID",
     *          example=1,
     *          required=true,
     *          allowEmptyValue=false,
     *          @Schema(type="integer")
     *     ),
     *     @Parameter(
     *          name="productId",
     *          in="path",
     *          description="Project Area product ID",
     *          example=1,
     *          required=true,
     *          allowEmptyValue=false,
     *          @Schema(type="integer")
     *     ),
     *     @Parameter(
     *          name="measureId",
     *          in="path",
     *          description="Project Area product measure ID",
     *          example=1,
     *          required=true,
     *          allowEmptyValue=false,
     *          @Schema(type="integer")
     *     ),
     *     @Parameter(
     *          name="page",
     *          in="query",
     *          description="Notes page number",
     *          example=1,
     *          required=false,
     *          allowEmptyValue=true,
     *          @Schema(type="integer")
     *     ),
     *     @Parameter(
     *          name="limit",
     *          in="query",
     *          description="Number of notes per page",
     *          example=1,
     *          required=false,
     *          allowEmptyValue=true,
     *          @Schema(type="integer")
     *     ),
     *     @Response(
     *          response="200",
     *          description="Example list of notes",
     *          @JsonContent(
     *                  allOf={
     *                      @Schema(title="data", @Property(property="data", type="array", @Items(ref="#/components/schemas/ProjectAreaProductNoteResponse"))),
     *                      @Schema(title="total", @Property(property="total", title="total", type="integer", example="200")),
     *                  }
     *          )
     *     ),
     *     @Response(response="401", description="Error: Unauthorized"),
     *     @Response(response="500", description="Internal Server Error.")
     * )
     */
    public function index(int $projectId, int $areaId, int $productId, int $measureId, GetProjectAreaProductNote $request)
    {
        $notes = $this->areaProductMeasureNotes->getNotes($projectId, $areaId, $productId, $measureId, $request);

        return $notes ? response()->json($notes) : response()->json(['message' => 'Not found'], 404);
    }

    /**
     * @Post(
     *     path="/project/{projectId}/area/{areaId}/products/{productId}/measurements/{measureId}/notes",
     *     description="Add a new note",
     *     tags={"Project area product measure notes"},
     *     security={{"Auth": {}}},
     *     @Parameter(
     *          name="projectId",
     *          in="path",
     *          description="Project ID",
     *          example=1,
     *          required=true,
     *          allowEmptyValue=false,
     *          @Schema(type="integer")
     *     ),
     *     @Parameter(
     *          name="areaId",
     *          in="path",
     *          description="Project Area ID",
     *          example=1,
     *          required=true,
     *          allowEmptyValue=false,
     *          @Schema(type="integer")
     *     ),
     *     @Parameter(
     *          name="productId",
     *          in="path",
     *          description="Project Area product ID",
     *          example=1,
     *          required=true,
     *          allowEmptyValue=false,
     *          @Schema(type="integer")
     *     ),
     *     @Parameter(
     *          name="measureId",
     *          in="path",
     *          description="Project Area product measure ID",
     *          example=1,
     *          required=true,
     *          allowEmptyValue=false,
     *          @Schema(type="integer")
     *     ),
     *     @RequestBody(
     *         description="Add a new note",
     *         @JsonContent(@Property(property="note", title="note", type="string", example="new note"))
     *     ),
     *     @Response(
     *          response="201",
     *          description="Example of a new note response",
     *          @JsonContent(ref="#/components/schemas/NoteResponse")
     *     ),
     *     @Response(response=422, description="Error: Unprocessable Entity"),
     *     @Response(response=401, description="Error: Unauthorized")
     * )
     */
    public function store(int $projectId, int $areaId, int $productId, int $measureId, AddUpdateNoteRequest $request)
    {
        $note = $this->areaProductMeasureNotes->updateOrCreateNote($request, $projectId, $areaId, $productId, $measureId);

        return $note ? response()->json($note, 201) : response()->json(['message' => 'Not found'], 404);
    }

    /**
     * @Put(
     *     path="/project/{projectId}/area/{areaId}/products/{productId}/measurements/{measureId}/notes/{noteId}",
     *     description="Update note",
     *     tags={"Project area product measure notes"},
     *     security={{"Auth": {}}},
     *     @Parameter(
     *          name="projectId",
     *          in="path",
     *          description="Project ID",
     *          example=1,
     *          required=true,
     *          allowEmptyValue=false,
     *          @Schema(type="integer")
     *     ),
     *     @Parameter(
     *          name="areaId",
     *          in="path",
     *          description="Project Area ID",
     *          example=1,
     *          required=true,
     *          allowEmptyValue=false,
     *          @Schema(type="integer")
     *     ),
     *     @Parameter(
     *          name="productId",
     *          in="path",
     *          description="Project Area product ID",
     *          example=1,
     *          required=true,
     *          allowEmptyValue=false,
     *          @Schema(type="integer")
     *     ),
     *     @Parameter(
     *          name="measureId",
     *          in="path",
     *          description="Project Area product measure ID",
     *          example=1,
     *          required=true,
     *          allowEmptyValue=false,
     *          @Schema(type="integer")
     *     ),
     *     @Parameter(
     *          name="noteId",
     *          in="path",
     *          description="Note ID",
     *          example=1,
     *          required=true,
     *          allowEmptyValue=false,
     *          @Schema(type="integer")
     *     ),
     *     @RequestBody(
     *         description="Update note by id",
     *         @JsonContent(@Property(property="note", title="note", type="string", example="update note"))
     *     ),
     *     @Response(
     *          response="201",
     *          description="Example of note response",
     *          @JsonContent(ref="#/components/schemas/NoteResponse")
     *     ),
     *     @Response(response=422, description="Error: Unprocessable Entity"),
     *     @Response(response=401, description="Error: Unauthorized")
     * )
     */
    public function update(int $projectId, int $areaId, int $productId, int $measureId, int $noteId, AddUpdateNoteRequest $request)
    {
        $updated = $this->areaProductMeasureNotes->updateOrCreateNote($request, $projectId, $areaId, $productId, $measureId, $noteId);

        return $updated ? response()->json($updated) : response()->json(['message' => 'Not found'], 404);
    }


    /**
     * @Delete(
     *     path="/project/{projectId}/area/{areaId}/products/{productId}/measurements/{measureId}/notes/{noteId}",
     *     description="Delete note by id",
     *     tags={"Project area product measure notes"},
     *     security={{"Auth": {}}},
     *     @Parameter(
     *          name="projectId",
     *          in="path",
     *          description="Project ID",
     *          example=1,
     *          required=true,
     *          allowEmptyValue=false,
     *          @Schema(type="integer")
     *     ),
     *     @Parameter(
     *          name="areaId",
     *          in="path",
     *          description="Project Area ID",
     *          example=1,
     *          required=true,
     *          allowEmptyValue=false,
     *          @Schema(type="integer")
     *     ),
     *     @Parameter(
     *          name="productId",
     *          in="path",
     *          description="Project Area product ID",
     *          example=1,
     *          required=true,
     *          allowEmptyValue=false,
     *          @Schema(type="integer")
     *     ),
     *     @Parameter(
     *          name="measureId",
     *          in="path",
     *          description="Project Area product measure ID",
     *          example=1,
     *          required=true,
     *          allowEmptyValue=false,
     *          @Schema(type="integer")
     *     ),
     *     @Parameter(
     *          name="noteId",
     *          in="path",
     *          description="Project Area product note ID",
     *          example=1,
     *          required=true,
     *          allowEmptyValue=false,
     *          @Schema(type="integer")
     *     ),
     *     @Response(response=204, description="No content"),
     *     @Response(response=401, description="Error: Unauthorized"),
     *     @Response(response=422, description="Error: Unprocessable Entity"),
     * )
     */
    public function destroy(int $projectId, int $areaId, int $productId, int $measureId, int $noteId)
    {
        return $this->areaProductMeasureNotes->deleteNote($projectId, $areaId, $productId, $measureId, $noteId)
            ? response()->json([], 204)
            : response()->json(['message' => 'Not found'], 404);
    }
    /**
     * @Get(
     *     path="/notes/{measureId}",
     *     description="Get list of notes by measureId",
     *     tags={"Project area product measure notes"},
     *     security={{"Auth": {}}},
     *     @Parameter(
     *          name="measureId",
     *          in="path",
     *          description="Measure ID",
     *          example=1,
     *          required=true,
     *          allowEmptyValue=false,
     *          @Schema(type="integer")
     *     ),
     *     @Response(
     *          response="200",
     *          description="Example list of notes",
     *          @JsonContent(
     *                  allOf={
     *                      @Schema(title="data", @Property(property="data", type="array", @Items(ref="#/components/schemas/ProjectAreaProductNoteResponse"))),
     *                      @Schema(title="total", @Property(property="total", title="total", type="integer", example="200")),
     *                  }
     *          )
     *     ),
     *     @Response(response="401", description="Error: Unauthorized"),
     *     @Response(response="500", description="Internal Server Error.")
     * )
     */
    public function getNotesByMeasureId( int $measureId)
    {
        $notes = $this->areaProductMeasureNotes->getNotesByMeasureId($measureId);

        return $notes ? response()->json($notes) : response()->json(['message' => 'Not found'], 404);
    }
}
