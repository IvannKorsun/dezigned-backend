<?php

namespace App\Http\Controllers\Api\Project;

use App\Http\Controllers\Controller;
use App\Http\Requests\Project\AddAreaProductMeasureRequest;
use App\Http\Requests\Project\GetInstallersRequest;
use App\Http\Requests\Project\UpdateAreaProductMeasureRequest;
use App\Http\Requests\Project\UpdateProductMeasureInstallerRequest;
use App\Services\Project\AreaProductMeasureService;
use OpenApi\Annotations\Get;
use OpenApi\Annotations\Items;
use OpenApi\Annotations\JsonContent;
use OpenApi\Annotations\MediaType;
use OpenApi\Annotations\Parameter;
use OpenApi\Annotations\Post;
use OpenApi\Annotations\Property;
use OpenApi\Annotations\Put;
use OpenApi\Annotations\RequestBody;
use OpenApi\Annotations\Response;
use OpenApi\Annotations\Schema;

class ProductMeasureController extends Controller
{
    /**
     * @var AreaProductMeasureService
     */
    private $measureService;

    public function __construct(AreaProductMeasureService $measureService)
    {
        $this->measureService = $measureService;
    }

    /**
     * @Post(
     *     path="/project/area/products/measurements",
     *     description="Add a new project area product measure",
     *     tags={"Project area product measure"},
     *     security={{"Auth": {}}},
     *     @RequestBody(
     *         description="Add a new project area product measure",
     *          @MediaType(
     *              mediaType="application/json",
     *              @JsonContent(ref="#/components/schemas/AreaProductMeasureRequest")
     *          )
     *     ),
     *     @Response(response=201, description="Project area created successfully", @JsonContent(ref="#/components/schemas/ProjectMeasureDetails")),
     *     @Response(response=422, description="Error: Unprocessable Entity"),
     *     @Response(response=401, description="Error: Unauthorized")
     * )
     */

    public function addMeasure(AddAreaProductMeasureRequest $request)
    {
        $measure = $this->measureService->addMeasure($request);

        return $measure
            ? response()->json($this->measureService->addMeasure($request))
            : response()->json(['message' => 'Not found'], 404);
    }

    /**
     * @Put(
     *     path="/project/area/products/measurements/{measureId}",
     *     description="Update product measure details",
     *     tags={"Project area product measure"},
     *     security={{"Auth": {}}},
     *     @Parameter(
     *          name="measureId",
     *          in="path",
     *          description="Measure ID",
     *          example=1,
     *          required=true,
     *          allowEmptyValue=false,
     *          @Schema(type="integer")
     *     ),
     *     @RequestBody(
     *         description="Update measure",
     *         @JsonContent(
     *              allOf={
     *                  @Schema(ref="#/components/schemas/AreaProductMeasureRequest"),
     *                  @Schema(@Property(property="details", type="string", example="Updated details")),
     *                  @Schema(@Property(property="installer_id", type="integer", example=1)),
     *                  @Schema(@Property(property="step", type="integer", example="1 or 2"))
     *              }
     *         )
     *     ),
     *     @Response(
     *          response="200",
     *          description="Example product measure update details response",
     *          @JsonContent(ref="#/components/schemas/ProjectMeasureDetails")
     *     ),
     *     @Response(response=422, description="Error: Unprocessable Entity"),
     *     @Response(response=401, description="Error: Unauthorized")
     * )
     */
    public function updateMeasure(int $measureId, UpdateAreaProductMeasureRequest $request)
    {
        $updated = $this->measureService->updateMeasure($measureId, $request);

        return $updated ? response()->json($updated) : response()->json(['message' => 'Not found'], 404);
    }

    /**
     * @Get(
     *     path="/project/area/products/measurements/get-installers",
     *     description="Select another product measure installer",
     *     tags={"Project area product measure"},
     *     security={{"Auth": {}}},
     *     @Parameter(
     *          name="lat",
     *          in="query",
     *          description="Latitude value",
     *          example=-90,
     *          required=true,
     *          allowEmptyValue=false,
     *          @Schema(type="integer")
     *     ),
     *     @Parameter(
     *          name="lng",
     *          in="query",
     *          description="Longitude value",
     *          example=-180,
     *          required=true,
     *          allowEmptyValue=false,
     *          @Schema(type="integer")
     *     ),
     *     @Response(
     *          response="200",
     *          description="Example list of installer",
     *          @JsonContent(@Property(property="data", type="array", @Items(ref="#/components/schemas/InstallerResponse")))
     *     ),
     *     @Response(response=422, description="Error: Unprocessable Entity"),
     *     @Response(response=401, description="Error: Unauthorized")
     * )
     */
    public function getInstallers(GetInstallersRequest $request)
    {
        return response()->json($this->measureService->getInstallers($request->get('lat'), $request->get('lng')));
    }
}
