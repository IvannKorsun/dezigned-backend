<?php

namespace App\Http\Controllers\Api\Project;

use App\Http\Requests\AddUpdateNoteRequest;
use App\Http\Requests\Project\GetProjectAreaProductNote;
use App\Services\Project\AreaProductNotesService;
use App\Http\Controllers\Controller;
use OpenApi\Annotations\Delete;
use OpenApi\Annotations\Get;
use OpenApi\Annotations\Items;
use OpenApi\Annotations\JsonContent;
use OpenApi\Annotations\Parameter;
use OpenApi\Annotations\Post;
use OpenApi\Annotations\Property;
use OpenApi\Annotations\Put;
use OpenApi\Annotations\RequestBody;
use OpenApi\Annotations\Response;
use OpenApi\Annotations\Schema;

class AreaProductNotesController extends Controller
{
    /**
     * @var AreaProductNotesService
     */
    private $areaProductNotes;

    /**
     * AreaProductNotesController constructor.
     */
    public function __construct(AreaProductNotesService $areaProductNotes)
    {
        $this->areaProductNotes = $areaProductNotes;
    }

    /**
     * @Get(
     *     path="/project/{projectId}/area/{areaId}/products/{productId}/notes",
     *     description="Get list of project area product notes",
     *     tags={"Project area product notes"},
     *     security={{"Auth": {}}},
     *     @Parameter(
     *          name="projectId",
     *          in="path",
     *          description="Project ID",
     *          example=1,
     *          required=true,
     *          allowEmptyValue=false,
     *          @Schema(type="integer")
     *     ),
     *     @Parameter(
     *          name="areaId",
     *          in="path",
     *          description="Project Area ID",
     *          example=1,
     *          required=true,
     *          allowEmptyValue=false,
     *          @Schema(type="integer")
     *     ),
     *     @Parameter(
     *          name="productId",
     *          in="path",
     *          description="Project Area product ID",
     *          example=1,
     *          required=true,
     *          allowEmptyValue=false,
     *          @Schema(type="integer")
     *     ),
     *     @Parameter(
     *          name="page",
     *          in="query",
     *          description="Notes page number",
     *          example=1,
     *          required=false,
     *          allowEmptyValue=true,
     *          @Schema(type="integer")
     *     ),
     *     @Parameter(
     *          name="limit",
     *          in="query",
     *          description="Number of notes per page",
     *          example=1,
     *          required=false,
     *          allowEmptyValue=true,
     *          @Schema(type="integer")
     *     ),
     *     @Response(
     *          response="200",
     *          description="Example list of project area product notes",
     *          @JsonContent(
     *                  allOf={
     *                      @Schema(title="title", @Property(property="title", title="title", type="string", example="Product name")),
     *                      @Schema(title="data", @Property(property="data", type="array", @Items(ref="#/components/schemas/ProjectAreaProductNoteResponse"))),
     *                      @Schema(title="total", @Property(property="total", title="total", type="integer", example="200")),
     *                  }
     *          )
     *     ),
     *     @Response(response="401", description="Error: Unauthorized"),
     *     @Response(response="500", description="Internal Server Error.")
     * )
     */
    public function index(int $projectId, int $areaId, int $productId, GetProjectAreaProductNote $request)
    {
        $notes = $this->areaProductNotes->getNotes($projectId, $areaId, $productId, $request);

        return $notes ? response()->json($notes) : response()->json(['message' => 'Not found'], 404);
    }

    /**
     * @Post(
     *     path="/project/{projectId}/area/{areaId}/products/{productId}/notes",
     *     description="Add new project area product note",
     *     tags={"Project area product notes"},
     *     security={{"Auth": {}}},
     *     @Parameter(
     *          name="projectId",
     *          in="path",
     *          description="Project ID",
     *          example=1,
     *          required=true,
     *          allowEmptyValue=false,
     *          @Schema(type="integer")
     *     ),
     *     @Parameter(
     *          name="areaId",
     *          in="path",
     *          description="Project Area ID",
     *          example=1,
     *          required=true,
     *          allowEmptyValue=false,
     *          @Schema(type="integer")
     *     ),
     *     @Parameter(
     *          name="productId",
     *          in="path",
     *          description="Project Area product ID",
     *          example=1,
     *          required=true,
     *          allowEmptyValue=false,
     *          @Schema(type="integer")
     *     ),
     *     @RequestBody(
     *         description="Add a new project area product note",
     *         @JsonContent(@Property(property="note", title="note", type="string", example="new project area product note"))
     *     ),
     *     @Response(
     *          response="201",
     *          description="Example of add a new project area product note response",
     *          @JsonContent(ref="#/components/schemas/ProjectAreaProductNoteResponse")
     *     ),
     *     @Response(response=422, description="Error: Unprocessable Entity"),
     *     @Response(response=401, description="Error: Unauthorized")
     * )
     */
    public function store(int $projectId, int $areaId, int $productId, AddUpdateNoteRequest $request)
    {
        $note = $this->areaProductNotes->updateOrCreateNote($request, $projectId, $areaId, $productId);

        return $note ? response()->json($note, 201) : response()->json(['message' => 'Not found'], 404);
    }

    /**
     * @Put(
     *     path="/project/{projectId}/area/{areaId}/products/{productId}/notes/{noteId}",
     *     description="Add new project area product note",
     *     tags={"Project area product notes"},
     *     security={{"Auth": {}}},
     *     @Parameter(
     *          name="projectId",
     *          in="path",
     *          description="Project ID",
     *          example=1,
     *          required=true,
     *          allowEmptyValue=false,
     *          @Schema(type="integer")
     *     ),
     *     @Parameter(
     *          name="areaId",
     *          in="path",
     *          description="Project Area ID",
     *          example=1,
     *          required=true,
     *          allowEmptyValue=false,
     *          @Schema(type="integer")
     *     ),
     *     @Parameter(
     *          name="productId",
     *          in="path",
     *          description="Project Area product ID",
     *          example=1,
     *          required=true,
     *          allowEmptyValue=false,
     *          @Schema(type="integer")
     *     ),
     *     @Parameter(
     *          name="noteId",
     *          in="path",
     *          description="Project Area product note ID",
     *          example=1,
     *          required=true,
     *          allowEmptyValue=false,
     *          @Schema(type="integer")
     *     ),
     *     @RequestBody(
     *         description="Update a project area product note",
     *         @JsonContent(@Property(property="note", title="note", type="string", example="update a project area product note"))
     *     ),
     *     @Response(
     *          response="201",
     *          description="Example of update a project area product note response",
     *          @JsonContent(ref="#/components/schemas/ProjectAreaProductNoteResponse")
     *     ),
     *     @Response(response=422, description="Error: Unprocessable Entity"),
     *     @Response(response=401, description="Error: Unauthorized")
     * )
     */
    public function update(int $projectId, int $areaId, int $productId, int $noteId, AddUpdateNoteRequest $request)
    {
        $updated = $this->areaProductNotes->updateOrCreateNote($request, $projectId, $areaId, $productId, $noteId);

        return $updated ? response()->json($updated) : response()->json(['message' => 'Not found'], 404);
    }


    /**
     * @Delete(
     *     path="/project/{projectId}/area/{areaId}/products/{productId}/notes/{noteId}",
     *     description="Delete a project area product note by id",
     *     tags={"Project area product notes"},
     *     security={{"Auth": {}}},
     *     @Parameter(
     *          name="projectId",
     *          in="path",
     *          description="Project ID",
     *          example=1,
     *          required=true,
     *          allowEmptyValue=false,
     *          @Schema(type="integer")
     *     ),
     *     @Parameter(
     *          name="areaId",
     *          in="path",
     *          description="Project Area ID",
     *          example=1,
     *          required=true,
     *          allowEmptyValue=false,
     *          @Schema(type="integer")
     *     ),
     *     @Parameter(
     *          name="productId",
     *          in="path",
     *          description="Project Area product ID",
     *          example=1,
     *          required=true,
     *          allowEmptyValue=false,
     *          @Schema(type="integer")
     *     ),
     *     @Parameter(
     *          name="noteId",
     *          in="path",
     *          description="Project Area product note ID",
     *          example=1,
     *          required=true,
     *          allowEmptyValue=false,
     *          @Schema(type="integer")
     *     ),
     *     @Response(response=204, description="No content"),
     *     @Response(response=401, description="Error: Unauthorized"),
     *     @Response(response=422, description="Error: Unprocessable Entity"),
     * )
     */
    public function destroy(int $projectId, int $areaId, int $productId, int $noteId)
    {
        return $this->areaProductNotes->deleteNote($projectId, $areaId, $productId, $noteId)
            ? response()->json([], 204)
            : response()->json(['message' => 'Not found'], 404);
    }
}
