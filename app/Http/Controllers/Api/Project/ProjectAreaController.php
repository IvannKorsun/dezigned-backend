<?php

namespace App\Http\Controllers\Api\Project;

use App\Http\Requests\FilterRequest;
use App\Http\Requests\Project\AddAreaProductRequest;
use App\Http\Requests\Project\AreaProductListRequest;
use App\Http\Requests\Project\UpdateAreaProductRequest;
use App\Http\Requests\Project\ProjectAreaCreateRequest;
use App\Http\Requests\Project\ProjectAreaUpdateRequest;
use App\Services\Project\ProjectAreaService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use OpenApi\Annotations\Delete;
use OpenApi\Annotations\Get;
use OpenApi\Annotations\Items;
use OpenApi\Annotations\JsonContent;
use OpenApi\Annotations\MediaType;
use OpenApi\Annotations\Parameter;
use OpenApi\Annotations\Post;
use OpenApi\Annotations\Property;
use OpenApi\Annotations\Put;
use OpenApi\Annotations\RequestBody;
use OpenApi\Annotations\Response;
use OpenApi\Annotations\Schema;

class ProjectAreaController extends Controller
{

    /**
     * @var ProjectAreaService
     */
    private $projectAreaService;

    public function __construct(ProjectAreaService $projectAreaService)
    {
        $this->projectAreaService = $projectAreaService;
    }

    /**
     * @Get(
     *     path="/project/{projectId}/area",
     *     description="Get list of project areas",
     *     tags={"Project area"},
     *     security={{"Auth": {}}},
     *     @Parameter(
     *          name="projectId",
     *          in="path",
     *          description="Project id",
     *          example=1,
     *          required=true,
     *          allowEmptyValue=false,
     *          @Schema(type="integer")
     *     ),
     *     @Parameter(
     *          name="search",
     *          in="query",
     *          description="Text search word. Search by project area name",
     *          example="project name",
     *          required=false,
     *          allowEmptyValue=true,
     *          @Schema(type="string")
     *     ),
     *     @Parameter(
     *          name="page",
     *          in="query",
     *          description="Number of projects areas page",
     *          example="1",
     *          required=false,
     *          allowEmptyValue=true,
     *          @Schema(type="int")
     *     ),
     *     @Parameter(
     *          name="limit",
     *          in="query",
     *          description="Number of projects areas per page",
     *          example=16,
     *          required=false,
     *          allowEmptyValue=true,
     *          @Schema(type="int")
     *     ),
     *     @Response(response="403", description="User does not have the right roles."),
     *     @Response(
     *          response="200",
     *          description="Example project areas list response",
     *          @JsonContent(
     *                  allOf={
     *                      @Schema(
     *                          title="data",
     *                          @Property(type="array", property="data", @\OpenApi\Annotations\Items(ref="#/components/schemas/ProjectAreaResponse")),
     *                      ),
     *                      @Schema(
     *                          title="total",
     *                          @Property(title="total", property="total", type="integer", example="10")
     *                      )
     *                  }
     *          )
     *     ),
     *     @Response(response="401", description="Error: Unauthorized"),
     *     @Response(response="404", description="Project not found."),
     *     @Response(response="500", description="Internal Server Error.")
     * )
     */
    public function index(int $projectId, Request $request)
    {
        $areas = $this->projectAreaService->getAreas($projectId, $request);
        return $areas ? response()->json($areas) : response()->json(['message' => 'Not found'], 404);
    }

    /**
     * @Post(
     *     path="/project/{projectId}/area",
     *     description="Add a new project area",
     *     tags={"Project area"},
     *     security={{"Auth": {}}},
     *     @Parameter(
     *          name="projectId",
     *          in="path",
     *          description="Project id",
     *          example=1,
     *          required=true,
     *          allowEmptyValue=false,
     *          @Schema(type="integer")
     *     ),
     *     @RequestBody(
     *         description="Add a new project area",
     *          @MediaType(
     *              mediaType="application/json",
     *              @JsonContent(ref="#/components/schemas/ProjectAreaCreateRequest")
     *          )
     *     ),
     *     @Response(response=201, description="Project area created successfully", @JsonContent(ref="#/components/schemas/ProjectAreaResponse")),
     *     @Response(response=422, description="Error: Unprocessable Entity"),
     *     @Response(response=401, description="Error: Unauthorized")
     * )
     */
    public function store(int $projectId, ProjectAreaCreateRequest $request)
    {
        $area = $this->projectAreaService->createArea($projectId, $request);

        return $area ? response()->json($area) : response()->json(['message' => 'Not found'], 404);
    }

    /**
     * @Get(
     *     path="/project/{projectId}/area/{id}",
     *     description="Get project area details by project area id",
     *     tags={"Project area"},
     *     security={{"Auth": {}}},
     *     @Parameter(
     *          name="projectId",
     *          in="path",
     *          description="Project id",
     *          example=1,
     *          required=true,
     *          allowEmptyValue=false,
     *          @Schema(type="integer")
     *     ),
     *     @Parameter(
     *          name="id",
     *          in="path",
     *          description="Project area id",
     *          example=1,
     *          required=true,
     *          allowEmptyValue=false,
     *          @Schema(type="integer")
     *     ),
     *     @Response(
     *          response="200",
     *          description="Project area details by project area id",
     *          @JsonContent(ref="#/components/schemas/ProjectAreaResponse")
     *     ),
     *     @Response(response="401", description="Error: Unauthorized"),
     *     @Response(response="404", description="Project area or project not found."),
     * )
     */
    public function show($projectId, $id)
    {
        $area = $this->projectAreaService->getArea($projectId, $id);

        return $area ? response()->json($area) : response()->json(['message' => 'Not found'], 404);
    }
    /**
     * @Put(
     *     path="/project/{projectId}/area/{id}",
     *     description="Update project area by id",
     *     tags={"Project area"},
     *     security={{"Auth": {}}},
     *     @Parameter(
     *          name="projectId",
     *          in="path",
     *          description="Project id",
     *          example=1,
     *          required=true,
     *          allowEmptyValue=false,
     *          @Schema(type="integer")
     *     ),
     *     @Parameter(
     *          name="id",
     *          in="path",
     *          description="Project area id",
     *          example=1,
     *          required=true,
     *          allowEmptyValue=false,
     *          @Schema(type="integer")
     *     ),
     *     @RequestBody(
     *          @MediaType(
     *              mediaType="application/json",
     *              @Schema(ref="#/components/schemas/ProjectAreaCreateRequest")
     *          )
     *     ),
     *     @Response(
     *          response="200",
     *          description="Updated project area details by id",
     *          @JsonContent(ref="#/components/schemas/ProjectAreaResponse")
     *     ),
     *     @Response(response="401", description="Error: Unauthorized"),
     *     @Response(response="404", description="Project area or project not found."),
     *     @Response(response="500", description="Internal Server Error.")
     * )
     */
    public function update($projectId, $id, ProjectAreaUpdateRequest $request)
    {
        $area = $this->projectAreaService->updateArea($projectId, $id, $request);

        return $area ? response()->json($area) : response()->json(['message' => 'Not found'], 404);
    }

    /**
     * @Delete(
     *     path="/project/{projectId}/area/{id}",
     *     description="Delete project area by id",
     *     tags={"Project area"},
     *     security={{"Auth": {}}},
     *     @Parameter(
     *          name="projectId",
     *          in="path",
     *          description="Project id",
     *          example=1,
     *          required=true,
     *          allowEmptyValue=false,
     *          @Schema(type="integer")
     *     ),
     *     @Parameter(
     *          name="id",
     *          in="path",
     *          description="Project area id",
     *          example=1,
     *          required=true,
     *          allowEmptyValue=false,
     *          @Schema(type="integer")
     *     ),
     *     @Response(response=204, description="No content"),
     *     @Response(response="404", description="Project area or project not found."),
     *     @Response(response=401, description="Error: Unauthorized")
     * )
     */
    public function destroy($projectId, $id)
    {
        $area = $this->projectAreaService->deleteArea($projectId, $id);

        return $area
            ? response()->json(['message' => 'Project successfully deleted'], 204)
            : response()->json(['message' => 'Not found'], 404);
    }
    /**
     * @Get(
     *     path="/project/{projectId}/area/{areaId}/products",
     *     description="Get list of products added to project area",
     *     tags={"Project area products"},
     *     security={{"Auth": {}}},
     *     @Parameter(
     *          name="projectId",
     *          in="path",
     *          description="Project id",
     *          example=1,
     *          required=true,
     *          allowEmptyValue=false,
     *          @Schema(type="integer")
     *     ),
     *     @Parameter(
     *          name="areaId",
     *          in="path",
     *          description="Project area id",
     *          example=1,
     *          required=true,
     *          allowEmptyValue=false,
     *          @Schema(type="integer")
     *     ),
     *     @Parameter(
     *          name="search",
     *          in="query",
     *          description="Text search word. Found in the name, description, or sku fields, or in the brand name",
     *          example="product name",
     *          required=false,
     *          allowEmptyValue=true,
     *          @Schema(type="string")
     *     ),
     *     @Parameter(
     *          name="page",
     *          in="query",
     *          description="Number of projects area product page",
     *          example="1",
     *          required=false,
     *          allowEmptyValue=true,
     *          @Schema(type="int")
     *     ),
     *     @Parameter(
     *          name="limit",
     *          in="query",
     *          description="Number of products per page",
     *          example=16,
     *          required=false,
     *          allowEmptyValue=true,
     *          @Schema(type="int")
     *     ),
     *     @Response(response="403", description="User does not have the right roles."),
     *     @Response(
     *          response="200",
     *          description="Example product list response",
     *          @JsonContent(
     *                  allOf={
     *                      @Schema(
     *                          title="products",
     *                          @Property(
     *                              property="products",
     *                              type="array",
     *                              @Items(
     *                                  allOf={
     *                                      @Schema(ref="#/components/schemas/ProductResponse"),
     *                                      @Schema(
     *                                          @Property(
     *                                              property="qty",
     *                                              title="Product quantity",
     *                                              description="Number of products added to project area",
     *                                              format="integer",
     *                                              example="100"
     *                                          )
     *                                      )
     *                                  }
     *                              ),
     *                          )
     *                      ),
     *                      @Schema(
     *                          title="meta",
     *                          type="object",
     *                          @Property(title="meta", property="meta", type="object", ref="#/components/schemas/BcMetaResponse")
     *                      ),
     *                  }
     *          )
     *     ),
     *     @Response(response="401", description="Error: Unauthorized"),
     *     @Response(response="404", description="Not found."),
     *     @Response(response="500", description="Internal Server Error.")
     * )
     */
    public function getProducts($projectId, $id, AreaProductListRequest $request)
    {
        $products = $this->projectAreaService->getProducts($projectId, $id, $request);

        return $products ? response()->json($products) : response()->json(['message' => 'Not found'], 404);
    }

    /**
     * @Post(
     *     path="/project/{projectId}/area/products",
     *     description="Add product to project area",
     *     tags={"Project area products"},
     *     security={{"Auth": {}}},
     *     @Parameter(
     *          name="projectId",
     *          in="path",
     *          description="Project id",
     *          example=1,
     *          required=true,
     *          allowEmptyValue=false,
     *          @Schema(type="integer")
     *     ),
     *     @RequestBody(
     *          @MediaType(
     *              mediaType="application/json",
     *              @Schema(ref="#/components/schemas/AddProductRequest")
     *          )
     *     ),
     *     @Response(
     *          response="200",
     *          description="Add product to project area",
     *          @JsonContent(
     *            example={
     *                "message":"Product Added!"
     *              }
     *          )
     *     ),
     *     @Response(response="401", description="Error: Unauthorized"),
     *     @Response(response="404", description="Project area or project not found."),
     *     @Response(response="500", description="Internal Server Error.")
     * )
     */
    public function addProduct(int $projectId, AddAreaProductRequest $request)
    {
        $product = $this->projectAreaService->addProduct($projectId, $request);

        return $product ? response()->json(['message' => 'Product Added!'], 201) : response()->json(['message' => 'Not found'], 404);
    }

    /**
     * @Put(
     *     path="/project/{projectId}/area/{areaId}/products",
     *     description="Update product quantity added in project area",
     *     tags={"Project area products"},
     *     security={{"Auth": {}}},
     *     @Parameter(
     *          name="projectId",
     *          in="path",
     *          description="Project id",
     *          example=1,
     *          required=true,
     *          allowEmptyValue=false,
     *          @Schema(type="integer")
     *     ),
     *     @Parameter(
     *          name="areaId",
     *          in="path",
     *          description="Project area id",
     *          example=1,
     *          required=true,
     *          allowEmptyValue=false,
     *          @Schema(type="integer")
     *     ),
     *     @RequestBody(
     *          @MediaType(
     *              mediaType="application/json",
     *              @Schema(ref="#/components/schemas/UpdateProductRequest")
     *          )
     *     ),
     *     @Response(
     *          response="200",
     *          description="Updated project area details by id",
     *          @JsonContent(
     *            example={
     *                "message":"Product Updated!"
     *              }
     *          )
     *     ),
     *     @Response(response="401", description="Error: Unauthorized"),
     *     @Response(response="404", description="Project area or project not found."),
     *     @Response(response="500", description="Internal Server Error.")
     * )
     */
    public function updateProduct(int $projectId, int $id, UpdateAreaProductRequest $request)
    {
        $product = $this->projectAreaService->updateProduct($projectId, $id, $request);

        return $product ? response()->json(['message' => 'Product Updated!'], 200) : response()->json(['message' => 'Not found'], 404);
    }

    /**
     * @Delete(
     *     path="/project/{projectId}/area/{areaId}/products/{productId}",
     *     description="Delete project area by id",
     *     tags={"Project area products"},
     *     security={{"Auth": {}}},
     *     @Parameter(
     *          name="projectId",
     *          in="path",
     *          description="Project id",
     *          example=1,
     *          required=true,
     *          allowEmptyValue=false,
     *          @Schema(type="integer")
     *     ),
     *     @Parameter(
     *          name="areaId",
     *          in="path",
     *          description="Project area id",
     *          example=1,
     *          required=true,
     *          allowEmptyValue=false,
     *          @Schema(type="integer")
     *     ),
     *     @Parameter(
     *          name="productId",
     *          in="path",
     *          description="Product id added in project area ",
     *          example=1,
     *          required=true,
     *          allowEmptyValue=false,
     *          @Schema(type="integer")
     *     ),
     *     @Response(response=204, description="No content"),
     *     @Response(response="404", description="Not found."),
     *     @Response(response=401, description="Error: Unauthorized")
     * )
     */
    public function deleteProduct(int $projectId, int $areaId, int $id)
    {
        $product = $this->projectAreaService->deleteProduct($projectId, $areaId, $id);

        return $product ? response()->json(['message' => 'Product deleted!'], 204) : response()->json(['message' => 'Not found'], 404);
    }
}
