<?php

namespace App\Http\Controllers\Api\Auth;

use App\Events\UserCreated;
use App\Http\Requests\Auth\RegisterUserRequest;
use App\Jobs\Email\SignupActivateEmail;
use App\Models\PasswordReset;
use App\Models\User;
use Carbon\Carbon;
use App\Http\Controllers\Controller;
use Illuminate\Support\Str;
use OpenApi\Annotations\Get;
use OpenApi\Annotations\JsonContent;
use OpenApi\Annotations\Parameter;
use OpenApi\Annotations\Post;
use OpenApi\Annotations\Property;
use OpenApi\Annotations\RequestBody;
use OpenApi\Annotations\Response;
use OpenApi\Annotations\Schema;

class RegisterController extends Controller
{
    /**
     * @Post(
     *     path="/register",
     *     description="Register user",
     *     tags={"Auth"},
     *     @RequestBody(
     *         description="Registering user by form fields and send confirmation email message",
     *         required=true,
     *         @JsonContent(ref="#/components/schemas/UserRegisterRequest")
     *     ),
     *     @Response(response="201", description="User successfully created!"),
     *     @Response(response="422", description="Error: Unprocessable Entity"),
     * )
     */
    public function register(RegisterUserRequest $request)
    {
        $input = $request->all();

        $input['activation_token'] = Str::random(60);

        /** @var User $user */
        $user = User::create($input);

        $user->assignRole($input['role']);

        event(new UserCreated($user));

        SignupActivateEmail::dispatch($user);

        return response()->json(['message' => 'User successfully created!'], 201);
    }

    /**
     * @Get(
     *     path="/register/confirm/{token}",
     *     description="Confirmation email. Used for confirmation user email.",
     *     tags={"Auth"},
     *     @Parameter(
     *          name="token",
     *          in="path",
     *          description="Token for verify user email",
     *          example="eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6ImQzYTkxM...",
     *          required=true,
     *          @Schema(type="string")
     *     ),
     *     @Response(response="404", description="This activation token is invalid."),
     *     @Response(response="302", description="Redirect to set user password")
     * )
     */
    public function registerConfirm(string $token)
    {
        $user = User::where('activation_token', $token)->first();

        if (!$user) {
            return response()->json([
                'message' => 'This activation token is invalid.'
            ], 404);
        }

        $user->active = true;
        $user->activation_token = '';
        $user->save();

        $passwordReset = PasswordReset::updateOrCreate(
            ['email' => $user->email],
            [
                'email' => $user->email,
                'token' => Str::random(60),
                'expires_at' => Carbon::now()->addYear()
            ]
        );

        return redirect(env('SET_PASS_REDIRECT_URL') . '/' . $passwordReset->token);
    }
}
