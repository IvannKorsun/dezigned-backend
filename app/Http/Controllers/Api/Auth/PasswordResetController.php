<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\SetPasswordRequest;
use App\Http\Requests\Password\ForgotPasswordRequest;
use App\Jobs\Email\PasswordResetRequest;
use App\Jobs\Email\PasswordResetSuccess;
use App\Models\PasswordReset;
use App\Services\OauthTokenService;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Str;
use OpenApi\Annotations\Get;
use OpenApi\Annotations\JsonContent;
use OpenApi\Annotations\Parameter;
use OpenApi\Annotations\Post;
use OpenApi\Annotations\Property;
use OpenApi\Annotations\RequestBody;
use OpenApi\Annotations\Response;
use OpenApi\Annotations\Schema;

class PasswordResetController extends Controller
{

    /**
     * @Post(
     *     path="/password/forgot",
     *     description="Forgot password request by user email or name",
     *     tags={"Auth"},
     *     @RequestBody(
     *         description="Create forgot password request. Send email, if user with email or name exists",
     *         required=true,
     *         @JsonContent(ref="#/components/schemas/ForgotPasswordRequest")
     *     ),
     *     @Response(response="200", description="We have e-mailed your password reset link!"),
     *     @Response(response="404", description="Registered user not found"),
     *     @Response(response="422", description="Error: Unprocessable Entity"),
     * )
     */
    public function forgot(ForgotPasswordRequest $request)
    {
        $user = User::where('email', $request->get('identifier'))->orWhere('name', $request->get('identifier'))->first();

        if (!$user){
            return response()->json([
                'message' => 'We can\'t find a user with that e-mail address.'
            ], 404);
        }

        $passwordReset = PasswordReset::updateOrCreate(
            ['email' => $user->email],
            [
                'email' => $user->email,
                'token' => Str::random(60),
                'expires_at' => Carbon::now()->addHour()
            ]
        );

        $url = env('SET_PASS_REDIRECT_URL') . '/' . $passwordReset->token;

        PasswordResetRequest::dispatch($url, $user->email);

        return response()->json([
            'message' => 'We have e-mailed your password reset link!'
        ]);
    }

    /**
     * @Post(
     *     path="/password/set",
     *     description="Set or reset user password",
     *     tags={"Auth"},
     *     @RequestBody(
     *         description="Set or reset user password",
     *         required=true,
     *         @JsonContent(ref="#/components/schemas/PasswordSetRequest")
     *     ),
     *     @Response(
     *         response=200,
     *         description="User response",
     *         @JsonContent(
     *              allOf={
     *                  @Schema(ref="#/components/schemas/TokenResponse"),
     *                  @Schema(
     *                      title="user",
     *                      @Property(property="user", title="user", ref="#/components/schemas/UserResponse")
     *                  )
     *              }
     *         )
     *     ),
     *     @Response(response="422", description="Error: Unprocessable Entity"),
     *     @Response(response="404", description="We can't find a user with that e-mail address or This password reset token is invalid.")
     * )
     */
    public function reset(SetPasswordRequest $request, OauthTokenService $tokenService)
    {
        $notify = true;

        /** @var PasswordReset $passwordReset */

        $passwordReset = PasswordReset::where('token', $request->get('token'))->first();

        if (!$passwordReset){
            return response()->json(['message' => 'This password reset token is invalid.'], 404);
        }

        if (Carbon::parse($passwordReset->expires_at)->isPast()){
            $passwordReset->delete();
            return response()->json(['message' => 'This password reset token is invalid.'], 404);
        }

        $user = User::where('email', $passwordReset->email)->first();

        if (!$user){
            return response()->json([
                'message' => 'We can\'t find a user with that e-mail address or name.'
            ], 404);
        }

        if ($user->password === null){
            $notify = false;
        }

        $user->password = $request->password;
        $user->save();


        if ($notify){
            PasswordResetSuccess::dispatch($passwordReset);
        }
        
        $passwordReset->delete();

        $token = $tokenService->getAccessTokens($user, $request);

        return response()->json(array_merge($token, ['user' => $user]), 200);
    }
}
