<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\SocialLoginRequest;
use App\Http\Requests\Auth\SocialRegisterRequest;
use App\Services\OauthTokenService;
use App\Services\SocialAccountsService;
use OpenApi\Annotations\JsonContent;
use OpenApi\Annotations\Parameter;
use OpenApi\Annotations\Post;
use OpenApi\Annotations\Property;
use OpenApi\Annotations\RequestBody;
use OpenApi\Annotations\Response;
use OpenApi\Annotations\Schema;

class SocialController extends Controller
{
    /**
     * @var SocialAccountsService
     */
    private $socialAccountsService;
    /**
     * @var OauthTokenService
     */
    private $tokenService;

    public function __construct(SocialAccountsService $socialAccountsService, OauthTokenService $tokenService)
    {
        $this->socialAccountsService = $socialAccountsService;
        $this->tokenService = $tokenService;
    }

    /**
     * @Post(
     *     path="/login/{provider}",
     *     description="Login user via social networks (Facebook, Google, LinkedIn)",
     *     tags={"Auth"},
     *     @Parameter(
     *          name="provider",
     *          in="path",
     *          description="Social network provider, supports 'facebook', 'google' or 'linkedin'",
     *          example="facebook, google, linkedin",
     *          required=true,
     *          @Schema(type="string")
     *     ),
     *     @RequestBody(
     *         required=true,
     *         @JsonContent(
     *                @Property(
     *                      property="access_token",
     *                      title="access_token",
     *                      type="string",
     *                      example="eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6ImQzYTkxMmU4ZDBiZmI0YWE4NjMxZDQ0NjMyOWQ4MDdlYWYwMTJmM2FjOGFiZTViNTlkMTBiNzJlNzQ4ZWYxNjkyNzBhZWRjYjIwOGVmZDE0In0")
     *         )
     *     ),
     *     @Response(
     *         response=200,
     *         description="User login response",
     *         @JsonContent(
     *              allOf={
     *                  @Schema(ref="#/components/schemas/TokenResponse"),
     *                  @Schema(
     *                      title="user",
     *                      @Property(property="user", title="user", ref="#/components/schemas/UserResponse")
     *                  )
     *              }
     *         )
     *     ),
     *     @Response(response="422", description="Error: Unprocessable Entity"),
     *     @Response(response="404", description="User not found")
     * )
     */
    public function login(string $provider, SocialLoginRequest $request)
    {
        $existingUser = $this->socialAccountsService->checkExistingUser($provider, $request->get('access_token'), $request->get('code'));

        if (!$existingUser && $this->socialAccountsService->getAccessToken() && $provider === 'linkedin'){
            return response()->json(['access_token' => $this->socialAccountsService->getAccessToken()], 404);
        }

        return $existingUser ? response()->json($existingUser, 200) : response()->json(['message' => 'User not found'], 404);
    }

    /**
     * @Post(
     *     path="/register/{provider}",
     *     description="Login user via social networks (Facebook, Google, LinkedIn)",
     *     tags={"Auth"},
     *     @Parameter(
     *          name="provider",
     *          in="path",
     *          description="Social network provider, supports 'facebook', 'google' or 'linkedin'",
     *          example="facebook, google, linkedin",
     *          required=true,
     *          @Schema(type="string")
     *     ),
     *     @RequestBody(
     *         required=true,
     *         description="User already register via social network, returns token and user details",
     *         @JsonContent(ref="#/components/schemas/SocialRegisterRequest")
     *     ),
     *     @Response(
     *         response=201,
     *         description="User register via social network, returns user details and auth tokens",
     *         @JsonContent(
     *              allOf={
     *                  @Schema(ref="#/components/schemas/TokenResponse"),
     *                  @Schema(
     *                      title="user",
     *                      @Property(property="user", title="user", ref="#/components/schemas/UserResponse")
     *                  )
     *              }
     *         )
     *     ),
     *     @Response(
     *         response=200,
     *         description="User response if user already registered via social network",
     *         @JsonContent(
     *              allOf={
     *                  @Schema(ref="#/components/schemas/TokenResponse"),
     *                  @Schema(
     *                      title="user",
     *                      @Property(property="user", title="user", ref="#/components/schemas/UserResponse")
     *                  )
     *              }
     *         )
     *     ),
     *     @Response(response="422", description="Error: Unprocessable Entity"),
     *     @Response(response="400", description="Access token is invalid"),
     * )
     */
    public function register(string $provider, SocialRegisterRequest $request)
    {
        $existingUser = $this->socialAccountsService->checkExistingUser($provider, $request->get('access_token'), $request->get('code'));

        if ($existingUser){
            return response()->json($existingUser, 200);
        }

        $providerUser = $this->socialAccountsService->getProviderUser();

        if ($providerUser){
            $user = $this->socialAccountsService->create($providerUser, $provider, $request);
            return  response()->json($user, 201);
        }

        return response()->json(['message' => 'Access token is invalid'], 400);
    }
}
