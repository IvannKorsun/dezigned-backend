<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\LoginRequest;
use App\Http\Requests\Auth\RefreshTokenRequest;
use App\Services\OauthTokenService;
use App\Models\User;
use App\Services\UserService;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Laravel\Passport\Passport;
use OpenApi\Annotations\Get;
use OpenApi\Annotations\JsonContent;
use OpenApi\Annotations\Post;
use OpenApi\Annotations\Property;
use OpenApi\Annotations\RequestBody;
use OpenApi\Annotations\Response;
use OpenApi\Annotations\Schema;

class LoginController extends Controller
{
    /**
     * @var UserService
     */
    private $userService;

    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    /**
     * @Post(
     *     path="/login",
     *     description="Login user",
     *     tags={"Auth"},
     *     @RequestBody(
     *         description="Login user by name or email",
     *         required=true,
     *         @JsonContent(ref="#/components/schemas/UserLoginRequest")
     *     ),
     *     @Response(
     *         response=200,
     *         description="User response",
     *         @JsonContent(
     *              allOf={
     *                  @Schema(ref="#/components/schemas/TokenResponse"),
     *                  @Schema(
     *                      title="user",
     *                      @Property(property="user", title="user", ref="#/components/schemas/UserResponse")
     *                  )
     *              }
     *         )
     *     ),
     *     @Response(response="422", description="Error: Unprocessable Entity"),
     *     @Response(response="401", description="Error: Unauthorized")
     * )
     */
    public function login(LoginRequest $request, OauthTokenService $tokenService)
    {
        $auth = $this->userService->loginUser($request, $tokenService);

        if (isset($auth['error'])){
            return response()->json([
                'message' => $auth['error']
            ], 401);
        }
        return response()->json($auth, 200);
    }

    /**
     * @Get(
     *     path="/logout",
     *     description="User logout",
     *     tags={"Auth"},
     *     security={{"Auth": {}}},
     *     @Response(response="200", description="Successfully logged out"),
     *     @Response(response="401", description="Error: Unauthorized")
     * )
     */
    public function logout(Request $request)
    {
        $request->user()->token()->revoke();

        return response()->json([
            'message' => 'Successfully logged out'
        ]);
    }
    /**
     * @Post(
     *     path="/refresh",
     *     description="Refresh user access token",
     *     tags={"Auth"},
     *     @RequestBody(
     *         required=true,
     *         @JsonContent(
     *                @Property(property="refresh_token", title="refresh_token", type="string", example="eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6ImQzYTkxMmU4ZDBiZmI0YWE4NjMxZDQ0NjMyOWQ4MDdlYWYwMTJmM2FjOGFiZTViNTlkMTBiNzJlNzQ4ZWYxNjkyNzBhZWRjYjIwOGVmZDE0In0")
     *         )
     *     ),
     *     @Response(
     *         response=200,
     *         description="Token response",
     *         @JsonContent(ref="#/components/schemas/TokenResponse"),
     *     ),
     *     @Response(response="422", description="Error: Unprocessable Entity"),
     *     @Response(response="401", description="Error: Unauthorized")
     * )
     */
    public function refreshToken(RefreshTokenRequest $request,  OauthTokenService $tokenService)
    {
        $token = $tokenService->refreshToken($request);

        if (isset($token['error'])){
            return response()->json(['message' => $token['message']], 422);
        }

        return response()->json($token, 201);
    }
}
