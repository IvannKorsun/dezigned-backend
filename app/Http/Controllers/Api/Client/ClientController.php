<?php

namespace App\Http\Controllers\Api\Client;

use App\Http\Requests\Client\ClientsListRequest;
use App\Http\Requests\Client\ClientUpdateRequest;
use App\Http\Requests\Client\CreateClientRequest;
use App\Services\Client\ClientService;
use App\Http\Controllers\Controller;
use OpenApi\Annotations\Delete;
use OpenApi\Annotations\Get;
use OpenApi\Annotations\JsonContent;
use OpenApi\Annotations\MediaType;
use OpenApi\Annotations\Parameter;
use OpenApi\Annotations\Post;
use OpenApi\Annotations\Property;
use OpenApi\Annotations\Put;
use OpenApi\Annotations\RequestBody;
use OpenApi\Annotations\Response;
use OpenApi\Annotations\Schema;

class ClientController extends Controller
{
    /**
     * @var ClientService
     */
    private $clientService;

    public function __construct(ClientService $clientService)
    {
        $this->clientService = $clientService;
    }

    /**
     * @Get(
     *     path="/client",
     *     description="Get list of clients",
     *     tags={"Client"},
     *     security={{"Auth": {}}},
     *     @Parameter(
     *          name="active",
     *          in="query",
     *          description="Active (1) and archived(0) client filter",
     *          example="1",
     *          required=false,
     *          allowEmptyValue=true,
     *          @Schema(type="string")
     *     ),
     *     @Parameter(
     *          name="search",
     *          in="query",
     *          description="Text search word. Searhc by client name",
     *          example="client name",
     *          required=false,
     *          allowEmptyValue=true,
     *          @Schema(type="string")
     *     ),
     *     @Parameter(
     *          name="page",
     *          in="query",
     *          description="Number of clients page",
     *          example="1",
     *          required=false,
     *          allowEmptyValue=true,
     *          @Schema(type="int")
     *     ),
     *     @Parameter(
     *          name="limit",
     *          in="query",
     *          description="Number of clients per page",
     *          example=16,
     *          required=false,
     *          allowEmptyValue=true,
     *          @Schema(type="int")
     *     ),
     *     @Parameter(
     *          name="sort",
     *          in="query",
     *          description="Sorting. Sort by client name, company, email, phone_number, address, city, zip, notes. By default sort by name asc.",
     *          example="name,asc",
     *          required=false,
     *          allowEmptyValue=true,
     *          @Schema(type="string")
     *     ),
     *     @Response(response="403", description="User does not have the right roles."),
     *     @Response(
     *          response="200",
     *          description="Example clients list response",
     *          @JsonContent(
     *                  allOf={
     *                      @Schema(
     *                          title="data",
     *                          @Property(type="array", property="data", @\OpenApi\Annotations\Items(ref="#/components/schemas/ClientResponse")),
     *                      ),
     *                      @Schema(
     *                          title="total",
     *                          @Property(title="total", property="total", type="integer", example="10")
     *                      )
     *                  }
     *          )
     *     ),
     *     @Response(response="401", description="Error: Unauthorized"),
     *     @Response(response="500", description="Internal Server Error.")
     * )
     */
    public function index(ClientsListRequest $request)
    {
        return response()->json($this->clientService->getClients($request));
    }

    /**
     * @Post(
     *     path="/client",
     *     description="Add a new client",
     *     tags={"Client"},
     *     security={{"Auth": {}}},
     *     @RequestBody(
     *         description="Add a new client",
     *          @MediaType(
     *              mediaType="application/json",
     *              @JsonContent(ref="#/components/schemas/CreateClientRequest")
     *          )
     *     ),
     *     @Response(response=201, description="Client created successfully", @JsonContent(ref="#/components/schemas/ClientResponse")),
     *     @Response(response=422, description="Error: Unprocessable Entity"),
     *     @Response(response=401, description="Error: Unauthorized")
     * )
     */
    public function store(CreateClientRequest $request)
    {
        return response()->json($this->clientService->addNewClient($request));
    }

    /**
     * @Get(
     *     path="/client/{id}",
     *     description="Get client details by client id",
     *     tags={"Client"},
     *     security={{"Auth": {}}},
     *     @Parameter(
     *          name="id",
     *          in="path",
     *          description="Get client details by id",
     *          example=1,
     *          required=true,
     *          allowEmptyValue=false,
     *          @Schema(type="integer")
     *     ),
     *     @Response(
     *          response="200",
     *          description="Client details by client id",
     *          @JsonContent(ref="#/components/schemas/ClientResponse")
     *     ),
     *     @Response(response="401", description="Error: Unauthorized"),
     *     @Response(response="404", description="Client not found."),
     * )
     */
    public function show(int $id)
    {
        $client = $this->clientService->getClient($id);

        return $client
            ? response()->json($client)
            : response()->json(['message' => 'Not found'], 404);
    }

    /**
     * @Put(
     *     path="/client/{id}",
     *     description="Update client by id",
     *     tags={"Client"},
     *     security={{"Auth": {}}},
     *     @Parameter(
     *          name="id",
     *          in="path",
     *          description="Update client by id",
     *          example=1,
     *          required=true,
     *          allowEmptyValue=false,
     *          @Schema(type="integer")
     *     ),
     *     @RequestBody(
     *          @MediaType(
     *              mediaType="application/json",
     *              @Schema(ref="#/components/schemas/CreateClientRequest")
     *          )
     *     ),
     *     @Response(
     *          response="200",
     *          description="Updated client details by client id",
     *          @JsonContent(ref="#/components/schemas/ClientResponse")
     *     ),
     *     @Response(response="401", description="Error: Unauthorized"),
     *     @Response(response="404", description="Client not found."),
     *     @Response(response="500", description="Internal Server Error.")
     * )
     */
    public function update(ClientUpdateRequest $request, $id)
    {
        $client = $this->clientService->updateClient($id, $request);

        return $client
            ? response()->json($client)
            : response()->json(['message' => 'Not found'], 404);
    }

    /**
     * @Delete(
     *     path="/client/{id}",
     *     description="Delete client",
     *     tags={"Client"},
     *     security={{"Auth": {}}},
     *     @Parameter(
     *          name="id",
     *          in="path",
     *          description="Delete client by id",
     *          example=1,
     *          required=true,
     *          allowEmptyValue=false,
     *          @Schema(type="integer")
     *     ),
     *     @Response(response=204, description="No content"),
     *     @Response(response=422, description="Error: Unprocessable Entity", @JsonContent()),
     *     @Response(response=401, description="Error: Unauthorized")
     * )
     */
    public function destroy($id)
    {
        $deleted = $this->clientService->deleteClient($id);

        if ($deleted){
            return response()->json(['message' => 'Client successfully deleted'], 204);
        }

        return response()->json(['message' => 'Not found'], 404);
    }
}
