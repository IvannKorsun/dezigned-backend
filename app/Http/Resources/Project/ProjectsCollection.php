<?php

namespace App\Http\Resources\Project;

use Illuminate\Http\Resources\Json\ResourceCollection;

class ProjectsCollection extends ResourceCollection
{
    public function toArray($request)
    {
        return [
            'data' => ProjectListEntryResourse::collection($this->collection),
            'total' => ($request->get('page') && $request->get('limit'))
                ? $this->total()
                : $this->collection->count()
        ];
    }
}
