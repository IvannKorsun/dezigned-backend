<?php

namespace App\Http\Resources\Project;

use App\Http\Resources\Client\ClientResource;
use Illuminate\Http\Resources\Json\JsonResource;

class ProjectResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'description' => $this->description,
            'active' => $this->active,
            'client' => ClientResource::make($this->client),
            'cover_image_original' => $this->cover_image_original,
            'cover_image_thumbnail' => $this->cover_image_thumbnail,
            'images' =>
                [
                    'data' => ProjectImageResource::collection($this->images()->paginate(8)),
                    'total' => $this->images->count()
                ],
            'project_areas' =>
                [
                    'data' => ProjectAreaResourse::collection($this->areas()->paginate(8)),
                    'total' => $this->areas->count()
                ]
        ];
    }
}
