<?php

namespace App\Http\Resources\Project;

use App\Http\Resources\User\InstallerResource;
use Illuminate\Http\Resources\Json\JsonResource;

class AreaProductMeasureResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        $clientInfo = $this->clientInfo();

        return [
            'id' => $this->id,
            'installer' => $this->installer ? InstallerResource::make($this->installer) : null,
            'details' => $this->details,
            'product_details' => $this->product_details,
            'fee' => $this->fee,
            'step' => $this->step,
            'client' => $clientInfo
        ];
    }

    public function clientInfo()
    {
        $client = $this->measurable->area->project->client ?? null;

        return $client
            ? [
                'address' => $client->address . ', ' . $client->city . ', ' . $client->zip,
                'location' => [
                    'lat' =>  $client->location ? $client->location->getLat() : null,
                    'lng' =>  $client->location ? $client->location->getLng() : null
                ],
              ]
            : null;
    }

}
