<?php

namespace App\Http\Resources\Project;

use App\Services\Bigcommerce\Facades\BcProductRepository;
use Illuminate\Http\Resources\Json\ResourceCollection;

class AreaProductNotesCollection extends ResourceCollection
{

    protected function getBcProductName(int $productId): ?string
    {
        $bcProduct =  BcProductRepository::getProduct($productId, ['include_fields' => 'name']);

        return $bcProduct ? $bcProduct->name : null;
    }

    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $bcProductId = $this->collection->first() ? $this->collection->first()->areaProduct->bc_product_id : null;
        return [
            'title' => $bcProductId ? $this->getBcProductName($bcProductId) : ' ',
            'data' => AreaProductNoteResource::collection($this->collection),
            'total' => ($request->get('page') && $request->get('limit'))
                ? $this->total()
                : $this->collection->count()
        ];
    }
}
