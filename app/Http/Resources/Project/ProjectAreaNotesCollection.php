<?php

namespace App\Http\Resources\Project;

use App\Models\Project\ProjectArea;
use Illuminate\Http\Resources\Json\ResourceCollection;

class ProjectAreaNotesCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $area = ProjectArea::find($request->route()->parameter('areaId'));
        return [
            'title' => $area ? $area->name : null,
            'data' => ProjectAreaNoteResource::collection($this->collection),
            'total' => ($request->get('page') && $request->get('limit'))
                ? $this->total()
                : $this->collection->count()
        ];
    }
}
