<?php

namespace App\Http\Resources\User;

use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return
            [
                'id' => $this->id,
                'name' => $this->name,
                'first_name' => $this->first_name,
                'last_name' => $this->last_name,
                'phone_number' => $this->phone_number,
                'email' => $this->email,
                'active' => $this->active,
                'twitter_url' => $this->twitter_url,
                'instagram_url' => $this->instagram_url,
                'facebook_url' => $this->facebook_url,
                'education' => $this->education,
                'experience' => $this->experience,
                'avatar_url' => $this->avatar_url,
                'description' => $this->description,
                'address' => $this->address,
                'city' => $this->city,
                'zip' => $this->zip,
                'location' =>
                    [
                        'lat' =>  $this->location ? $this->location->getLat() : null,
                        'lng' =>  $this->location ? $this->location ->getLng() : null,
                    ],
                'roles' => $this->roles
            ];
    }
}
