<?php

namespace App\Http\Resources\User;

use Illuminate\Http\Resources\Json\JsonResource;

class InstallerResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return
            [
                'id' => $this->id,
                'name' => ucfirst($this->first_name) . ' ' . ucfirst($this->last_name),
                'reviews' => ['rating' => 5, 'reviews_count' => 10]
            ];
    }
}
