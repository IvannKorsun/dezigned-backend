<?php

namespace App\Http\Resources\User;

use Illuminate\Http\Resources\Json\JsonResource;

class UserExperienceResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'position' => $this->position,
            'company' => $this->company,
            'currently_works' => $this->currently_works,
            'company_image' => $this->company_image,
            'date' => $this->date_from->format("M. Y") . ' - ' . (($this->currently_works) ? 'Present' : $this->date_to->format("M. Y")),
            'date_from' => $this->date_from->format('Y-m'),
            'date_to' => $this->date_to ? $this->date_to->format('Y-m') : null
        ];
    }
}
