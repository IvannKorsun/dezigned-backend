<?php

namespace App\Http\Resources\Installer;

use Illuminate\Http\Resources\Json\JsonResource;

class InstallerProjectDetailsEntryResourse extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {

        return [
            'id' => $this->id,
            'client_name' => $this->measurable->area->project->client->name,
            'address' => $this->measurable->area->project->user->address,
            'location' => $this->measurable->area->project->user->address,
            'product_name' => $this->product_name,
            'project_description' => $this->measurable->area->project->description,
            'pay' => $this->fee,
            'email' => $this->measurable->area->project->client->email,
            'phone_number' => $this->measurable->area->project->client->phone_number,
            'dates' => $this->dates->makeHidden('product_measure_id'),
        ];
    }
}
