<?php

namespace App\Http\Resources\Installer;

use Illuminate\Http\Resources\Json\JsonResource;

class InstallerProjectListEntryResourse extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {

        return [
            'id' => $this->product_measure_id,
            'client_name' => $this->measurable->area->project->client->name,
            'address' => $this->measurable->area->project->client->address,
            'location' => $this->measurable->area->project->client->address,
            'product_name' => $this->product_name,
            'email' => $this->measurable->area->project->user->email,
            'phone_number' => $this->measurable->area->project->user->phone_number,
            'status' =>$this->installer_status,
            'date_id' => $this-> status == 'approved' ? $this->id: null,
            'date_form' => $this-> status == 'approved' ? $this->date_from: null,
        ];
    }
}
