<?php

namespace App\Http\Resources\Installer;

use Illuminate\Http\Resources\Json\ResourceCollection;

class InstallerProjectsCollection extends ResourceCollection
{
    public function toArray($request)
    {
        return [
            'data' => InstallerProjectListEntryResourse::collection($this->collection),
            'total' => ($request->get('page') && $request->get('limit'))
                ? $this->total()
                : $this->collection->count()
        ];
    }
}
