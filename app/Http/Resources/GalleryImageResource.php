<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class GalleryImageResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'url_thumbnail' => $this->url_thumbnail,
            'url_original' => $this->url_original,
            'created_at' => $this->created_at->format('d/m/Y')
        ];
    }
}
