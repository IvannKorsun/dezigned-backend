<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class GalleryImageCollection extends ResourceCollection
{
    public function toArray($request)
    {
        return [
            'data' => GalleryImageResource::collection($this->collection),
            'total' => ($request->get('page') && $request->get('limit'))
                ? $this->total()
                : $this->collection->count()
        ];
    }
}
