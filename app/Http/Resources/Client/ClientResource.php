<?php

namespace App\Http\Resources\Client;

use Illuminate\Http\Resources\Json\JsonResource;

class ClientResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return
            [
                'id' => $this->id,
                'name' => $this->name,
                'company' => $this->company,
                'email' => $this->email,
                'phone_number' => $this->phone_number,
                'address' => $this->address,
                'city' => $this->city,
                'zip' => $this->zip,
                'notes' => $this->notes,
                'active' => $this->active,
                'location' =>
                    [
                        'lat' =>  $this->location ? $this->location->getLat() : null,
                        'lng' =>  $this->location ? $this->location ->getLng() : null,
                    ]
            ];
    }
}
