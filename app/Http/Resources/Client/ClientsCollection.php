<?php

namespace App\Http\Resources\Client;

use Illuminate\Http\Resources\Json\ResourceCollection;

class ClientsCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return
            [
                'data' => ClientResource::collection($this->collection),
                'total' => ($request->get('page') && $request->get('limit'))
                    ? $this->total()
                    : $this->collection->count()
            ];
    }
}
