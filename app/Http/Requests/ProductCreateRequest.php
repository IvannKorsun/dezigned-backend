<?php

namespace App\Http\Requests;

use App\Rules\Base64Validation;
use App\Rules\ProductCustomFieldValidate;
use Illuminate\Foundation\Http\FormRequest;

class ProductCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:250|unique:products',
            'description' => 'required|string',
            'price' => 'numeric|required',
            'brand_id' => 'required|integer',
            'images' => 'array|between:0,4',
            'images.*' => ['sometimes', 'string', new Base64Validation(['jpeg','png','jpg'], 65536)],
            'custom_fields' => 'array',
            'custom_fields.*' => ['required','json', new ProductCustomFieldValidate],
        ];
    }

    public function messages()
    {
        return
            [
                'image.*.max' => "Maximum file size to upload is 64MB (65536 KB)."
            ];
    }

}
