<?php

namespace App\Http\Requests\Project;

use Illuminate\Foundation\Http\FormRequest;

class ProductMeasureAddDatesRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'action' => 'required|in:save,request',
            'dates' => 'present|array',
            'dates.*.date_from' => 'required|date_format:"Y-m-d g:ia"',
            'dates.*.date_to' => 'required|date_format:"Y-m-d g:ia"',
        ];
    }
}
