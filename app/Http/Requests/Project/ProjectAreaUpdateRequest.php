<?php

namespace App\Http\Requests\Project;

use App\Rules\Base64Validation;
use Illuminate\Foundation\Http\FormRequest;

class ProjectAreaUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'sometimes|required|string|max:255|unique:project_areas,name,NULL,id,project_id,' . $this->projectId ,
            'cover_image' => ['sometimes', 'nullable', 'string', new Base64Validation(['jpeg','png','jpg'], 65536)],
            'description' => 'sometimes|nullable|string|max:65535',
        ];
    }
}
