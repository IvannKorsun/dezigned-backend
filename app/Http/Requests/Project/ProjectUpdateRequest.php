<?php

namespace App\Http\Requests\Project;

use App\Rules\Base64Validation;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class ProjectUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'sometimes|string|min:1|max:250|unique:projects,name,NULL,id,user_id,' . Auth::id(),
            'description' => 'sometimes|string|nullable',
            'client_id' => 'sometimes|integer|nullable|exists:clients,id,user_id,' . Auth::id(),
            'cover_image' => ['sometimes', 'nullable', 'string', new Base64Validation(['jpeg','png','jpg'], 65536)],
        ];
    }
}
