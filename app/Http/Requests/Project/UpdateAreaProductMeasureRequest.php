<?php

namespace App\Http\Requests\Project;

use Illuminate\Foundation\Http\FormRequest;

class UpdateAreaProductMeasureRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'project_id' => 'required|integer',
            'area_id' => 'required|integer',
            'product_id' => 'required|integer',
            'details' => 'sometimes|required|string|min:1|max:65535',
            'installer_id' => 'sometimes|required|integer|exists:users,id',
            'step' => 'sometimes|required|integer|in:1,2'
        ];
    }
}
