<?php

namespace App\Http\Requests\User;

use App\Rules\Base64Validation;
use Illuminate\Foundation\Http\FormRequest;

class UpdateExperienceRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'position' => 'sometimes|string|min:1|max:100',
            'company' => 'sometimes|string|min:1|max:100',
            'currently_works' => 'sometimes|boolean',
            'company_image' => ['sometimes', 'nullable', 'string', new Base64Validation(['jpeg','png','jpg'], 1024)],
            'date_from' => 'sometimes|date_format:"Y-m"',
            'date_to' => 'required_if:currently_works,0|required_if:currently_works,false|date_format:"Y-m"',
        ];
    }
}
