<?php

namespace App\Http\Requests\User;

use App\Rules\Base64Validation;
use Illuminate\Foundation\Http\FormRequest;

class UserUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'sometimes|string|max:255|unique:users',
            'first_name' => 'sometimes|string|max:255',
            'last_name' => 'sometimes|string|max:255',
            'phone_number' => 'sometimes|string|nullable|max:20',
            'email' => 'sometimes|string|email|max:255|unique:users',
            'description' => 'sometimes|nullable|string|max:255',
            'avatar' => ['sometimes', 'nullable', 'string', new Base64Validation(['jpeg','png','jpg'], 65536)],
            'experience' => 'sometimes|nullable|string|max:20',
            'education' => 'sometimes|nullable|string|max:255',
            'facebook_url' => 'sometimes|nullable|string|max:255',
            'instagram_url' => 'sometimes|nullable|string|max:255',
            'twitter_url' => 'sometimes|nullable|string|max:255',
            'address' => 'sometimes|string',
            'city' => 'sometimes|string|max:50',
            'zip' => 'sometimes|numeric',
            'location' => 'array|sometimes',
            'location.lat' => 'required_with:location|numeric',
            'location.lng' => 'required_with:location|numeric',
        ];
    }
}
