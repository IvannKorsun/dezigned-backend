<?php

namespace App\Http\Requests\Auth;

use Illuminate\Foundation\Http\FormRequest;

class RegisterUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|max:255|unique:users',
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'phone_number' => 'required|string|nullable|max:20',
            'email' => 'required|string|email|max:255|unique:users',
            'role' => 'required|exists:roles,name'
        ];
    }

    public function messages()
    {
        return [
            'email.unique' => sprintf('User with email %s already exists',  $this->validationData()['email'])
        ];
    }
}
