<?php

namespace App\Http\Requests\Client;

use Illuminate\Foundation\Http\FormRequest;

class CreateClientRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|max:100',
            'company' => 'required|string|max:100',
            'email' => 'required|string|email|max:100',
            'phone_number' => 'required|string|max:20',
            'address' => 'required|string',
            'city' => 'required|string|max:50',
            'zip' => 'required|numeric',
            'notes' => 'string|nullable',
            'location' => 'array|required',
            'location.lat' => 'required_with:location|numeric',
            'location.lng' => 'required_with:location|numeric',
        ];
    }
}
