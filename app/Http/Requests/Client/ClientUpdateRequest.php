<?php

namespace App\Http\Requests\Client;

use Illuminate\Foundation\Http\FormRequest;

class ClientUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'sometimes|string|max:100',
            'company' => 'sometimes|string|max:100',
            'email' => 'sometimes|string|email|max:100',
            'phone_number' => 'sometimes|required|string|max:20',
            'address' => 'sometimes|string',
            'city' => 'sometimes|string|max:50',
            'zip' => 'sometimes|numeric',
            'notes' => 'sometimes|string',
            'active' => 'boolean',
            'location' => 'array|sometimes',
            'location.lat' => 'required_with:location|numeric',
            'location.lng' => 'required_with:location|numeric',
        ];
    }
}
