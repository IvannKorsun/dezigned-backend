<?php

namespace App\Http\Requests\Client;

use Illuminate\Foundation\Http\FormRequest;

class ClientsListRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'search' => 'sometimes|string',
            'page' => 'sometimes|integer',
            'limit' => 'sometimes|integer',
            'sort' => ['sometimes', "regex:~[A-Za-z]{1,},\basc\b|\bdesc\b~"]
        ];
    }
}
