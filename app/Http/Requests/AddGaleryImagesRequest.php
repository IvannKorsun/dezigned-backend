<?php

namespace App\Http\Requests;

use App\Rules\Base64Validation;
use Illuminate\Foundation\Http\FormRequest;

class AddGaleryImagesRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'images' => 'required|array',
            'images.*.name' => 'required|string|max:100',
            'images.*.image' => ['required', 'string', new Base64Validation(['jpeg','png','jpg'], 65536)],
        ];
    }
}
