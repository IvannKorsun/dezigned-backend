<?php

namespace App\Helpers;

class ProductSeedHelper
{
    public static function makeNamesToInsert(array $names){

        $result = [];

        foreach ($names as $name){
            $result[] = ['name' => trim($name)];
        }

        return $result;
    }
}
