<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class ProductCustomFieldValidate implements Rule
{

    private $errorMessages = [];

    private $customFieldNames = ['name', 'value'];
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $customFields = json_decode($value, true);

        foreach ($this->customFieldNames as $customFieldName){

            if (!isset($customFields[$customFieldName])){
                $this->errorMessages[] = 'Custom field ' . $customFieldName . ' is required!';
            }

            if (isset($customFields[$customFieldName]) && !is_string($customFields[$customFieldName])){
                $this->errorMessages[] = 'Custom field ' . $customFieldName . ' must be a string!';
            }
        }

        if (!empty($this->errorMessages)){
            return false;
        }

        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return implode($this->errorMessages, ' , ');
    }
}
