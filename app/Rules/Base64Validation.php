<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class Base64Validation implements Rule
{
    private $errorMessages = [];

    private $mimes;

    private $size;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct(array $mimes, int $size)
    {
        $this->mimes = $mimes;
        $this->size = $size;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if (explode(',', explode(';', $value)[1])[0] !== 'base64'){
            $this->errorMessages[] = 'The :attribute must have a base64 type';
        }

        if (!in_array(explode('/', explode(';base64', $value)[0])[1], $this->mimes)){
            $this->errorMessages[] = 'The :attribute must be a file of type: ' . implode(',', $this->mimes);
        }

        if (strlen(base64_decode($value)) / 1000000 > $this->size){
            $this->errorMessages[] = 'The :attribute may not be greater than ' . $this->size  . ' kilobytes.';
        }

        if (!empty($this->errorMessages)){
            return false;
        }

        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return implode($this->errorMessages, ' , ');
    }
}
