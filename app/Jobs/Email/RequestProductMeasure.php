<?php


namespace App\Jobs\Email;


use App\Models\Project\ProductMeasureDates;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Mail\Message;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;
use Sichikawa\LaravelSendgridDriver\Transport\SendgridTransport;

class RequestProductMeasure implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var User
     */
    private $user;
    /**
     * @var string
     */
    private $productName;
    /**
     * @var array
     */
    private $dates;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(User $user, ?string $productName, Collection $dates)
    {
        $this->user = $user;
        $this->productName = $productName;
        $this->dates = $dates;

        $this->queue = env('EMAILS_QUEUE_NAME', 'dezigned-dev-emails');
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $template = config('sendgrid.templates.request_product_measure');

        Mail::send([], [], function (Message $message) use ($template){
            $message
                ->to($this->user->email)
                ->from(env('MAIL_FROM'))
                ->embedData([
                    'personalizations' => [
                        [
                            'dynamic_template_data' => [
                                'dates' => $this->datesToString($this->dates),
                                'productName' => $this->productName,
                                'name'  => ucfirst($this->user->first_name) .  ' ' . ucfirst($this->user->last_name),
                            ],
                        ],
                    ],
                    'template_id' => $template,
                ], SendgridTransport::SMTP_API_NAME);
        });
    }

    public function datesToString(Collection $dates)
    {
        $datesForEmail = '';

        foreach ($dates as $date){
            $datesForEmail .= $date->date_from . ' - ' . $date->date_to . '<br>';
        }

        return $datesForEmail;
    }
}
