<?php

namespace App\Jobs\Email;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Message;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Mail;
use Sichikawa\LaravelSendgridDriver\Transport\SendgridTransport;

class PasswordResetRequest implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    /**
     * @var string
     */
    private $url;
    /**
     * @var string
     */
    private $email;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(string $url, string $email)
    {
        $this->url = $url;
        $this->email = $email;
        $this->queue = env('EMAILS_QUEUE_NAME', 'dezigned-dev-emails');
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $template = config('sendgrid.templates.password_reset_request');

        Mail::send([], [], function (Message $message) use ($template){
            $message
                ->to($this->email)
                ->from(env('MAIL_FROM'))
                ->embedData([
                    'personalizations' => [
                        [
                            'dynamic_template_data' => [
                                'url' => $this->url,
                            ],
                        ],
                    ],
                    'template_id' => $template,
                ], SendgridTransport::SMTP_API_NAME);
        });
    }
}
