<?php

namespace App\Jobs\Email;

use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Message;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Mail;
use Sichikawa\LaravelSendgridDriver\Transport\SendgridTransport;

class SignupActivateEmail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    /**
     * @var User
     */
    private $user;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(User $user)
    {
        $this->user = $user;
        $this->queue = env('EMAILS_QUEUE_NAME', 'dezigned-dev-emails');
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $url = route('registerConfirm', ['token' => $this->user->activation_token]);

        $template = config('sendgrid.templates.confirm_email');

        Mail::send([], [], function (Message $message) use ($url, $template){
            $message
                ->to($this->user->email)
                ->from(env('MAIL_FROM'))
                ->embedData([
                    'personalizations' => [
                        [
                            'dynamic_template_data' => [
                                'url' => $url,
                                'name'  => ucfirst($this->user->name),
                            ],
                        ],
                    ],
                    'template_id' => $template,
                ], SendgridTransport::SMTP_API_NAME);
        });
    }
}
