<?php

namespace App\Jobs\Email;

use App\Models\PasswordReset;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Message;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Mail;
use Sichikawa\LaravelSendgridDriver\Transport\SendgridTransport;

class PasswordResetSuccess implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    /**
     * @var PasswordReset
     */
    private $passwordReset;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(PasswordReset $passwordReset)
    {
        $this->passwordReset = $passwordReset;
        $this->queue = env('EMAILS_QUEUE_NAME', 'dezigned-dev-emails');
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $template = config('sendgrid.templates.password_reset_success');

        Mail::send([], [], function (Message $message) use ($template){
            $message
                ->to($this->passwordReset->email)
                ->from(env('MAIL_FROM'))
                ->embedData([
                    'template_id' => $template,
                ], SendgridTransport::SMTP_API_NAME);
        });
    }
}
