<?php

namespace App\Listeners;

use App\Events\UserCreated;
use App\Events\UserUpdated;
use App\Services\UserService;

class UserSubscriber
{
    /**
     * @var UserService
     */
    private $userService;

    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    public function handleUserCreate($event)
    {
        $this->userService->saveUser($event->getUser());
    }

    public function handleUserUpdate($event)
    {
        $this->userService->updateUser($event->getUser());
    }

    public function subscribe($events)
    {
        $events->listen(
            UserCreated::class,
            'App\Listeners\UserSubscriber@handleUserCreate'
        );

        $events->listen(
            UserUpdated::class,
            'App\Listeners\UserSubscriber@handleUserUpdate'
        );
    }
}
