<?php

namespace App\Models;

use App\Models\Client\Client;
use App\Models\Product\Product;
use App\Models\Project\Measurement;
use App\Models\Project\Project;
use App\Models\User\GalleryImage;
use App\Models\User\MissionStatement;
use App\Models\User\UserCertificate;
use App\Models\User\UserExperience;
use Grimzy\LaravelMysqlSpatial\Eloquent\SpatialTrait;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Hash;
use Laravel\Passport\HasApiTokens;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable, HasRoles, SpatialTrait;

    protected $guard_name = 'api';

    protected $spatialFields =
        [
            'location'
        ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable =
        [
            'name',
            'phone_number',
            'email',
            'password',
            'active',
            'activation_token',
            'set_pass_token',
            'first_name',
            'last_name',
            'description',
            'avatar_url',
            'experience',
            'education',
            'facebook_url',
            'instagram_url',
            'twitter_url',
            'bc_user_id',
            'address',
            'city',
            'zip'
        ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'activation_token', 'set_pass_token', 'created_at', 'updated_at', 'bc_user_id'
    ];

    protected $with = ['roles'];

    public function setPasswordAttribute($password)
    {
        $this->attributes['password'] = $password ? Hash::make($password) : null;
    }

    public function linkedSocialAccounts()
    {
        return $this->hasMany(LinkedSocialAccount::class);
    }

    public function products()
    {
        return $this->hasMany(Product::class);
    }

    public function profileShareableLink()
    {
        return $this->hasOne(SharedProfile::class);
    }

    public function galleryImages()
    {
        return $this->hasMany(GalleryImage::class);
    }

    public function userClients()
    {
        return $this->hasMany(Client::class);
    }

    public function projects()
    {
        return $this->hasMany(Project::class);
    }

    public function certificates()
    {
        return $this->hasMany(UserCertificate::class);
    }

    public function experience()
    {
        return $this->hasMany(UserExperience::class);
    }

    public function scopeClosestInstaller(Builder $query, $latitude, $longitude)
    {
        return $query
            ->selectRaw("(
            3958.756  *
                acos(
                  cos( radians($latitude) ) *
                  cos( radians( st_x(location) ) ) *
                  cos( radians( st_y(location) ) -
                  radians($longitude) ) +
                  sin( radians($latitude) ) *
                  sin( radians( st_x(location) ) )
                  )
            ) AS dist")->having('dist','<','30')
            ->whereNotNull('users.location')
            ->orderBy('dist');
    }

    public function missionStatement()
    {
        return $this->hasOne(MissionStatement::class);
    }
    public function measurements()
    {
        return $this->hasMany(Measurement::class);
    }
}
