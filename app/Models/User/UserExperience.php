<?php

namespace App\Models\User;

use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class UserExperience extends Model
{
    protected $table = 'users_experiences';

    protected $casts =
        [
            'date_from' => 'datetime',
            'date_to' => 'datetime'
        ];

    protected $fillable =
        [
            'position',
            'company',
            'currently_works',
            'company_image',
            'date_from',
            'date_to',
            'user_id',
        ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    protected function getCompanyImageAttribute($value)
    {
        return $value ? Storage::disk('s3-users-experience-comp-img')->url($value) : $value;
    }

    public function setDateFromAttribute($value)
    {
        $this->attributes['date_from'] = Carbon::createFromFormat('Y-m', $value);
    }

    public function setDateToAttribute($value)
    {
       $value
           ? $this->attributes['date_to'] = Carbon::createFromFormat('Y-m', $value)
           : $this->attributes['date_to'] = null;
    }
}
