<?php

namespace App\Models\User;

use App\Models\BaseGallery;
use App\Models\User;

class UserCertificate extends BaseGallery
{
    protected  $storageS3DiscName = 's3-user-certificates';

    protected $table = 'user_certifications';

    protected $fillable = ['url_thumbnail', 'url_original', 'user_id', 'name'];

    protected $hidden = ['created_at', 'updated_at'];

    public function user(){
        $this->belongsTo(User::class);
    }
}
