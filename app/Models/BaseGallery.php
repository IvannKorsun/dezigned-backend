<?php


namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class BaseGallery extends Model
{
    protected $storageS3DiscName;

    protected function getUrlThumbnailAttribute($value)
    {
        return Storage::disk($this->storageS3DiscName)->url($value);
    }

    protected function getUrlOriginalAttribute($value)
    {
        return Storage::disk($this->storageS3DiscName)->url($value);
    }

    public function scopeSearch($query, ?string $keyword)
    {
        if (!$keyword){
            return $query;
        }

        $pattern = "~^[0-9]{4}\-[0-9]{2}\-[0-9]{2}$~";

        return preg_match($pattern, $keyword)
            ? $query->whereRaw('date(created_at) = \''. $keyword . '\'')
            : $query->where('name', 'like', '%' . $keyword . '%');
    }

    public function scopeSorting($query, ?string $params)
    {
        $params = $params ? explode(',', $params) : false;

        return $params ? $query->orderBy($params[0], $params[1]) : $query->orderByDesc('created_at');
    }

}
