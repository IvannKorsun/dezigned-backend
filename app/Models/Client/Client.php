<?php

namespace App\Models\Client;

use App\Models\Project\Project;
use App\Models\User;
use Grimzy\LaravelMysqlSpatial\Eloquent\SpatialTrait;
use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    use SpatialTrait;

    protected $spatialFields =
        [
            'location'
        ];

    protected $fillable =
        [
            'name',
            'company',
            'email',
            'phone_number',
            'address',
            'city',
            'zip',
            'notes',
            'user_id',
            'active',
        ];

    private static $sort =
        [
            'company',
            'email',
            'phone_number',
            'address',
            'city',
            'zip',
            'notes',
        ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function scopeSearch($query, ?string $text)
    {
        return $text
            ? $query->where('name', 'like', '%' . $text . '%')
                ->orWhere('company', 'like', '%' . $text . '%')
                ->orWhere('email', 'like', '%' . $text . '%')
                ->orWhere('phone_number', 'like', '%' . $text . '%')
                ->orWhere('address', 'like', '%' . $text . '%')
                ->orWhere('city', 'like', '%' . $text . '%')
                ->orWhere('zip', 'like', '%' . $text . '%')
            : $query;
    }

    public function scopeSorting($query, array $sort)
    {
        return $query->orderBy($sort[0], $sort[1]);
    }

    public static function hasSort($attr)
    {
        return in_array($attr, self::$sort);
    }

    public function projects()
    {
        return $this->hasMany(Project::class);
    }
}
