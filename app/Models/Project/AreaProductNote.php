<?php

namespace App\Models\Project;

use Illuminate\Database\Eloquent\Model;

class AreaProductNote extends Model
{
    protected $table = 'project_area_product_notes';

    protected $fillable = ['note', 'area_product_id'];

    public function areaProduct()
    {
        return $this->belongsTo(ProjectAreaProduct::class, 'area_product_id');
    }
}
