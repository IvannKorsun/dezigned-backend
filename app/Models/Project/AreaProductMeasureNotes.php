<?php

namespace App\Models\Project;

use Illuminate\Database\Eloquent\Model;

class AreaProductMeasureNotes extends Model
{
    protected $fillable = ['note', 'area_product_measure_id'];

    public function measure()
    {
        return $this->belongsTo(Measurement::class, 'area_product_measure_id');
    }
}
