<?php

namespace App\Models\Project;

use App\Models\BaseGallery;

class ProjectImage extends BaseGallery
{
    protected $storageS3DiscName = 's3-project-gallery';

    protected $fillable = ['url_thumbnail', 'url_original', 'project_id', 'name'];

    protected $hidden = ['created_at', 'updated_at'];

    public function project(){
        $this->belongsTo(Project::class);
    }
}
