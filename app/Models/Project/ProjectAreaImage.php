<?php

namespace App\Models\Project;

use App\Models\BaseGallery;

class ProjectAreaImage extends BaseGallery
{
    protected $storageS3DiscName = 's3-project-area-gallery';

    protected $fillable = ['url_thumbnail', 'url_original', 'project_area_id', 'name'];

    public function area()
    {
        return $this->belongsTo(ProjectArea::class);
    }
}
