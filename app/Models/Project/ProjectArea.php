<?php

namespace App\Models\Project;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class ProjectArea extends Model
{

    protected $fillable =
        [
            'name',
            'description',
            'project_id',
            'cover_image_thumbnail',
            'cover_image_original',
        ];

    public function project()
    {
        return $this->belongsTo(Project::class);
    }

    public function galleryImages()
    {
        return $this->hasMany(ProjectAreaImage::class);
    }

    public function scopeSearch($query, ?string $text)
    {
        return $text ? $query->where('name', 'like', '%' . $text . '%') : $query;
    }

    public function getCoverImageThumbnailAttribute($value)
    {
        return $value ? Storage::disk('s3-project-area-cover-img')->url($value) : null;
    }

    public function getCoverImageOriginalAttribute($value)
    {
        return $value ? Storage::disk('s3-project-area-cover-img')->url($value) : null;
    }

    public function products()
    {
        return $this->hasMany(ProjectAreaProduct::class, 'project_area_id', 'id');
    }

    public function notes()
    {
        return $this->hasMany(ProjectAreaNote::class);
    }
}
