<?php

namespace App\Models\Project;

use Illuminate\Database\Eloquent\Model;

class ProjectAreaNote extends Model
{
    protected $fillable = ['note', 'project_area_id'];

    public function area()
    {
        return $this->belongsTo(ProjectArea::class, 'project_area_id');
    }
}
