<?php

namespace App\Models\Project;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;

class Measurement extends Model
{
    protected $fillable = ['details', 'product_details', 'fee', 'installer_id', 'area_product_id', 'step','installer_status'];

    public function installer()
    {
        return $this->belongsTo(User::class, 'installer_id');
    }

    public function measurable()
    {
        return $this->morphTo();
    }

    public function notes()
    {
        return $this->hasMany(AreaProductMeasureNotes::class, 'area_product_measure_id');
    }

    public function dates()
    {
        return $this->hasMany(ProductMeasureDates::class, 'product_measure_id');
    }
}
