<?php

namespace App\Models\Project;

use App\Models\Client\Client;
use App\Models\User;
use App\Services\Project\ProjectService;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Project extends Model
{
    protected $fillable =
        [
            'name',
            'description',
            'active',
            'user_id',
            'client_id',
            'cover_image_thumbnail',
            'cover_image_original'
        ];

    protected $hidden = ['created_at', 'updated_at'];

    public function images()
    {
        return $this->hasMany(ProjectImage::class);
    }

    public function areas()
    {
        return $this->hasMany(ProjectArea::class);
    }

    public function scopeSearch($query, ?string $text)
    {
        return $text ? $query->where('name', 'like', '%' . $text . '%') : $query;
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function client()
    {
        return $this->belongsTo(Client::class, 'client_id');
    }

    public function getCoverImageThumbnailAttribute($value)
    {
        return $value ? Storage::disk(ProjectService::$storageS3name)->url($value) : null;
    }

    public function getCoverImageOriginalAttribute($value)
    {
        return $value ? Storage::disk(ProjectService::$storageS3name)->url($value) : null;
    }
}
