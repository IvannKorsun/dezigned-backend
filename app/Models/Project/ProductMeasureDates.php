<?php

namespace App\Models\Project;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

class ProductMeasureDates extends Model
{
    public $timestamps = false;

    protected $casts = ['date_from' => 'datetime', 'date_to' => 'datetime'];

    protected $dates = ['date_from', 'date_to'];

    public $fillable = ['date_from', 'date_to', 'status'];

    public function productMeasure()
    {
        return $this->belongsTo(Measurement::class, 'product_measure_id');
    }

    public function setDateFromAttribute($value)
    {
        $this->attributes['date_from'] = Carbon::createFromFormat('Y-m-d g:ia', $value);
    }

    public function setDateToAttribute($value)
    {
        $this->attributes['date_to'] = Carbon::createFromFormat('Y-m-d g:ia', $value);
    }

    public function getDateFromAttribute($value)
    {
        return is_string($value) ? Carbon::parse($value)->format('Y-m-d g:ia') : $value->format('Y:m:d g:ia');
    }

    public function getDateToAttribute($value)
    {
        return is_string($value) ? Carbon::parse($value)->format('Y-m-d g:ia') : $value->format('Y:m:d g:ia');
    }
}
