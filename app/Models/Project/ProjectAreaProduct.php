<?php


namespace App\Models\Project;

use Illuminate\Database\Eloquent\Model;

class ProjectAreaProduct  extends Model
{
    protected $table = 'project_area_products';

    protected $fillable = ['qty', 'bc_product_id'];

    public function notes()
    {
        return $this->hasMany(AreaProductNote::class, 'area_product_id');
    }

    public function area()
    {
        return $this->belongsTo(ProjectArea::class, 'project_area_id');
    }

    public function measure()
    {
        return $this->morphOne(Measurement::class, 'measurable');
    }
}
