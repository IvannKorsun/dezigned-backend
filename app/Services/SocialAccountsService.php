<?php


namespace App\Services;

use App\Events\UserCreated;
use App\Models\LinkedSocialAccount;
use App\Models\PasswordReset;
use App\Models\User;
use Carbon\Carbon;
use Coderello\SocialGrant\Resolvers\SocialUserResolverInterface;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use Laravel\Socialite\Facades\Socialite;
use Laravel\Socialite\Two\User as ProviderUser;

class SocialAccountsService implements SocialUserResolverInterface
{
    /**
     * @var OauthTokenService
     */
    private $tokenService;

    private $providerUser = null;
    /** @var string|null */
    private $accessToken;

    public function __construct(OauthTokenService $tokenService)
    {
        $this->tokenService = $tokenService;
    }

    public function find(ProviderUser $providerUser, string $provider): ?Authenticatable
    {
        $user = null;

        $linkedSocialAccount = LinkedSocialAccount::where('provider_name', $provider)
            ->where('provider_id', $providerUser->getId())
            ->first();

        if ($linkedSocialAccount) {
            return $linkedSocialAccount->user;
        }

        if ($email = $providerUser->getEmail()) {
            $user = User::where('email', $email)->first();
        }

        if ($user) {
            $user->linkedSocialAccounts()->create([
                'provider_id' => $providerUser->getId(),
                'provider_name' => $provider,
            ]);
        }

        return $user;
    }

    public function create(ProviderUser $providerUser, string $provider, Request $request): array
    {
        $name = explode(' ',trim(preg_replace('/\s+/', ' ', $providerUser->getName())));
        /** @var User $user */
        $user = User::create([
            'name' =>  ($name[0] ?? 'user') . $providerUser->getId(),
            'first_name' => $name[0] ?? 'FirstName',
            'last_name' => $name[1] ?? 'LastName',
            'email' => $providerUser->getEmail(),
            'activation_token' => '',
            'active' => true
        ]);

        $user->assignRole($request->get('role'));

        $user->linkedSocialAccounts()->create([
            'provider_id' => $providerUser->getId(),
            'provider_name' => $provider,
        ]);

        event(new UserCreated($user));

        return array_merge($this->tokenService->getAccessTokensForSocialAuth($provider, $request->get('access_token')), ['user' => $user]);
    }

    public function createPasswordReset(Authenticatable $user): PasswordReset
    {
        $passwordReset = PasswordReset::updateOrCreate(
            ['email' => $user->email],
            [
                'email' => $user->email,
                'token' => Str::random(60),
                'expires_at' => Carbon::now()->addYear()
            ]
        );

        return $passwordReset;
    }

    public function checkExistingUser(string $provider, ?string $accessToken, ?string $code): ?array
    {
        $this->accessToken = $accessToken;

        if ($provider === 'linkedin' && $code){
            $this->accessToken = Socialite::driver($provider)
                    ->redirectUrl($this->getRedirectLinkedinUri())
                    ->getAccessTokenResponse($code)['access_token'] ?? null;
        }

        $user = $this->resolveUserByProviderCredentials($provider, $this->accessToken);

        if ($user){
            return array_merge($this->tokenService->getAccessTokensForSocialAuth($provider, $this->accessToken), ['user' => $user]);
        }

        return null;
    }

    public function providerUser(string $provider, string $accessToken): ?ProviderUser
    {
        try {
            $providerUser = Socialite::driver($provider)->userFromToken($accessToken);
        } catch (\Exception $exception) {
            Log::warning('Socialite error' . $exception->getMessage());
        }

        return $providerUser ?? null;
    }

    public function resolveUserByProviderCredentials(string $provider, string $accessToken): ?Authenticatable
    {
        $this->providerUser = $this->providerUser($provider, $accessToken);

        if ($this->providerUser){
            return $this->find($this->providerUser, $provider);
        }

        return null;
    }

    public function getProviderUser()
    {
        return $this->providerUser;
    }

    /**
     * @return string|null
     */
    public function getAccessToken(): ?string
    {
        return $this->accessToken;
    }

    public function getRedirectLinkedinUri()
    {
        return (\request()->route()->uri() === 'api/v1/register/{provider}') ? env('LINKEDIN_REDIRECT_REG') : env('LINKEDIN_REDIRECT');
    }
}
