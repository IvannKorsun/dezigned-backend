<?php

namespace App\Services\Installer;

use App\Http\Resources\Installer\InstallerProjectDetailsEntryResourse;
use App\Http\Resources\Installer\InstallerProjectsCollection;
use App\Models\Project\Measurement;
use App\Services\Bigcommerce\Facades\BcProductRepository;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Resources\Json\ResourceCollection;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;

class InstallerProjectsService
{

    public function all(Request $request): ResourceCollection
    {
        $page =  $request->get('page');
        $limit =  $request->get('limit');
        $sorting = $request->get('sorting');

        $installerProjects = Measurement::where('installer_id', Auth::id())->with(['measurable.area.project.client']);

        if($sorting == 'status'){
            $installerProjects->orderBy('installer_status');
        }
        if($sorting == 'ASC' || $sorting == 'DESC' ){
            $installerProjects->join(
                'product_measure_dates',
                'measurements.id',
                '=',
                'product_measure_dates.product_measure_id'
            )->orderBy('product_measure_dates.date_from', $sorting);
        }
        $installerProjects =  ($page && $limit) ?
            $installerProjects->paginate((int) $limit, '*', 'page', (int) $page) :
            $installerProjects->get();

        $this->mergeBcProducts($installerProjects);

        return new InstallerProjectsCollection($installerProjects);
    }

    public function mergeBcProducts($installerProjects): void
    {
        /** @var Collection $installerProjects */
        if ($installerProjects->isNotEmpty()) {
            $bcProducts = BcProductRepository::getProducts([
                'id:in'          => $installerProjects->pluck('measurable.bc_product_id')
                                                      ->unique()
                                                      ->join(','),
                'include_fields' => 'name',
            ]);
            $installerProjects->each(function ($item) use ($bcProducts) {
                $bcProduct          = $bcProducts->where('id', $item->measurable->bc_product_id)->first();
                $item->product_name = $bcProduct->name ?? null;
            });
        }
    }

    public function getInstallerProject( int $measureId ): JsonResource
    {
        $installerProject = Measurement::where('id', $measureId)
                                       ->with(['dates' ,'measurable.area.project.client'])
                                       ->first();
        if ($installerProject) {
            $this->mergeBcProduct($installerProject);
            return new InstallerProjectDetailsEntryResourse($installerProject);
        }

        return null;
    }

    public function mergeBcProduct($installerProject): void
    {
        $params = ['include_fields' => 'name',];
        /** @var Collection $installerProject */
        if ($installerProject) {
            $bcProduct = BcProductRepository::getProduct(
                $installerProject->measurable->bc_product_id,
                $params
            );
            $installerProject->product_name = $bcProduct->name ?? null;
        }
    }
    public function updateInstallerProject(Request $request, int $id)
    {
        $installerProject = Measurement::findOrFail($id);

        $installerProject->installer_status = $request->get('installer_status');

        return $installerProject->save();
    }
}
