<?php


namespace App\Services\Project;


use App\Http\Resources\Project\AreaProductMeasureResource;
use App\Http\Resources\User\InstallersCollection;
use App\Models\Project\Measurement;
use App\Models\Project\ProjectAreaProduct;
use App\Models\User;
use App\Services\Bigcommerce\Facades\BcProductRepository;
use Grimzy\LaravelMysqlSpatial\Types\Point;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Resources\Json\ResourceCollection;
use Illuminate\Support\Facades\Auth;

class AreaProductMeasureService
{
    /**
     * @var ProjectAreaService
     */
    private $projectAreaService;

    public function __construct(ProjectAreaService $projectAreaService)
    {
        $this->projectAreaService = $projectAreaService;
    }

    public function addMeasure(Request $request): ?JsonResource
    {
        $area = $this->projectAreaService->findArea($request->get('project_id'), $request->get('area_id'));
        $bcProduct = null;

        if (!$area){
            return null;
        }

        $product = $area->products()->where('id', $request->get('product_id'))->first();

        $measure = $this->getMeasure($product);

        if (($measure && $measure->installer_id) || ($measure && $measure->step > 2)){
            return new AreaProductMeasureResource($measure);
        }

        $measure = $this->createMeasure($product, $measure);

        return $measure ? new AreaProductMeasureResource($measure) : null;
    }

    public function createMeasure(?ProjectAreaProduct $product, ?Measurement $measure): ?Measurement
    {
        $attributes = [];

        if (!$product){
            return null;
        }

        if ($measure && !$measure->installer_id){

            $client = $product->area->project->client;

            $installer = $this->findClosestInstaller($client->location);

            $attributes['installer_id'] = $installer ? $installer->id : null;

            $measure->update($attributes);

            return $measure;
        }

        $bcProduct = BcProductRepository::getProduct($product->bc_product_id);

        $client = $product->area->project->client;

        $installer = $this->findClosestInstaller($client->location);

        $attributes['details'] = $product->area->project->description;
        $attributes['product_details'] = $bcProduct->description;
        $attributes['installer_id'] = $installer ? $installer->id : null;
        $attributes['step'] = 1;
        $attributes['fee'] = 250;
        $attributes['installer_status'] = 'awaited';

        return $product->measure()->create($attributes);
    }

    private function getMeasure(?ProjectAreaProduct $product): ?Measurement
    {
        return $product ? $product->measure()->first() : null;
    }

    public function updateMeasure(int $measureId, Request $request): ?JsonResource
    {
        $measure = $this->findMeasure($request->get('project_id'), $request->get('area_id'), $request->get('product_id'), $measureId);

        if ($measure && $measure->step < 3){
            $measure->update($request->all());
            return new AreaProductMeasureResource($measure);
        }

        return null;
    }

    public function findClosestInstaller(?Point $location)
    {
        if ($location){
            return User::role('installer')
                ->selectRaw('*')
                ->where('active', 1)
                ->closestInstaller($location->getLat(), $location->getLng())
                ->first();
        }

        return null;
    }

    public function findMeasure(int $projectId, int $areaId, int $productId, ?int $measureId): ?Model
    {
        $measure =  Measurement::whereHasMorph('measurable', ProjectAreaProduct::class, function ($query) use ($projectId, $areaId, $productId){
            $query->where('id', $productId)
                ->whereHas('area', function ($query) use ($projectId, $areaId) {
                    $query->where('id', $areaId)->whereHas('project', function ($query) use ($projectId){
                        $query->where('id', $projectId)->where('user_id', Auth::id());
                   });
              });
        });

        return $measureId ? $measure->where('measurements.id', $measureId)->first() : $measure->first();
    }

    public function getInstallers($latitude, $longitude): ResourceCollection
    {
        return new InstallersCollection(User::role('installer')->where('active', 1)->selectRaw('*')->closestInstaller($latitude, $longitude)->get());
    }
}
