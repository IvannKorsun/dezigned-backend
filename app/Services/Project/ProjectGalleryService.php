<?php

namespace App\Services\Project;

use App\Http\Resources\GalleryImageCollection;
use App\Http\Resources\GalleryImageResource;
use App\Models\Project\Project;
use App\Models\Project\ProjectImage;
use App\Models\User\GalleryImage;
use App\Services\BaseGalleryService;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\ResourceCollection;
use Illuminate\Support\Facades\Auth;

class ProjectGalleryService extends BaseGalleryService
{
    protected $storageDisk = 's3-project-gallery';

    public function getGalleryImages(Request $request, ?int $page = null, ?int $limit = null): ResourceCollection
    {
        $project = $this->getProject($request->user()->id, $request->route()->parameter('projectId'));

        return $project ? new GalleryImageCollection($this->getAll($request, $project->images(), $page, $limit)) : null;
    }

    public function addGalleryImages(Request $request)
    {
        $project = $this->getProject($request->user()->id, $request->route()->parameter('projectId'));

        if (!$project){
            return null;
        }

        $basePath = $request->user()->id . '/' . $request->route()->parameter('projectId');

        $this->addImages($basePath, $request, $project->images());

        return $this->getGalleryImages($request, 1, 16);
    }

    public function deleteImage(int $projectId, int $imageId): bool
    {
        $project = $this->getProject(Auth::id(), $projectId);

        return $project ? $this->delete($project->images()->where('id', $imageId)->first()) : false;
    }

    public function updateImage(int $projectId, int $imageId, Request $request)
    {
        $project = $this->getProject(Auth::id(), $projectId);

        if ($project){
            $image = $project->images()->where('id', $imageId)->first();
        }

        if (!$project || !$image){
            return null;
        }

        $basePath = $request->user()->id . '/' . $request->route()->parameter('projectId');

        return new GalleryImageResource($this->update($image, $request, $basePath));
    }

    public function getProject(int $userId, int $projectId): ?Project
    {
        return Project::where('user_id', $userId)
            ->where('id', $projectId)
            ->first();
    }

    protected function imagesConfig(): array
    {
        return
            [
                'url_original' => [],
                'url_thumbnail' => ['width' => 290, 'height' => 290],
            ];
    }
}
