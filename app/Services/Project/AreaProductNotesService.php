<?php

namespace App\Services\Project;

use App\Http\Resources\Project\AreaProductNoteResource;
use App\Http\Resources\Project\AreaProductNotesCollection;
use App\Models\Project\ProjectAreaProduct;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Resources\Json\ResourceCollection;
use Illuminate\Support\Facades\Auth;

class AreaProductNotesService extends BaseNotesService
{
    /**
     * @var ProjectAreaService
     */
    private $projectAreaService;

    public function __construct(ProjectAreaService $projectAreaService)
    {
        $this->projectAreaService = $projectAreaService;
    }

    public function getNotes(int $projectId, int $areaId, int $productId, Request $request): ?ResourceCollection
    {
        $areaProduct = $this->findAreaProduct($projectId, $areaId, $productId);

        return $areaProduct ? new AreaProductNotesCollection($this->all($areaProduct->notes(), $request)) : null;
    }

    public function updateOrCreateNote(Request $request, int $projectId, int $areaId, int $productId, ?int $noteId = null): ?JsonResource
    {
        $areaProduct = $this->findAreaProduct($projectId, $areaId, $productId);

        return $areaProduct ? new AreaProductNoteResource($this->updateOrCreate($areaProduct->notes(), $request, $noteId)) : null;
    }

    public function deleteNote(int $projectId, int $areaId, int $productId, int $noteId): bool
    {
        $areaProduct = $this->findAreaProduct($projectId, $areaId, $productId);

        return $areaProduct ? $this->delete($areaProduct->notes(), $noteId) : null;
    }

    public function findAreaProduct(int $projectId, int $areaId, int $productId): ?ProjectAreaProduct
    {
        return ProjectAreaProduct::where('project_area_products.id', $productId)
            ->whereHas('area', function ($query) use ($projectId, $areaId){
                $query->where('id', $areaId)
                    ->whereHas('project', function ($query) use ($projectId){
                        $query->where('id', $projectId)->where('user_id', Auth::id());
                    });
            })->first();
    }
}
