<?php

namespace App\Services\Project;

use App\Http\Resources\Project\ProjectAreaNoteResource;
use App\Http\Resources\Project\ProjectAreaNotesCollection;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Resources\Json\ResourceCollection;

class ProjectAreaNotesService extends BaseNotesService
{
    /**
     * @var ProjectAreaService
     */
    private $projectAreaService;

    public function __construct(ProjectAreaService $projectAreaService)
    {
        $this->projectAreaService = $projectAreaService;
    }

    public function getNotes(int $projectId, int $areaId, Request $request): ResourceCollection
    {
        $area = $this->projectAreaService->findArea($projectId, $areaId);

        return $area ? new ProjectAreaNotesCollection($this->all($area->notes(), $request)) : null;
    }

    public function updateOrCreateNote(Request $request, int $projectId, int $areaId, ?int $noteId): ?JsonResource
    {
        $area = $this->projectAreaService->findArea($projectId, $areaId);

        return $area ? new ProjectAreaNoteResource($this->updateOrCreate($area->notes(), $request, $noteId)) : null;
    }

    public function deleteNote(int $projectId, int $areaId, int $noteId): bool
    {
        $area = $this->projectAreaService->findArea($projectId, $areaId);

        return $area ? $this->delete($area->notes(), $noteId) : null;
    }
}
