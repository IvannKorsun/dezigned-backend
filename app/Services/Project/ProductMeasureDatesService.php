<?php

namespace App\Services\Project;

use App\Http\Resources\Project\ProductMeasureDatesCollection;
use App\Jobs\Email\RequestProductMeasure;
use App\Models\Project\ProductMeasureDates;
use App\Services\Bigcommerce\Facades\BcProductRepository;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\ResourceCollection;

class ProductMeasureDatesService
{
    /**
     * @var AreaProductMeasureService
     */
    private $productMeasure;

    /**
     * ProductMeasureDatesService constructor.
     */
    public function __construct(AreaProductMeasureService $productMeasure)
    {
        $this->productMeasure = $productMeasure;
    }
    //TODO: rewrite  it to getDatesByMeasureId
    public function getDates(int $projectId, int $areaId, int $productId, int $measureId): ?ResourceCollection
    {
        $measure =  $this->productMeasure->findMeasure($projectId, $areaId, $productId, $measureId);

        if (!$measure){
            return null;
        }

        return new ProductMeasureDatesCollection($measure->dates()->get());
    }

    //TODO: need refactoring to separate methods !
    public function storeDates(int $projectId, int $areaId, int $productId, int $measureId, Request $request)
    {
        $measure =  $this->productMeasure->findMeasure($projectId, $areaId, $productId, $measureId);

        if (!$measure){
            return null;
        }

        $action = $request->get('action');
        $status = $action == 'save' ? 'saved' : 'received';

        $dates = $request->get('dates');

        $measureDates = $measure->dates()->get();

        if (empty($dates) && $status === 'saved'){

            $measure->dates()->delete();

            return new ProductMeasureDatesCollection(collect([]));
        }

        if ($measureDates->isNotEmpty()){
            $measure->dates()->delete();
        }

        $dates = array_map(function($item) use ($status){
            return $item += ['status' => $status];
        }, $dates);

        $createdDates = $measure->dates()->createMany($dates);

        if ($status === 'received' && $createdDates->isNotEmpty()){
            $measure->update(['step' => 3]);
            RequestProductMeasure::dispatch($measure->installer, $this->parseBcProductName($measure->measurable->bc_product_id), $createdDates);
        }

        return new ProductMeasureDatesCollection($createdDates);
    }

    public function parseBcProductName(int $id)
    {
        $bcProduct = BcProductRepository::getProduct($id, ['include_fields' => 'id,name']);

        return $bcProduct ? $bcProduct->name : null;
    }


    public function getDatesByMeasureId(int $measureId)
    {
        $dates = ProductMeasureDates::where('product_measure_id', $measureId)->get();
        return $dates ? new ProductMeasureDatesCollection($dates) :null;
    }

    public function updateDate(Request $request, $id)
    {
        $date = ProductMeasureDates::findOrFail($id);

        if($request->get('date_from')){
            $date->date_from = $request->get('date_from');
        }

        if($request->get('date_to')){
            $date->date_to = $request->get('date_to');
        }

        if($request->get('status')){
            if($request->get('status') == 'approved'){
                $approvedBefore = ProductMeasureDates::where('product_measure_id', $date->product_measure_id)
                                                     ->where('status', 'approved')
                                                     ->first();
                if ($approvedBefore && $approvedBefore != $date) {
                    $approvedBefore->status = 'received';
                    $approvedBefore->save();
                }
            }
            $date->status = $request->get('status');
        }

        return $date->save();
    }

}
