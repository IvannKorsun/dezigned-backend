<?php

namespace App\Services\Project;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Http\Request;

class BaseNotesService
{
    public function all(Relation $relation, Request $request)
    {
        $page = (int) $request->get('page');
        $limit = (int) $request->get('limit');

        return ($page && $limit)
            ? $relation->paginate((int) $limit, '*', 'page', (int) $page)
            : $relation->get();
    }

    public function updateOrCreate(Relation $relation, Request $request, ?int $noteId): ?Model
    {
        return $relation->updateOrCreate(['id' => $noteId], $request->all());
    }

    public function delete(Relation $relation, int $noteId): bool
    {
        return $relation->where('id', $noteId)->delete();
    }
}
