<?php


namespace App\Services\Project;


use App\Http\Requests\Project\AddAreaProductRequest;
use App\Http\Requests\Project\UpdateAreaProductRequest;
use App\Http\Requests\Project\ProjectAreaCreateRequest;
use App\Http\Requests\Project\ProjectAreaUpdateRequest;
use App\Http\Resources\Project\ProjectAreaProductsCollection;
use App\Http\Resources\Project\ProjectAreaResourse;
use App\Http\Resources\Project\ProjectAreasCollection;
use App\Models\Project\Project;
use App\Models\Project\ProjectArea;
use App\Services\Bigcommerce\Facades\BcProductRepository;
use App\Services\Product\ProductService;
use App\Services\S3ImagesService;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class ProjectAreaService
{
    /**
     * @var S3ImagesService
     */
    private $imagesService;
    /**
     * @var ProductService
     */
    private $productService;

    public function __construct(S3ImagesService $imagesService, ProductService $productService)
    {
        $this->imagesService = $imagesService;
        $this->productService = $productService;
    }

    public function getAreas(int $projectId, Request $request)
    {
        $page =  $request->get('page');
        $limit =  $request->get('limit');

        $project = Project::where('user_id', Auth::id())->where('id', $projectId)->first();

        if (!$project){
            return null;
        }

        $areas = $project
            ->areas()
            ->search($request->get('search'))
            ->orderByDesc('created_at');

        return ($page && $limit)
            ?  new ProjectAreasCollection($areas->paginate((int) $limit, '*', 'page', (int) $page))
            : new ProjectAreasCollection($areas->get());
    }

    public function getArea(int $projectId, int $areaId)
    {
        $area = $this->findArea($projectId, $areaId);

        if ($area){
            return new ProjectAreaResourse($area);
        }

        return null;
    }

    public function createArea(int $projectId, ProjectAreaCreateRequest $request): ?JsonResource
    {
        $area = null;

        $this->imagesService->setStorageDisk('s3-project-area-cover-img');

        $attributes = $request->except('cover_image');

        $project = Project::where('user_id', Auth::id())->where('id', $projectId)->first();

        if ($project){
            $area = $project->areas()->create($attributes);
        }

        if ($area && $request->get('cover_image')){
            $basePath =  $request->user()->id  .'/'. $project->id . '/' . $area->id;
            $images = $this->imagesService->uploadImage($basePath, $request->get('cover_image'), $this->areaCoverImageConfig());
            $area->update($images);
        }

        return $area ? new ProjectAreaResourse($area) : null;
    }

    public function areaCoverImageConfig(): array
    {
        return [
            'cover_image_original' => [],
            'cover_image_thumbnail' => ['width' => 290, 'height' => 290]
        ];
    }

    public function updateArea(int $projectId, int $areaId, ProjectAreaUpdateRequest $request): ?JsonResource
    {
        $this->imagesService->setStorageDisk('s3-project-area-cover-img');

        $attributes = $request->except('cover_image');

        $area = $this->findArea($projectId, $areaId);

        if ($area){
            $area->update($attributes);
        }

        if ($area && $request->get('cover_image')){
            Storage::disk('s3-project-area-cover-img')->delete([$area->getOriginal('cover_image_original'), $area->getOriginal('cover_image_thumbnail')]);
            $basePath =  $request->user()->id  .'/'. $projectId . '/' . $areaId;
            $images = $this->imagesService->uploadImage($basePath, $request->get('cover_image'), $this->areaCoverImageConfig());
            $area->update($images);
        }

        return $area ? new ProjectAreaResourse($area) : null;

    }

    public function deleteArea(int $projectId, int $areaId): bool
    {
        $area = $this->findArea($projectId, $areaId);

        if ($area){
            Storage::disk('s3-project-area-cover-img')->deleteDirectory(Auth::id()  .'/'. $projectId . '/' . $areaId);
            $area->delete();
            return true;
        }
        return false;
    }

    public function getProducts(int $projectId, int $areaId, Request $request)
    {
        $area = $this->findArea($projectId, $areaId);

        if (!$area){
            return null;
        }

        $page = $request->get('page');
        $limit = $request->get('limit');

        $areaProducts =  ($page && $limit) ? $area->products()->paginate((int) $limit, '*', 'page', (int) $page) : $area->products()->get();

        $this->mergeBcProducts($areaProducts);

        return new ProjectAreaProductsCollection($areaProducts);
    }

    public function mergeBcProducts($areaProducts): void
    {
        /** @var Collection $areaProducts */
        if ($areaProducts->isNotEmpty()) {

            $bcProducts = BcProductRepository::getProducts([
                'id:in' => $areaProducts->pluck('bc_product_id')->unique()->join(','),
                'include' => 'images,custom_fields',
                'include_fields' => 'name,description,brand_id,categories,price',

            ]);

            $areaProducts->each(function ($item) use ($bcProducts) {
                $bcProduct = $bcProducts->where('id', $item->bc_product_id)->first();
                $item->name = $bcProduct->name ?? null;
                $item->description = $bcProduct->description ?? null;
                $item->price = $bcProduct->price ?? null;
                $item->images = $bcProduct->images ?? null;
            });
        }

    }

    public function addProduct(int $projectId, AddAreaProductRequest $request)
    {
        $areas = $this->findAreas($projectId, $request->get('area_ids'));

        if (!$areas->isNotEmpty()){
            return false;
        }

        foreach ($areas as $area){
            $product = $area->products()->where('bc_product_id', $request->get('bc_product_id'))->first();

            if ($product){
                $product->qty = $product->qty + $request->get('qty');
                $product->save();
            }else{
                $area->products()->create($request->except('area_ids'));
            }
        }
        return true;
    }

    public function updateProduct(int $projectId, int $areaId, UpdateAreaProductRequest $request)
    {
        $area = $this->findArea($projectId, $areaId);

        if (!$area){
            return false;
        }

        $product = $area->products()->where('id', $request->get('id'))->first();

        if ($product){
            $product->update($request->all());
            return true;
        }

        return true;
    }

    public function deleteProduct(int $projectId, int $areaId, int $productId)
    {
        $area = $this->findArea($projectId, $areaId);

        if (!$area){
            return false;
        }

        $product = $area->products()->where('id', $productId)->first();

        if ($product){
            $product->delete();
            return true;
        }

        return false;
    }

    public function baseFindArea(int $projectIds, $areaIds): Builder
    {
        $areaIds = (array) $areaIds;

        return ProjectArea::select('project_areas.*')
            ->join('projects', 'project_areas.project_id', '=', 'projects.id')
            ->whereIn('project_areas.id', $areaIds)
            ->where('projects.id', $projectIds)
            ->where('projects.user_id', Auth::id());
    }

    public function findArea(int $projectId, int $areaId)
    {
        return $this->baseFindArea($projectId, $areaId)->first();
    }

    public function findAreas(int $projectId, array $areaIds)
    {
        return $this->baseFindArea($projectId, $areaIds)->get();
    }
}
