<?php

namespace App\Services\Project;

use App\Http\Resources\Project\ProjectResource;
use App\Http\Resources\Project\ProjectsCollection;
use App\Models\Project\Project;
use App\Services\S3ImagesService;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Resources\Json\ResourceCollection;
use Illuminate\Support\Facades\Auth;

class ProjectService
{
    /**
     * @var S3ImagesService
     */
    private $imagesService;

    public static $storageS3name = 's3-project-cover-images';

    public function __construct(S3ImagesService $imagesService)
    {
        $this->imagesService = $imagesService;
        $this->imagesService->setStorageDisk(self::$storageS3name);
    }

    public function all(Request $request): ResourceCollection
    {
        $page =  $request->get('page');
        $limit =  $request->get('limit');
        $active = ($request->get('active') == '0') ? 0 : 1;
        $search = $request->get('search');

        $projects = Project::where('user_id', Auth::id())->where('active', $active);

        return ($page && $limit)
            ?  new ProjectsCollection($projects->search($search)->paginate((int) $limit, '*', 'page', (int) $page))
            : new ProjectsCollection($projects->search($search)->get());
    }

    public function getProject(int $id): ?JsonResource
    {
        $project = Project::where('user_id', Auth::id())->where('id', $id)->first();

        if ($project){
            return new ProjectResource($project);
        }
        return null;
    }

    public function updateProject(Request $request, $id)
    {
        $project = Project::where('user_id', Auth::id())->where('id', $id)->first();

        if (!$project){
            return null;
        }

        $project->update($request->except('cover_image'));

        if ($request->get('cover_image')){

            $this->imagesService->delete([$project->getOriginal('cover_image_original'), $project->getOriginal('cover_image_thumbnail')]);

            $basePath =  $request->user()->id  .'/'. $project->id;

            $images = $this->imagesService->uploadImage($basePath, $request->get('cover_image'), $this->imagesConfig());

            $project->update($images);
        }

        return new ProjectResource($project);
    }

    public function deleteProject(int $id)
    {
        $project = Project::where('user_id', Auth::id())->where('id', $id)->first();

        if (!$project){
            return false;
        }

        $s3Paths = [];

        $s3Paths[] = $project->getOriginal('cover_image_thumbnail');
        $s3Paths[] = $project->getOriginal('cover_image_original');

        $this->imagesService->delete($s3Paths);

        return $project->delete();
    }

    public function createProject(Request $request)
    {
        $attributes = $request->except('images');

        $attributes['user_id'] = $request->user()->id;
        $attributes['active'] = 1;

        $project = Project::create($attributes);

        if ($request->get('cover_image')){
            $basePath =  $request->user()->id  .'/'. $project->id;

            $image = $this->imagesService->uploadImage($basePath, $request->get('cover_image'), $this->imagesConfig());

            $project->update($image);
        }

        return new ProjectResource($project);
    }

    public function imagesConfig()
    {
        return [
            'cover_image_original' => [],
            'cover_image_thumbnail' => ['width' => 290, 'height' => 290]
        ];
    }
}
