<?php

namespace App\Services\Project;

use App\Http\Resources\GalleryImageCollection;
use App\Http\Resources\GalleryImageResource;
use App\Services\BaseGalleryService;
use App\Services\S3ImagesService;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\ResourceCollection;

class ProjectAreaGalleryService extends BaseGalleryService
{
    /**
     * @var S3ImagesService
     */
    protected $imagesService;
    /**
     * @var ProjectAreaService
     */
    private $projectAreaService;

    protected $storageDisk = 's3-project-area-gallery';

    public function __construct(S3ImagesService $imagesService, ProjectAreaService $projectAreaService)
    {
        parent::__construct($imagesService);
        $this->projectAreaService = $projectAreaService;
    }

    public function getGalleryImages(Request $request, ?int $page = null, ?int $limit = null): ?ResourceCollection
    {
        $area = $this->projectAreaService->findArea($request->route()->parameter('projectId'), $request->route()->parameter('areaId'));

        if (!$area){
            return null;
        }

        return new GalleryImageCollection($this->getAll($request, $area->galleryImages(), $page, $limit));
    }

    public function addGalleryImages(Request $request)
    {
        $area = $this->projectAreaService->findArea($request->route()->parameter('projectId'), $request->route()->parameter('areaId'));

        if (!$area){
            return null;
        }

        $basePath = $request->user()->id . '/' . $request->route()->parameter('projectId') . '/' . $request->route()->parameter('areaId');

        $this->addImages($basePath, $request, $area->galleryImages());

        return $this->getGalleryImages($request, 1, 16);
    }

    public function deleteImage(int $projectId, int $areaId, int $imageId)
    {
        $area = $this->projectAreaService->findArea($projectId, $areaId);

        if (!$area){
            return null;
        }

        return $this->delete($area->galleryImages()->where('id', $imageId)->first());
    }

    public function updateImage(int $projectId, int $areaId, int $imageId, Request $request)
    {
        $image = null;
        $area = $this->projectAreaService->findArea($projectId, $areaId);

        if ($area){
            $image = $area->galleryImages()->where('id', $imageId)->first();
        }

        if (!$area || !$image){
            return null;
        }

        $basePath = $request->user()->id . '/' . $projectId . '/' . $areaId;

        return new GalleryImageResource($this->update($image, $request, $basePath));
    }

    protected function imagesConfig(): array
    {
        return
            [
                'url_original' => [],
                'url_thumbnail' => ['width' => 290, 'height' => 290],
            ];
    }
}
