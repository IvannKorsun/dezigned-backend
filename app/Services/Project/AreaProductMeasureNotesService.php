<?php

namespace App\Services\Project;

use App\Http\Resources\Project\AreaProductMeasureNoteResource;
use App\Http\Resources\Project\AreaProductMeasureNotesCollection;
use App\Models\Project\AreaProductMeasureNotes;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Resources\Json\ResourceCollection;

class AreaProductMeasureNotesService extends BaseNotesService
{
    /**
     * @var AreaProductMeasureService
     */
    private $measureService;

    /**
     * AreaProductMeasureNotesService constructor.
     */
    public function __construct(AreaProductMeasureService $measureService)
    {
        $this->measureService = $measureService;
    }

    public function getNotes(int $projectId, int $areaId, int $productId, int $measureId, Request $request): ?ResourceCollection
    {
        $measure = $this->measureService->findMeasure($projectId, $areaId, $productId, $measureId);

        return $measure ? new AreaProductMeasureNotesCollection($this->all($measure->notes(), $request)) : null;
    }
    public function getNotesByMeasureId(int $measureId): ?ResourceCollection
    {
        $dates = AreaProductMeasureNotes::where('area_product_measure_id', $measureId)->get();
        return $dates ? new AreaProductMeasureNotesCollection($dates) : null;
    }

    public function updateOrCreateNote(Request $request, int $projectId, int $areaId, int $productId, int $measureId, ?int $noteId = null): ?JsonResource
    {
        $measure = $this->measureService->findMeasure($projectId, $areaId, $productId, $measureId);

        return $measure ? new AreaProductMeasureNoteResource($this->updateOrCreate($measure->notes(), $request, $noteId)) : null;
    }

    public function deleteNote(int $projectId, int $areaId, int $productId, int $measureId, int $noteId): bool
    {
        $measure = $this->measureService->findMeasure($projectId, $areaId, $productId, $measureId);

        return $measure ? $this->delete($measure->notes(), $noteId) : null;
    }

}
