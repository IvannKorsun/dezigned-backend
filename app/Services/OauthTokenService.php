<?php
namespace App\Services;

use App\Models\User;
use Illuminate\Http\Request;

class OauthTokenService
{

    public function getAccessTokens(User $user, Request $request)
    {
        $params = [
            'grant_type' => 'password',
            'client_id' => env('CLIENT_ID'),
            'client_secret' => env('CLIENT_SECRET'),
            'username' => $user->email,
            'password' => $request->get('password'),
            'scope' => ''
        ];

       request()->request->add($params);

        $proxy = Request::create(
            'oauth/token',
            'POST'
        );

        return json_decode(\Route::dispatch($proxy)->content(), true);
    }

    public function getAccessTokensForSocialAuth(string $provider, string $accessToken)
    {
        $params = [
            'grant_type' => 'social',
            'client_id' => env('CLIENT_ID'),
            'client_secret' => env('CLIENT_SECRET'),
            'provider' => $provider,
            'access_token' => $accessToken
        ];

        request()->request->add($params);

        $proxy = Request::create(
            'oauth/token',
            'POST'
        );

        return json_decode(\Route::dispatch($proxy)->content(), true);
    }

    public function refreshToken(Request $request)
    {
        $params = [
            'grant_type' => 'refresh_token',
            'refresh_token' => $request->refresh_token,
            'client_id' => env('CLIENT_ID'),
            'client_secret' => env('CLIENT_SECRET'),
            'scope' => ''
        ];

        request()->request->add($params);

        $proxy = Request::create(
            'oauth/token',
            'POST'
        );

        return json_decode(\Route::dispatch($proxy)->content(), true);
    }
}
