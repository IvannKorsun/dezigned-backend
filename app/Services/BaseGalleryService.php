<?php

namespace App\Services;

use App\Models\BaseGallery;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

abstract class BaseGalleryService
{
    protected  $storageDisk;
    /**
     * @var S3ImagesService
     */
    protected $imagesService;

    public function __construct(S3ImagesService $imagesService)
    {
        $this->imagesService = $imagesService;
        $this->imagesService->setStorageDisk($this->storageDisk);
    }

    protected function getAll(Request $request, Relation $relation, ?int $page = null, ?int $limit = null): ?object
    {
            $page = $request->get('page') ?? $page;
            $limit = $request->get('limit') ?? $limit;

            $relation->sorting($request->get('sort'))
                ->search($request->get('search'));

        return ($page && $limit) ? $relation->paginate((int) $limit, '*', 'page', (int) $page) : $relation->get();
    }

    protected function addImages(string $basePath, Request $request, Relation $relation)
    {
        $images = $request->input('images.*.image');
        $names = $request->input('images.*.name');

        $images = $this->imagesService->uploadImages($basePath, $images, $this->imagesConfig());

        $merged = [];

        foreach ($images as $key => $value){
            $merged[$key]['name'] = $names[$key];
            $merged[$key] += $value;
        }

        $relation->createMany($merged);
    }

    protected function delete(?BaseGallery $galleryModel)
    {
        if (!$galleryModel) {
            return false;
        }

        Storage::disk($this->storageDisk)->delete($this->getS3Paths($this->imagesConfig(), $galleryModel));

        return $galleryModel->delete();
    }

    public function update(BaseGallery $galleryModel, Request $request, string $basePath): ?BaseGallery
    {
        if ($request->get('image')){

            Storage::disk($this->storageDisk)->delete($this->getS3Paths($this->imagesConfig(), $galleryModel));

            $newImage = $this->imagesService->uploadImage($basePath, $request->get('image'), $this->imagesConfig());

            $galleryModel->update($newImage);
        }

        if ($request->get('name')){
            $galleryModel->update(['name' => $request->get('name')]);
        }

        return $galleryModel;
    }

    public function getS3Paths(array $config, BaseGallery $galleryModel): array
    {
        $s3Paths = [];

        foreach ($config as $key => $value){
            $s3Paths[] = $galleryModel->getOriginal($key);
        }

        return $s3Paths;
    }

    abstract protected function imagesConfig() :array;
}
