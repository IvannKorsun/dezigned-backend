<?php

namespace App\Services;

use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class S3ImagesService
{
    /** @var string $storageDisc */
    public $storageDisc;

    public function uploadImages(string $basePath, array $images, array $config)
    {
        $imagesUrls = [];

        foreach ($images as $key => $image){

            $filename = Carbon::now()->timestamp . uniqid() . '.' . explode('/', explode(';base64', $image)[0])[1];

            $imagesUrls[$key] = $this->upload($basePath, $config, $filename, $image);
        }

        return $imagesUrls;
    }

    public function uploadImage(string $basePath, string $image, array $config)
    {
        $filename = Carbon::now()->timestamp . uniqid() . '.' . explode('/', explode(';base64', $image)[0])[1];

        return $this->upload($basePath, $config, $filename, $image);
    }

    public function upload(string $basePath, array $config, string $filename, string $image): array
    {
        $urls = [];

        $extension = explode('/', explode(';base64', $image)[0])[1];

        foreach ($config as $sizeName => $values){
            $path = $basePath . '/' . $sizeName . $filename;
            Storage::disk($this->storageDisc)->put($path, $this->resize($image, $values, $extension), 'public');
            $urls[$sizeName] = $path;
        }

        return $urls;
    }

    public function delete(?array $paths): void
    {
        Storage::disk($this->storageDisc)->delete($paths);
    }

    public function resize(string $image, array $config, string $extension)
    {
        return (isset($config['width']) && isset($config['height']))
            ? Image::make($image)->fit($config['width'], $config['height'])->encode($extension)
            : Image::make($image)->encode($extension);
    }

    public function setStorageDisk(string $storageDisc): void
    {
        $this->storageDisc = $storageDisc;
    }
}
