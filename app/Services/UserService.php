<?php

namespace App\Services;

use App\Events\UserUpdated;
use App\Http\Requests\Auth\LoginRequest;
use App\Http\Requests\User\UserUpdateRequest;
use App\Http\Resources\User\UserResource;
use App\Models\SharedProfile;
use App\Models\User;
use App\Services\Bigcommerce\Repositories\BcUserRepository;
use Grimzy\LaravelMysqlSpatial\Types\Point;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Laravel\Passport\Passport;

class UserService
{
    /**
     * @var BcUserRepository
     */
    private $bcUserRepository;

    /**
     * UserService constructor.
     */
    public function __construct(BcUserRepository $bcUserRepository)
    {
        $this->bcUserRepository = $bcUserRepository;
    }

    public function updateProfile(UserUpdateRequest $request): ?JsonResource
    {
        $user = Auth::user();

        $attributes = $request->except(['avatar', 'avatar_url', 'bc_user_id', 'location']);

        if ($request->get('avatar')){
            $attributes['avatar_url'] = $this->uploadAvatarS3($user, $request->get('avatar'));
        }

        if ($request->get('location')){
            $user->location = new Point($request->get('location')['lat'], $request->get('location')['lng']);
        }

        $user->update($attributes);

        if ($request->hasAny(['last_name', 'first_name', 'email', 'phone'])){
            event(new UserUpdated($user));
        }
        return new UserResource($user);
    }

    public function saveUser(User $user)
    {
        $bcUser =
            [
                'last_name' => $user->last_name,
                'first_name' => $user->first_name,
                'email' => $user->email,
                'phone' => $user->phone_number,
            ];

        $savedUser = $this->bcUserRepository->saveUser([$bcUser]);

        $user->update(['bc_user_id' => $savedUser[0]['id']]);

        return $user;
    }

    public function updateUser(User $user)
    {
        if ($user->bc_user_id) {
            $bcUser =
                [
                    'id' => $user->bc_user_id,
                    'last_name' => $user->last_name,
                    'first_name' => $user->first_name,
                    'email' => $user->email,
                    'phone' => $user->phone_number,
                ];

            return $this->bcUserRepository->updateUser([$bcUser]);
        }

        return null;
    }

    public function uploadAvatarS3(Authenticatable $user, string $avatar): string
    {
        $avatarImage = explode(";base64,", $avatar)[1];

        $extension = explode('/', explode(';', $avatar)[0])[1];

        $path = $user->id . '/avatar_' . Carbon::now()->timestamp . '.' . $extension;

        if ($user->avatar_url){
            Storage::disk('s3-avatars')->delete($user->id . '/' . explode($user->id . '/', $user->avatar_url)[1]);
        }

        Storage::disk('s3-avatars')->put($path, base64_decode($avatarImage), 'public');

        return Storage::disk('s3-avatars')->url($path);
    }

    public function changePassword(User $user, Request $request)
    {
        if (Hash::check($request->get('current_password'), $user->password)){
            $user->update(['password' => $request->get('new_password')]);
            return true;
        }
        return false;
    }

    public function createSharedProfile(): string
    {
        $user = Auth::user();

        if ($user->profileShareableLink){
            return $user->profileShareableLink->token;
        }

        $token = Str::random(64) . $user->id;

        SharedProfile::create(
            ['token' => $token, 'user_id' => $user->id]
        );

        return $token;
    }

    public function getSharedProfile(string $token): ?JsonResource
    {
        $profileLink = SharedProfile::where('token', $token)->first();

        if ($profileLink){
            return new UserResource($profileLink->user);
        }

        return null;
    }

    public function loginUser(LoginRequest $request, OauthTokenService $tokenService): array
    {
        $login = $request->get('login');

        $user = User::where('name', $login)->orWhere('email', $login)->first();

        if (!$user || $user->active !== 1){
            return ['error' => 'Sorry, we couldn\'t find an account with that username.'];
        }

        if (!Hash::check($request->get('password'), $user->password)){
            return ['error' => 'Sorry, that password isn\'t right.'];
        }

        if ($request->get('remember-me') == 'true'){
            Passport::tokensExpireIn(\Carbon\Carbon::now()->addDays(7));
        }

        $token = $tokenService->getAccessTokens($user, $request);

        return array_merge($token, ['user' => $user]);

    }

}
