<?php

namespace App\Services\Client;

use App\Http\Requests\Client\CreateClientRequest;
use App\Http\Resources\Client\ClientResource;
use App\Http\Resources\Client\ClientsCollection;
use App\Models\Client\Client;
use Grimzy\LaravelMysqlSpatial\Types\Point;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Auth;

class ClientService
{
    public function addNewClient(CreateClientRequest $request)
    {
        $attributes = $request->except('location');
        $attributes['user_id'] = Auth::id();
        $attributes['active'] = 1;

        $client = new Client;
        $client->fill($attributes);
        $client->location = new Point($request->get('location')['lat'], $request->get('location')['lng']);
        $client->save();

        return new ClientResource($client);
    }

    public function getClients(Request $request)
    {
        $page = $request->get('page');
        $limit = $request->get('limit');
        $active = ($request->get('active') === '0') ? 0 : 1;
        $search = $request->get('search');
        $sort = $this->applySorting($request->get('sort'));

        $clients = Client::search($search)
            ->sorting($sort)
            ->where('user_id', Auth::id())
            ->where('active', $active);

        return ($page && $limit)
            ? new ClientsCollection($clients->paginate((int) $limit, '*', 'page', (int) $page))
            : new ClientsCollection($clients->get());
    }

    public function applySorting(?string $sort)
    {
        $params = ['name', 'asc'];
        if (!$sort){
            return $params;
        }

        $sort = explode(',', $sort);

        if (Client::hasSort($sort[0])){
           $params = $sort;
        }

        return $params;
    }

    public function updateClient(int $id, Request $request)
    {
        $client = Client::where('user_id', Auth::id())
            ->where('id', $id)
            ->first();

        if (!$client){
            return null;
        }

        $attributes = $request->except('location');

        if ($request->get('location')){
            $client->location = new Point($request->get('location')['lat'], $request->get('location')['lng']);
        }

        $client->update($attributes);

        return new ClientResource($client);
    }

    public function deleteClient(int $id)
    {
        $client = Client::where('user_id', Auth::id())->where('id', $id)->first();

        if ($client){
            return $client->delete();
        }
        return false;
    }

    public function getClient(int $id): ?JsonResource
    {
        $client = Client::where('user_id', Auth::id())->where('id', $id)->first();

        if ($client){
            return new ClientResource($client);
        }
        return null;
    }
}
