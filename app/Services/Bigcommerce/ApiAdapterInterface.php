<?php


namespace App\Services\Bigcommerce;


interface ApiAdapterInterface
{
    public function get(string $url): ?array;

    public function post(string $url, array $data): ?array;

    public function put(string $url, array $data): ?array;
}
