<?php

namespace App\Services\Bigcommerce\Resources;

class Resource
{
    public function __construct(?array $data)
    {
            $this->setFields($data);
    }

    protected function setFields($data)
    {
        foreach ($data as $key => $value){
            $this->setClassValue($key, $value);
        }
    }

    protected function setClassValue($key, $value)
    {
        $method = 'set' . str_replace('_', '', $key);

        if (method_exists(static::class, $method)){
            $this->$method($value);
        }else{
            $this->$key = $value;
        }

    }

    public function __get($field)
    {
        return (isset($this->field)) ? $this->field : null;
    }

    public function __set($field, $value)
    {
        $this->$field = $value;
    }

    public static function fromData(?array $data): ?self
    {
        if (isset($data['data']) && !empty($data['data'])){
            return new static($data['data']);
        }

        if (!empty($data)){
            return new static($data);
        }
        return null;
    }
}
