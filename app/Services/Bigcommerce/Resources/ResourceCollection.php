<?php


namespace App\Services\Bigcommerce\Resources;


use Illuminate\Support\Collection;

class ResourceCollection
{
    public static function collection(array $data, string $object = Resource::class, $meta = true): ?Collection
    {
        $collection = collect();

        if (isset($data['meta']) && $meta){
            $collection['meta'] = $data['meta'];
        }

        if (isset($data['data']) && !empty($data['data'])){

            if (isset($data['meta']) && $meta){
                $collection['meta'] = $data['meta'];
            }

            foreach ($data['data'] as $values){
                $collection->push(new $object($values));
            }

            return $collection;
        }
        return null;
    }
}
