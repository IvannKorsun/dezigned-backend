<?php

namespace App\Services\Bigcommerce\Resources;

use App\Services\Bigcommerce\Facades\BcProductRepository;
use Illuminate\Support\Collection;

class Product extends Resource
{
    private $brand_id;
    /** @var array|null */
    public $brand;
    public $id;
    public $name;
    public $description;
    public $price;
    public $images;
    public $custom_fields;
    public $categories;

    public function __construct(array $data, ?array $additionalParams = null)
    {
        parent::__construct($data);

        if ($additionalParams){
            $this->setBrand($data['brand_id'], $additionalParams['brands'] ?? null);
        }
    }

    public function setBrand(int $id, ?Collection $brands = null): self
    {
        if ($brands){
            $brand = $brands->where('id', $id)->first();
        }else{
            $brand = BcProductRepository::getBrand($id, ['include_fields' => 'id,name']);
        }
        $this->brand = $brand;

        return $this;
    }

    protected function setClassValue($key, $value)
    {
        $method = 'set' . str_replace('_', '', $key);

        if (method_exists(static::class, $method)){
            $this->$method($value);
        }
    }

    /**
     * @return mixed
     */
    public function getBrandId()
    {
        return $this->brand_id;
    }

    /**
     * @param mixed $brand_id
     */
    public function setBrandId($brand_id): void
    {
        $this->brand_id = $brand_id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @param mixed $name
     */
    public function setName($name): void
    {
        $this->name = $name;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description): void
    {
        $this->description = $description;
    }

    /**
     * @param mixed $price
     */
    public function setPrice($price): void
    {
        $this->price = $price;
    }

    /**
     * @param mixed $images
     */
    public function setImages($images): void
    {
        $this->images = $images;
    }

    /**
     * @param mixed $custom_fields
     */
    public function setCustomFields($custom_fields): void
    {
        $this->custom_fields = $custom_fields;
    }

    /**
     * @param mixed $categories
     */
    public function setCategories($categories): void
    {
        $this->categories = $categories;
    }
}
