<?php


namespace App\Services\Bigcommerce\Resources;

use Illuminate\Support\Collection;

class ProductsCollection extends ResourceCollection
{
    public static function collection(array $data, string $object = Resource::class, $meta = true, array $additionalParams = []): ?Collection
    {
        $collection = collect();

        !isset($data['meta']) ?: $collection['meta'] = $data['meta'];

        foreach ($data['data'] as $values){
            $collection->push(new $object($values, $additionalParams));
        }

        return $collection;
    }
}
