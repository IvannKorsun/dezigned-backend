<?php

namespace App\Services\Bigcommerce\Facades;

use Illuminate\Support\Facades\Facade;

class BcProductRepository extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'bcProduct';
    }
}
