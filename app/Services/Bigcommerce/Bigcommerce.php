<?php

namespace App\Services\Bigcommerce;

use App\Exceptions\BigcommerseApiException;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Support\Facades\Log;

class Bigcommerce implements ApiAdapterInterface
{
    private $api_path;
    private $stores_prefix = '/stores/%s/v3';
    private $api_url = 'https://api.bigcommerce.com';
    /**
     * @var Client
     */
    private $client;

    public function __construct()
    {
        $this->client = new Client(['headers' => [
            'X-Auth-Token' => env('BC_ACCESS_TOKEN', ''),
            'X-Auth-Client' => env('BC_CLIENT_ID', ''),
            'Content-Type' => 'application/json',
        ]
        ]);

        $this->api_path = $this->api_url .  sprintf($this->stores_prefix, env('BC_STORE_HASH'));
    }

    public function get(string $url): ?array
    {
        $response = [];

        try{
            $response = $this->client->get($url)->getBody()->getContents();
        }catch(GuzzleException $e){
            Log::error($e->getMessage());
        }

        return !empty($response) ? json_decode($response, true) ?? [] : null;
    }


    public function post(string $url, array $data): ?array
    {
        try{
            $response = $this->client->post($url,['body' => json_encode($data)])->getBody()->getContents();

            return !empty($response) ? json_decode($response, true) ?? [] : null;

        }catch(GuzzleException $e){
            Log::error($e->getMessage());
            throw new BigcommerseApiException($e->getMessage());
        }
    }

    public function put(string $url, array $data): ?array
    {
        try{
            $response = $this->client->put($url,['body' => json_encode($data)])->getBody()->getContents();

            return !empty($response) ? json_decode($response, true) ?? [] : null;

        }catch(GuzzleException $e){
            Log::error($e->getMessage());
            throw new BigcommerseApiException($e->getMessage());
        }
    }

    /**
     * @return string
     */
    public function getApiPath(): string
    {
        return $this->api_path;
    }
}
