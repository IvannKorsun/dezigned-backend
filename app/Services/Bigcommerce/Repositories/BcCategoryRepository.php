<?php


namespace App\Services\Bigcommerce\Repositories;


use App\Services\Bigcommerce\Resources\Category;
use App\Services\Bigcommerce\Resources\Resource;
use App\Services\Bigcommerce\Resources\ResourceCollection;
use Illuminate\Support\Collection;

class BcCategoryRepository extends BaseRepository
{
    public function getCategoriesTree(): ?Collection
    {
        $path = '/catalog/categories/tree';

        return ResourceCollection::collection($this->all($path), Resource::class, false);
    }

    public function getCategory(int $id, array $filters): ?Category
    {
        $path = '/catalog/categories';

        return Category::fromData($this->find($id, $path, $filters));
    }
}
