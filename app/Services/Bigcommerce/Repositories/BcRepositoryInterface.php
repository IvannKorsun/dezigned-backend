<?php


namespace App\Services\Bigcommerce\Repositories;


interface BcRepositoryInterface
{
    public function all(string $path, array $filters = []): ?array;

    public function find(int $id, string $path, array $filters = []): ?array;

    public function save(string $path, array $data, array $filters = []): ?array;

    public function update(int $id, string $path, array $data, array $filters = []): ?array;
}
