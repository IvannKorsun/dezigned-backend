<?php

namespace App\Services\Bigcommerce\Repositories;

use App\Services\Bigcommerce\ApiAdapterInterface;
use App\Services\Bigcommerce\Bigcommerce;
use App\Services\Bigcommerce\Filter;

class BaseRepository implements BcRepositoryInterface
{
    /**
     * @var Bigcommerce
     */
    protected $adapter;

    public function __construct(ApiAdapterInterface $adapter)
    {
        $this->adapter = $adapter;
    }

    public function all(string $path, array $filters = []): ?array
    {
        $filter = Filter::create($filters);

        $url = $this->adapter->getApiPath() . $path . $filter->toQuery();

        return $this->adapter->get($url);
    }

    public function find(int $id, string $path, array $filters = []): ?array
    {
        $filter = Filter::create($filters);

        $url = $this->adapter->getApiPath() . $path . '/' . $id . $filter->toQuery();

        return $this->adapter->get($url);
    }

    public function save(string $path, array $data, array $filters = []): ?array
    {
        $filter = Filter::create($filters);

        $url = $this->adapter->getApiPath() . $path . $filter->toQuery();

        return $this->adapter->post($url, $data);
    }

    public function update(int $id, string $path, array $data, array $filters = []): ?array
    {
        $filter = Filter::create($filters);

        $url = $this->adapter->getApiPath() . $path . '/' . $id . $filter->toQuery();

        return $this->adapter->put($url, $data);
    }
}
