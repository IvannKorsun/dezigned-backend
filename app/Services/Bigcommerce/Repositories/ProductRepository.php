<?php

namespace App\Services\Bigcommerce\Repositories;

use App\Services\Bigcommerce\Resources\Brand;
use App\Services\Bigcommerce\Resources\Product;
use App\Services\Bigcommerce\Resources\ProductsCollection;
use App\Services\Bigcommerce\Resources\Resource;
use App\Services\Bigcommerce\Resources\ResourceCollection;

class ProductRepository extends BaseRepository
{
    public function getProducts($filters = [])
    {
        $path = '/catalog/products';

        $additionalParams =
            [
                'brands' => $this->getBrands(['include_fields' => 'name'])
            ];

        return  ProductsCollection::collection($this->all($path, $filters), Product::class, true, $additionalParams);
    }

    public function getProduct(int $id, $filters = [])
    {
        $path = '/catalog/products';

        return Product::fromData($this->find($id, $path, $filters));
    }

    public function saveProduct(array $data, array $filters = [])
    {
        $path = '/catalog/products';

        return Product::fromData($this->save($path, $data, $filters));
    }

    public function getBrands(array $filters = ['include_fields' => 'id,name'])
    {
        $path = '/catalog/brands';

        return ResourceCollection::collection($this->all($path, $filters), Resource::class, false);
    }

    public function getBrand(int $id, array $filters = ['include_fields' => 'id,name'])
    {
        $path = '/catalog/brands';

        return Brand::fromData($this->find($id, $path, $filters));
    }

    public function getCategories($filters = [])
    {
        $path = '/catalog/categories';

        return ResourceCollection::collection($this->all($path, $filters), Resource::class, false);
    }

    public function saveCustomField(int $id, array $data, array $filters = [])
    {
        $path = '/catalog/products/' . $id . '/custom-fields';

        return Resource::fromData($this->save($path, $data, $filters));
    }

    public function saveImage(int $id, $data)
    {
        $path = '/catalog/products/' . $id . '/images';

        return Resource::fromData($this->save($path, $data));
    }

    public function getMaxPrice(int $id)
    {
        $path = '/catalog/products?sort=price&direction=desc&limit=1&categories:in=' . $id;

        return $this->all($path)['data'][0]['price'] ?? 0;
    }
}
