<?php

namespace App\Services\Bigcommerce\Repositories;

class BcUserRepository extends BaseRepository
{
    public function saveUser(array $bcUser)
    {
        $path = '/customers';

        return $this->save($path, $bcUser)['data'];
    }

    public function updateUser(array $bcUser)
    {
        $path = '/customers';

        $url = $this->adapter->getApiPath() . $path;

        return $this->adapter->put($url, $bcUser)['data'];
    }
}
