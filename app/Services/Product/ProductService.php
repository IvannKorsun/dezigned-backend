<?php
namespace App\Services\Product;

use App\Http\Requests\FilterRequest;
use App\Models\Product\Product;
use App\Services\Bigcommerce\Facades\BcProductRepository;
use App\Services\Bigcommerce\Repositories\BcCategoryRepository;
use App\Services\Bigcommerce\Resources\Product as BcProduct;
use Illuminate\Database\Eloquent\Collection as EloCollection;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class ProductService
{
    private $bcProductFields = 'name,description,brand_id,categories,price';
    /**
     * @var FilterService
     */
    private $filterService;
    /**
     * @var BcCategoryRepository
     */
    private $bcCategoryRepository;

    public function __construct(FilterService $filterService, BcCategoryRepository $bcCategoryRepository)
    {
        $this->filterService = $filterService;
        $this->bcCategoryRepository = $bcCategoryRepository;
    }

    public function getProducts(FilterRequest $request)
    {
        $defaults =
            [
                'include' => 'images,custom_fields',
                'include_fields' => $this->bcProductFields,
                'page' => 1,
                'limit' => 18
            ];

        $filters = $this->filterService->filter($request);

        if (!empty($filters)){
            $defaults = array_merge($defaults, $filters);
        }

        $bcProducts = BcProductRepository::getProducts($defaults);

        $bcProducts = $this->mergeS3Images($bcProducts);

        $result = [];
        $result['products'] = $bcProducts->except('meta');
        $result['meta'] = $bcProducts->get('meta') ?? [];

        $category = $request->get('category');

        if ((int) $category){
            $result['category'] = $this->bcCategoryRepository->getCategory(
                (int) $category,
                ['include_fields' => 'parent_id,name,description,image_url']
            );
        }

        return $result;
    }

    public function mergeS3Images($bcProducts)
    {
        if ($bcProducts){
            $bcProductIds = $bcProducts->except('meta')->pluck('id');

            /** @var EloCollection $dbProducts */
            $dbProducts = Product::whereIn('bc_product_id', $bcProductIds)->get();
        }

        if ($dbProducts->isNotEmpty() && $bcProducts){
            $dbProducts->each(function ($item) use ($bcProducts){
                if ($item->images){
                    $bcProduct = $bcProducts->where('id', $item->bc_product_id)->first();
                    $bcProduct->images = $item->images;
                }
            });
        }

        return $bcProducts;
    }

    public function saveProduct(Request $request)
    {
        $attributes = $request->except('images');

        $bcProduct =
            [
                'name' => $request->get('name'),
                'price' => $request->get('price'),
                'brand_id' => $request->get('brand_id'),
                'description' => $request->get('description'),
                'type' => 'physical',
                'weight' => 4,
                //TODO: remove hardcoded '39' when catagories will be implemented
                'categories' =>  [$request->get('category_id') ?? 39]
            ];

        $bcProduct = BcProductRepository::saveProduct($bcProduct, ['include_fields' => $this->bcProductFields,]);

        $attributes['user_id'] = $request->user()->id;
        $attributes['bc_product_id'] = $bcProduct->id;

        /** @var Product $product */
        $product = Product::create($attributes);


        if ($request->get('images')){

            $images = $this->uploadProductImages($product->id, $request->get('images'));

            $product->images()->createMany($images);

            $this->saveBcProductImages($images, $bcProduct->id);

            $bcProduct->images = $product->images;
        }

        $bcProduct->custom_fields = $this->saveCustomFields($bcProduct->id, $request->get('custom_fields'));

        return $bcProduct;
    }

    public function saveBcProductImages(array $images, int $id)
    {
        foreach ($images as $image){
            BcProductRepository::saveImage($id, ['image_url' => $image['url_standard']]);
        }
    }

    public function saveCustomFields(int $id, array $customFields): array
    {
        $customFields = $this->parseCustomFields($customFields);

        foreach ($customFields as $customField){
            $result[] = BcProductRepository::saveCustomField($id, $customField);
        }
        return $customFields;
    }

    private function parseCustomFields(array $custom_fields)
    {
        return array_map(function ($item){
            return $item = json_decode($item, true);
        }, $custom_fields);
    }

    public function uploadProductImages(int $productId, $images)
    {
        $resizedImages = $this->resizeImages($images);

        $imagesUrls= [];

        /** @var UploadedFile $image */
        foreach ($images as $imageNumber => $image){

            $filename = Carbon::now()->timestamp . '.' . explode('/', explode(';base64', $image)[0])[1];

            $imagesUrls[$imageNumber] = $this->uploadAndGetUrl($productId, $filename, $resizedImages[$imageNumber], $imageNumber);
        }

        return $imagesUrls;
    }

    public function resizeImages($images): array
    {
        $resizedImages = [];

        foreach ($images as $key => $image){

            $extension = explode('/', explode(';base64', $image)[0])[1];

            $resizedImages[$key]['zoom'] = Image::make($image)->fit(1280, 1280)->encode($extension);

            $resizedImages[$key]['standard'] = Image::make($image)->fit(386, 513)->encode($extension);

            $resizedImages[$key]['thumbnail'] = Image::make($image)->fit(290, 290)->encode($extension);
        }

        return $resizedImages;
    }

    public function uploadAndGetUrl(int $productId, string $filename, array $resizedImages, int $imageNumber): array
    {
        $urls = [];

        foreach ($resizedImages as $size => $image){
            $path = $productId . '/' . $imageNumber . '/' . $size . $filename;
            Storage::disk('s3')->put($path, $image, 'public');
            $urls['url_' . $size] = Storage::disk('s3')->url($path);
        }

        return $urls;
    }

    public function getProduct(int $id): ?BcProduct
    {
        $params =
            [
                'include' => 'images,custom_fields',
                'include_fields' => $this->bcProductFields,
            ];
        /** @var BcProduct $bcProduct */
        $bcProduct = BcProductRepository::getProduct($id, $params);

        if ($bcProduct){
            $dbProduct = Product::where('bc_product_id', $bcProduct->id)->first();
            if ($dbProduct && $dbProduct->images){
                $bcProduct->images = $dbProduct->images;
            }
            $bcProduct
                ->setBrand($bcProduct->getBrandId());
        }

        return $bcProduct;
    }

    public function getProductSelectableAttributes(): array
    {
        return
            [
                'brands' => BcProductRepository::getBrands(['include_fields' => 'name'])
            ];
    }

    public function categoriesTree()
    {
        $categories = $this->bcCategoryRepository->getCategoriesTree();
        return $categories->where('is_visible', true);
    }
}
