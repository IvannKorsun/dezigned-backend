<?php

namespace App\Services\Product;

use App\Models\Product\Product;

class ProductSearchService
{
    public function search(string $text)
    {
        return Product::search($text)->get();
    }
}
