<?php

namespace App\Services\Product;

use App\Http\Requests\FilterRequest;
use App\Services\Bigcommerce\Facades\BcProductRepository;
use Illuminate\Http\Request;

class FilterService
{
    public function getFilters(Request $request): array
    {
        $id = $request->get('category');
        $filters = [];
        $filterValues = null;

        $filterNames = BcProductRepository::getCategories(['is_visible' => false, 'include_fields' => 'name', 'parent_id' => $id]);
        if ($filterNames){
            //TODO: fix limit 250 issue (max limit in bigcommerce api)
            $filterValues =  BcProductRepository::getCategories(['is_visible' => false, 'limit' => 250, 'include_fields' => 'name,parent_id', 'parent_id:in' => implode(',', $filterNames->pluck('id')->toArray())]);
        }

        if ($filterValues){
            $filters = $filterValues->groupBy('parent_id')->mapWithKeys(function ($values, $key) use ($filterNames) {
                $filterName = $filterNames->where('id', $key)->first();
                return [$filterName->name => $values];
            })->toArray();
        }


        $additionalFilters = [
            'manufacturer' => BcProductRepository::getBrands(['include_fields' => 'name']),
            'maxPrice' => BcProductRepository::getMaxPrice($id)
        ];

        return array_merge($additionalFilters, $filters);
    }

    public function filter(FilterRequest $request): ?array
    {
        $params = $request->all();

        $filters = [];

        foreach ($params as $key => $value){
            $this->applyFilters($value, $key, $filters);
        }

        if (isset($filters['additional'])){
            $filters['categories:in'] = $filters['additional'];
            unset($filters['additional']);
        }

        return $filters;
    }

    public function applyFilters($value, $key, array &$filters): void
    {
        if ($key === 'price'){
            $price = explode(',', $value);
            $filters['price:min'] = $price[0] ?? 0;
            $filters['price:max'] = $price[1] ?? 0;
        }elseif ($key === 'page'){
            $filters['page'] = $value;
        }elseif ($key === 'limit'){
            $filters['limit'] = $value;
        }elseif ($key === 'search'){
            $filters['keyword'] = $value;
        }elseif ($key === 'manufacturer'){
            $filters['brand_id:in'] = $value;
        }elseif ($key === 'category'){
            (isset($filters['categories:in']))
                ? $filters['categories:in'] = $filters['categories:in'] . ',' . $value
                : $filters['categories:in'] = $value;
        }
        elseif($key === 'id'){
            $filters['id:in'] = $value;
        }
        else{
            $parentCategoryId = BcProductRepository::getCategories(['is_visible' => false, 'name' => $key,'include_fields' => 'id']);
            if ($parentCategoryId){
                (isset($filters['additional']))
                    ? $filters['additional'] = $filters['additional'] . ',' . $parentCategoryId->first()->id . ',' . $value
                    : $filters['additional'] = $value;
            }
        }
    }
}
