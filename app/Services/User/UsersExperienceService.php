<?php


namespace App\Services\User;


use App\Http\Resources\User\UserExperienceCollection;
use App\Http\Resources\User\UserExperienceResource;
use App\Models\User;
use App\Services\S3ImagesService;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Resources\Json\ResourceCollection;
use Illuminate\Support\Facades\Auth;

class UsersExperienceService
{
    /**
     * @var S3ImagesService
     */
    private $imagesService;

    public function __construct(S3ImagesService $imagesService)
    {
        $this->imagesService = $imagesService;
        $this->imagesService->setStorageDisk('s3-users-experience-comp-img');
    }

    public function getExperienceList(Request $request): ?ResourceCollection
    {
        if ($request->route()->parameter('userId')){
            $user = User::role('installer')->where('id', $request->route()->parameter('userId'))->first();

            if ($user){
                return new UserExperienceCollection($user->experience()->get());
            }
            return null;
        }

        return new UserExperienceCollection($request->user()->experience()->get());
    }

    public function addExperience(Request $request): JsonResource
    {
        $experience = $request->user()->experience()->create($request->except('company_image'));

        if ($request->get('company_image')){
            $image = $this->imagesService->uploadImage(
                $request->user()->id . '/' . $experience->id,
                $request->get('company_image'),
                ['company_image' => ['width' => 75, 'height' => 75]]
            );
            $experience->update($image);
        }

        return new UserExperienceResource($experience);
    }

    public function updateExperience(Request $request, int $id): ?JsonResource
    {
        $experience = $request
            ->user()
            ->experience()
            ->where('id', $id)
            ->first();

        if (!$experience){
            return null;
        }

        $attributes = $request->except('company_image');

        if ($request->get('company_image')){
            $this->imagesService->delete([$experience->getOriginal('company_image')]);
            $attributes += $this->imagesService->uploadImage(
                $request->user()->id . '/' . $experience->id,
                $request->get('company_image'),
                ['company_image' => ['width' => 75, 'height' => 75]]
            );
        }

        $experience->update($attributes);

        return new UserExperienceResource($experience);
    }

    public function delete(int $id): bool
    {
        $experience = Auth::user()
            ->experience()
            ->where('id', $id)
            ->first();

        if (!$experience){
            return false;
        }

        $this->imagesService->delete([$experience->getOriginal('company_image')]);

        return $experience->delete();
    }
}
