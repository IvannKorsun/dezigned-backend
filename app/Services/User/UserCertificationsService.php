<?php


namespace App\Services\User;


use App\Http\Resources\GalleryImageCollection;
use App\Http\Resources\GalleryImageResource;
use App\Models\User\UserCertificate;
use App\Services\BaseGalleryService;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\ResourceCollection;

class UserCertificationsService extends BaseGalleryService
{

    protected $storageDisk = 's3-user-certificates';

    public function getCertificates(Request $request, ?int $page = null, ?int $limit = null): ResourceCollection
    {
        return new GalleryImageCollection($this->getAll($request, $request->user()->certificates(), $page, $limit));
    }

    public function addCertificates(Request $request)
    {
        $this->addImages($request->user()->id, $request, $request->user()->certificates());

        return $this->getCertificates($request, 1, 16);
    }

    public function deleteCertificate(int $user_id, int $id): bool
    {
        $image = UserCertificate::where('user_id', $user_id)
            ->where('id', $id)
            ->first();

        return $this->delete($image);
    }

    public function updateCertificate(int $id, Request $request)
    {
        $image = UserCertificate::where('user_id', $request->user()->id)
            ->where('id', $id)
            ->first();

        $this->update($image, $request, $request->user()->id);

        return new GalleryImageResource($image);
    }

    protected function imagesConfig(): array
    {
        return
            [
                'url_original' => [],
                'url_thumbnail' => ['width' => 290, 'height' => 290],
            ];
    }
}
