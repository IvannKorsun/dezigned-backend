<?php

namespace App\Services\User;

use App\Http\Resources\GalleryImageCollection;
use App\Http\Resources\GalleryImageResource;
use App\Models\User;
use App\Models\User\GalleryImage;
use App\Services\BaseGalleryService;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\ResourceCollection;

class UserGalleryService extends BaseGalleryService
{
    protected $storageDisk = 's3-user-gallery';

    public function getGalleryImages(Request $request, ?int $page = null, ?int $limit = null): ?ResourceCollection
    {
        if ($request->route()->parameter('userId')){
            $user = User::role('installer')->where('id', $request->route()->parameter('userId'))->first();

            if ($user){
                return new GalleryImageCollection($this->getAll($request, $user->galleryImages(), $page, $limit));
            }
            return null;
        }

        return new GalleryImageCollection($this->getAll($request, $request->user()->galleryImages(), $page, $limit));
    }

    public function addGalleryImages(Request $request)
    {
        $this->addImages($request->user()->id, $request, $request->user()->galleryImages());

        return $this->getGalleryImages($request, 1, 16);
    }

    public function deleteImages(int $user_id, int $id): bool
    {
        $image = GalleryImage::where('user_id', $user_id)
            ->where('id', $id)
            ->first();

        return $this->delete($image);
    }

    public function updateImage(int $id, Request $request)
    {
        $image = GalleryImage::where('user_id', $request->user()->id)
            ->where('id', $id)
            ->first();

        $this->update($image, $request, $request->user()->id);

        return new GalleryImageResource($image);
    }

    protected function imagesConfig(): array
    {
        return
            [
                'url_original' => [],
                'url_thumbnail' => ['width' => 290, 'height' => 290],
            ];
    }
}
