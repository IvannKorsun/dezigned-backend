<?php

namespace App\Providers;

use App\Services\Bigcommerce\Bigcommerce;
use App\Services\Bigcommerce\Repositories\ProductRepository;
use Illuminate\Support\ServiceProvider;

class BcProductRepositoryProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('bcProduct',function(){
            return new ProductRepository(new Bigcommerce());
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
