<?php

namespace App\Providers;

use App\Services\Bigcommerce\ApiAdapterInterface;
use App\Services\Bigcommerce\Bigcommerce;
use App\Services\SocialAccountsService;
use Coderello\SocialGrant\Resolvers\SocialUserResolverInterface;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    public $bindings = [
        SocialUserResolverInterface::class => SocialAccountsService::class,
        ApiAdapterInterface::class => Bigcommerce::class,
    ];
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        (request()->header('x-forwarded-proto') !== 'https') ?: \URL::forceScheme('https');
    }
}
