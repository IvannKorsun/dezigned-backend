<?php

/**
 * @\OpenApi\Annotations\Schema(
 *     description="Location Example",
 *     type="object",
 *     title="Example of location"
 * )
 */

class LocationModel
{

    /**
     * @\OpenApi\Annotations\Property(
     *     title="latitude",
     *     description="latitude value",
     *     format="float",
     *     example=90.0001
     * )
     * @var float
     */
    public $lat;

    /**
     * @\OpenApi\Annotations\Property(
     *     title="longitude",
     *     description="longitude value",
     *     format="float",
     *     example=90.0001
     * )
     * @var float
     */
    public $lng;
}
