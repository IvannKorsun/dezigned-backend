<?php

/**
 * @\OpenApi\Annotations\Schema(
 *     description="Product custom field create",
 *     type="object",
 *     title="Product custom field create",
 *     required={"name", "value"},
 *     example="name"
 * )
 */

class ProductCustomFieldsCreate
{
    /**
     * @\OpenApi\Annotations\Property(
     *      title="Custom field name",
     *      type="string",
     *      example="Custom field name"
     * )
     * @var string
     */
    public $name;

    /**
     * @\OpenApi\Annotations\Property(
     *      title="Custom field value",
     *      type="string",
     *      example="Custom field value"
     * )
     * @var string
     */
    public $value;
}
