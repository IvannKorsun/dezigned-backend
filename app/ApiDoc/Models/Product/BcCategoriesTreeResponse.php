<?php

/**
 * @\OpenApi\Annotations\Schema(
 *     description="Bigcommerce category tree response",
 *     type="object",
 *     title="Bigcommerce category tree"
 * )
 */

class BcCategoriesTreeResponse
{
    /**
     * @\OpenApi\Annotations\Property(
     *      title="Category id",
     *      type="integer",
     *      example="1"
     * )
     * @var integer
     */
    public $id;

    /**
     * @\OpenApi\Annotations\Property(
     *      title="Parent category id",
     *      type="integer",
     *      example="2"
     * )
     * @var integer
     */
    public $parent_id;

    /** @\OpenApi\Annotations\Property(
     *     title="Category name",
     *     description="Name of catetegory in bigcommerce",
     *     format="string",
     *     example="RUGS"
     * )
     * @var string
     */
    public $name;

    /** @\OpenApi\Annotations\Property(
     *     title="Status of category",
     *     description="Category status hidden or visible",
     *     format="boolean",
     *     example="true"
     * )
     * @var boolean
     */
    public $is_visible;

    /** @\OpenApi\Annotations\Property(
     *     title="Category url",
     *     description="Category url, used only for bigcommerce store navigation",
     *     format="string",
     *     example="/flooring/rugs/"
     * )
     * @var string
     */
    public $url;

    /**
     * @\OpenApi\Annotations\Property(
     *     title="children",
     *     type="array",
     *     @\OpenApi\Annotations\Items(ref="#/components/schemas/BcCategoriesTreeChildrenResponse")
     * )
     * @var array
     */
    public $children;
}
