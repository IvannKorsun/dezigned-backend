<?php

/**
 * @\OpenApi\Annotations\Schema(
 *     description="Product type",
 *     type="object",
 *     title="Product type"
 * )
 */

class ProductTypeResponse
{
    /**
     * @\OpenApi\Annotations\Property(
     *      title="Type id",
     *      type="integer",
     *      example="1"
     * )
     * @var integer
     */
    public $id;

    /**
     * @\OpenApi\Annotations\Property(
     *      title="Type name",
     *      type="string",
     *      example="Type name"
     * )
     * @var string
     */
    public $name;

}
