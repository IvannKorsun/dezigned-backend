<?php

/**
 * @\OpenApi\Annotations\Schema(
 *     description="Product custom field response",
 *     type="object",
 *     title="Product custom field"
 * )
 */

class ProductCustomFieldResponse
{
    /**
     * @\OpenApi\Annotations\Property(
     *      title="Custom field id",
     *      type="integer",
     *      example="1"
     * )
     * @var integer
     */
    public $id;

    /**
     * @\OpenApi\Annotations\Property(
     *      title="Custom field name",
     *      type="string",
     *      example="Custom field name"
     * )
     * @var string
     */
    public $name;

    /**
     * @\OpenApi\Annotations\Property(
     *      title="Custom field value",
     *      type="string",
     *      example="Custom field value"
     * )
     * @var string
     */
    public $value;

}
