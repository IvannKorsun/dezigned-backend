<?php

/**
 * @\OpenApi\Annotations\Schema(
 *     description="Product brand",
 *     type="object",
 *     title="Product brand"
 * )
 */

class ProductBrandResponse
{
    /**
     * @\OpenApi\Annotations\Property(
     *      title="Brand id",
     *      type="integer",
     *      example="1"
     * )
     * @var integer
     */
    public $id;

    /**
     * @\OpenApi\Annotations\Property(
     *      title="Brand name",
     *      type="string",
     *      example="Brand name"
     * )
     * @var string
     */
    public $name;
}
