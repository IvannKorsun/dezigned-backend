<?php

/**
 * @\OpenApi\Annotations\Schema(
 *     description="Bigcommerce meta example",
 *     type="object",
 *     title="Bigcommerce pagination"
 * )
 */
class BcMetaResponse
{
    /**
     * @\OpenApi\Annotations\Property(ref="#/components/schemas/BcPaginationResponse")
     * @var object
     */
    public $pagination;

}
