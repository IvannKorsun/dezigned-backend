<?php

/**
 * @\OpenApi\Annotations\Schema(
 *     description="Bigcommerce category response",
 *     type="object",
 *     title="Bigcommerce category"
 * )
 */
class BcCategoryResponse
{
    /**
     * @\OpenApi\Annotations\Property(
     *      title="Category id",
     *      type="integer",
     *      example="1"
     * )
     * @var integer
     */
    public $id;

    /**
     * @\OpenApi\Annotations\Property(
     *      title="Parent category id",
     *      type="integer",
     *      example="2"
     * )
     * @var integer
     */
    public $parent_id;

    /** @\OpenApi\Annotations\Property(
     *     title="Category name",
     *     description="Name of catetegory in bigcommerce",
     *     format="string",
     *     example="RUGS"
     * )
     * @var string
     */
    public $name;

    /** @\OpenApi\Annotations\Property(
     *     title="Category description",
     *     description="Description",
     *     format="string",
     *     example="Category description"
     * )
     * @var string
     */
    public $description;

    /** @\OpenApi\Annotations\Property(
     *     title="Category image",
     *     description="Category image url",
     *     format="string",
     *     example="https://cdn11.bigcommerce.com/s-ljddwp4h79/product_images/k/selection_099__11958.png"
     * )
     * @var string
     */
    public $image_url;
}
