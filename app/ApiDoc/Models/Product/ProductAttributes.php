<?php

/**
 * @\OpenApi\Annotations\Schema(
 *     description="Product attributes",
 *     type="object",
 *     title="Product attributes"
 * )
 */


class ProductAttributes
{

    /**
     * @\OpenApi\Annotations\Property(
     *     title="Available product styles list",
     *     type="array",
     *     @\OpenApi\Annotations\Items(ref="#/components/schemas/ProductStyleResponse")
     * )
     * @var array
     */
    public $styles;

    /**
     * @\OpenApi\Annotations\Property(
     *     title="Available product types list",
     *     type="array",
     *     @\OpenApi\Annotations\Items(ref="#/components/schemas/ProductTypeResponse")
     * )
     * @var array
     */
    public $types;

    /**
     * @\OpenApi\Annotations\Property(
     *     title="Available product brands list",
     *     type="array",
     *     @\OpenApi\Annotations\Items(ref="#/components/schemas/ProductBrandResponse")
     * )
     * @var array
     */
    public $brands;
}
