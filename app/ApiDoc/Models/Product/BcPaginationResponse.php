<?php

/**
 * @\OpenApi\Annotations\Schema(
 *     description="Bigcommerce pagination example",
 *     type="object",
 *     title="Bigcommerce pagination"
 * )
 */

class BcPaginationResponse
{
    /**
     * @\OpenApi\Annotations\Property(
     *     title="Count of all products",
     *     type="int",
     *     example=100
     * )
     * @var array
     */
    public $total;

    /**
     * @\OpenApi\Annotations\Property(
     *     title="Count of all products on page",
     *     type="int",
     *     example=10
     * )
     * @var array
     */
    public $count;

    /**
     * @\OpenApi\Annotations\Property(
     *     title="Count of all products per page",
     *     type="int",
     *     example=10
     * )
     * @var array
     */
    public $per_page;

    /**
     * @\OpenApi\Annotations\Property(
     *     title="Count of all pages",
     *     type="int",
     *     example=10
     * )
     * @var array
     */
    public $total_pages;

    /**
     * @\OpenApi\Annotations\Property(
     *     title="Count of all products per page",
     *     type="object",
     *     allOf={
     *          @\OpenApi\Annotations\Property(
     *              title="next link used only in bigcommrece API",
     *              property="next",
     *              type="string",
     *              example="?sort=name&include=variants&limit=10&page=2"
     *          ),
     *          @\OpenApi\Annotations\Property(
     *              title="current link used only in bigcommrece API",
     *              property="current",
     *              type="string",
     *              example="?sort=name&include=variants&limit=10&page=1"
     *          )
     *     }
     * )
     * @var array
     */
    public $links;

    /**
     * @\OpenApi\Annotations\Property(
     *     title="too many",
     *     type="bool",
     *     example=false
     * )
     * @var array
     */
    public $too_many;



}
