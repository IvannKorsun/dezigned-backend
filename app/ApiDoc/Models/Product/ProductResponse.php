<?php

/**
 * @\OpenApi\Annotations\Schema(
 *     description="Get list of products",
 *     type="object",
 *     title="List of products"
 * )
 */
class ProductResponse
{
    /**
     * @\OpenApi\Annotations\Property(
     *      title="Product id",
     *      type="integer",
     *      example="1"
     * )
     * @var integer
     */
    public $id;

    /** @\OpenApi\Annotations\Property(
     *     title="Product name",
     *     description="Name of product, must be unique",
     *     format="string",
     *     example="Product name"
     * )
     * @var string
     */
    public $name;

    /** @\OpenApi\Annotations\Property(
     *     title="Product description",
     *     description="Product description",
     *     format="string",
     *     example="Simple product description"
     * )
     * @var string
     */
    public $description;

    /** @\OpenApi\Annotations\Property(
     *     title="Product price",
     *     description="Price of product",
     *     format="integer",
     *     example="100.10"
     * )
     * @var integer
     */
    public $price;

    /**
     * @\OpenApi\Annotations\Property(ref="#/components/schemas/ProductBrandResponse")
     * @var object
     */
    public $brand;

    /**
     * @\OpenApi\Annotations\Property(
     *     title="images",
     *     type="array",
     *     @\OpenApi\Annotations\Items(ref="#/components/schemas/ProductImageResponse")
     * )
     * @var array
     */
    public $images;

    /**
     * @\OpenApi\Annotations\Property(
     *     title="images",
     *     type="array",
     *     @\OpenApi\Annotations\Items(ref="#/components/schemas/ProductCustomFieldResponse")
     * )
     * @var array
     */
    public $custom_fields;

    /**
     * @\OpenApi\Annotations\Property(
     *     title="categories",
     *     type="array",
     *     @\OpenApi\Annotations\Items(example="10, 20"))
     * )
     * @var array
     */
    public $categories;

}
