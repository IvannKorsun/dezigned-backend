<?php

/**
 * @\OpenApi\Annotations\Schema(
 *     description="Product filters",
 *     type="object",
 *     title="Product filters"
 * )
 */

class FiltersResponse
{
    /**
     * @\OpenApi\Annotations\Property(
     *     title="Available product styles list",
     *     type="array",
     *     @\OpenApi\Annotations\Items(ref="#/components/schemas/ProductStyleResponse")
     * )
     * @var array
     */
    public $styles;

    /**
     * @\OpenApi\Annotations\Property(
     *     title="Available product types list",
     *     type="array",
     *     @\OpenApi\Annotations\Items(ref="#/components/schemas/ProductTypeResponse")
     * )
     * @var array
     */
    public $types;

    /**
     * @\OpenApi\Annotations\Property(
     *     title="Available product manufacturers list",
     *     type="array",
     *     @\OpenApi\Annotations\Items(ref="#/components/schemas/ProductBrandResponse")
     * )
     * @var array
     */
    public $manufacturer;

    /**
     * @\OpenApi\Annotations\Property(
     *      title="Max product price",
     *      type="string",
     *      example="100.10"
     * )
     * @var string
     */
    public $maxPrice;

}
