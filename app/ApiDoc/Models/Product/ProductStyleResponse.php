<?php

/**
 * @\OpenApi\Annotations\Schema(
 *     description="Product style",
 *     type="object",
 *     title="Product style"
 * )
 */

class ProductStyleResponse
{
    /**
     * @\OpenApi\Annotations\Property(
     *      title="Style id",
     *      type="integer",
     *      example="1"
     * )
     * @var integer
     */
    public $id;

    /**
     * @\OpenApi\Annotations\Property(
     *      title="Style name",
     *      type="string",
     *      example="Style name"
     * )
     * @var string
     */
    public $name;

}
