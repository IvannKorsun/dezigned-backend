<?php

/**
 * @\OpenApi\Annotations\Schema(
 *     description="Create new product",
 *     type="object",
 *     title="Crete new product"
 * )
 */

class ProductCreateRequest
{
    /** @\OpenApi\Annotations\Property(
     *     title="Product name",
     *     description="Name of product, must be unique",
     *     format="string",
     *     example="Product name",
     *     required={"name", "description", "brand_id", "type_id", "style_id", "images"}
     * )
     * @var string
     */
    public $name;

    /** @\OpenApi\Annotations\Property(
     *     title="Product description",
     *     description="Product description",
     *     format="string",
     *     example="Simple product description"
     * )
     * @var string
     */
    public $description;

    /** @\OpenApi\Annotations\Property(
     *     title="Product price",
     *     description="Price of product",
     *     format="integer",
     *     example="100.10"
     * )
     * @var integer
     */
    public $price;

    /** @\OpenApi\Annotations\Property(
     *     title="Product brand",
     *     description="Selected existing brand id for the product",
     *     format="integer",
     *     example="1"
     * )
     * @var integer
     */
    public $brand_id;

    /** @\OpenApi\Annotations\Property(
     *     title="Product type",
     *     description="Selected existing type id for the product",
     *     format="integer",
     *     example="1"
     * )
     * @var integer
     */
    public $type_id;

    /** @\OpenApi\Annotations\Property(
     *     title="Product style",
     *     description="Selected existing style id for the product",
     *     format="integer",
     *     example="1"
     * )
     * @var integer
     */
    public $style_id;

    /**
     * @\OpenApi\Annotations\Property(
     *     type="array",
     *     @\OpenApi\Annotations\Items(
     *          type="string",
     *          format="binary",
     *          example="data:image/jpeg;base64,/9j/4AAQSkZJRgA..."
     *     )
     * )
     * @var array
     */
    public $images;

    //TODO: investigate and fix custom fields array items example
    /**
     * @\OpenApi\Annotations\Property(
     *     title="custom_fields",
     *     description="Product custom fields",
     *     type="array",
     *     @OpenApi\Annotations\Items(
     *          type="string",
     *          format="json",
     *          example="{""name"":""custom name"", ""value"":""custom value""}"
     *     )
     * )
     * @var array
     */
    public $custom_fields = [];

}
