<?php

/**
 * @\OpenApi\Annotations\Schema(
 *     description="Product Image response",
 *     type="object",
 *     title="Product Image"
 * )
 */

class ProductImageResponse
{

    /**
     * @\OpenApi\Annotations\Property(
     *      title="Image id",
     *      type="integer",
     *      example="1"
     * )
     * @var integer
     */
    public $id;

    /**
     * @\OpenApi\Annotations\Property(
     *      title="Image zoom url",
     *      type="integer",
     *      example="https://image_zoom_url"
     * )
     * @var string
     */
    public $url_zoom;

    /**
     * @\OpenApi\Annotations\Property(
     *      title="Image standard url",
     *      type="integer",
     *      example="https://image_standard_url"
     * )
     * @var string
     */
    public $url_standard;

    /**
     * @\OpenApi\Annotations\Property(
     *      title="Image thumbnail url",
     *      type="integer",
     *      example="https://image_thumbnail_url"
     * )
     * @var string
     */
    public $url_thumbnail;

}
