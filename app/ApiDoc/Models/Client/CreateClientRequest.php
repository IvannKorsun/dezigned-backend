<?php

/**
 * @\OpenApi\Annotations\Schema(
 *     description="Create client request",
 *     type="object",
 *     title="Create client request"
 * )
 */
class CreateClientRequest
{
    /**
     * @\OpenApi\Annotations\Property(
     *     title="Client name",
     *     description="Client name",
     *     format="string",
     *     example="name"
     * )
     * @var string
     */
    public $name;

    /**
     * @\OpenApi\Annotations\Property(
     *     title="Client company",
     *     description="Client company",
     *     format="string",
     *     example="Client company"
     * )
     * @var string
     */
    public $company;

    /**
     * @\OpenApi\Annotations\Property(
     *     title="email",
     *     description="Client email",
     *     format="string",
     *     example="mail@example.com"
     * )
     * @var string
     */
    public $email;

    /**
     * @\OpenApi\Annotations\Property(
     *     title="Phone number",
     *     description="Client phone number",
     *     format="string",
     *     example="+18887779999"
     * )
     * @var string
     */
    public $phone_number;

    /**
     * @\OpenApi\Annotations\Property(
     *     title="Client address",
     *     description="Client address",
     *     format="string",
     *     example="Client address"
     * )
     * @var string
     */
    public $address;

    /**
     * @\OpenApi\Annotations\Property(
     *     title="Client city",
     *     description="Client city",
     *     format="string",
     *     example="Client city"
     * )
     * @var string
     */
    public $city;

    /**
     * @\OpenApi\Annotations\Property(
     *     title="Client zip code",
     *     description="Client zip code",
     *     format="string",
     *     example="Client zip code"
     * )
     * @var string
     */
    public $zip;

    /**
     * @\OpenApi\Annotations\Property(
     *     title="Notes",
     *     description="Notes",
     *     format="string",
     *     example="Some notes"
     * )
     * @var string
     */
    public $notes;

    /**
     * @\OpenApi\Annotations\Property(ref="#/components/schemas/LocationModel")
     * @var array
     */
    public $location;
}
