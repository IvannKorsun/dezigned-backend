<?php

/**
 * @\OpenApi\Annotations\Schema(
 *     description="Add gallery images",
 *     type="object",
 *     title="Add gallery images example"
 * )
 */
class AddGalleryImage
{
    /**
     * @\OpenApi\Annotations\Property(
     *      type="string",
     *      format="string",
     *      example="Image name"
     * )
     * @var array
     */
    public $name;

    /**
     * @\OpenApi\Annotations\Property(
     *      type="string",
     *      format="binary",
     *      example="data:image/jpeg;base64,/9j/4AAQSkZJRgA..."
     * )
     * @var array
     */
    public $image;
}
