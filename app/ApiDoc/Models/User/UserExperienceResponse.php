<?php

/**
 * @\OpenApi\Annotations\Schema(
 *     description="User experience response",
 *     type="object",
 *     title="Example of user experience response"
 * )
 */

class UserExperienceResponse
{

    /**
     * @\OpenApi\Annotations\Property(
     *      title="Experience id",
     *      type="integer",
     *      example="1"
     * )
     * @var integer
     */
    public $id;

    /** @\OpenApi\Annotations\Property(
     *     title="Position",
     *     description="User position in the company",
     *     format="string",
     *     example="Position name"
     * )
     * @var string
     */
    public $position;

    /** @\OpenApi\Annotations\Property(
     *     title="Compamy",
     *     description="Company name",
     *     format="string",
     *     example="Company name"
     * )
     * @var string
     */
    public $company;

    /**
     * @\OpenApi\Annotations\Property(
     *     title="Currently works",
     *     type="bool",
     *     example=false
     * )
     * @var boolean
     */
    public $currently_works;

    /**
     * @\OpenApi\Annotations\Property(
     *     title="Company image",
     *     description="Url to company image",
     *     format="base64",
     *     type="base64",
     *     example="base64 encoded image"
     * )
     * @var string
     */
    public $company_image;

    /** @\OpenApi\Annotations\Property(
     *     title="Date",
     *     description="User date of work in the company (from to)",
     *     format="string",
     *     example="Oct. 2019 - Dec. 2019"
     * )
     * @var string
     */
    public $date;

    /**
     * @\OpenApi\Annotations\Property(
     *     title="Date from",
     *     description="Date when user started working in the company",
     *     format="string",
     *     type="string",
     *     example="2019-11"
     * )
     * @var string
     */
    public $date_from;

    /**
     * @\OpenApi\Annotations\Property(
     *     title="Date from",
     *     description="Date when user finished working in the company",
     *     format="string",
     *     type="string",
     *     example="2019-12"
     * )
     * @var string
     */
    public $date_to;
}
