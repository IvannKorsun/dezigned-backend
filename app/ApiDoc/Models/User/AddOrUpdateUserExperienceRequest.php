<?php

/**
 * @\OpenApi\Annotations\Schema(
 *     description="User experience create or update request",
 *     type="object",
 *     title="Example of create or update user experience request"
 * )
 */
class AddOrUpdateUserExperienceRequest
{
    /** @\OpenApi\Annotations\Property(
     *     title="Position",
     *     description="User position in the company",
     *     format="string",
     *     example="Position name"
     * )
     * @var string
     */
    public $position;

    /** @\OpenApi\Annotations\Property(
     *     title="Compamy",
     *     description="Company name",
     *     format="string",
     *     type="string",
     *     example="Company name"
     * )
     * @var string
     */
    public $company;

    /**
     * @\OpenApi\Annotations\Property(
     *     title="Currently works",
     *     type="bool",
     *     example=false
     * )
     * @var boolean
     */
    public $currently_works;

    /**
     * @\OpenApi\Annotations\Property(
     *     title="Company image",
     *     description="Url to company image",
     *     format="base64",
     *     type="base64",
     *     example="base64 encoded image"
     * )
     * @var string
     */
    public $company_image;

    /**
     * @\OpenApi\Annotations\Property(
     *     title="Date from",
     *     description="Date when user started working in the company",
     *     format="string",
     *     type="string",
     *     example="2019-11"
     * )
     * @var string
     */
    public $date_from;

    /**
     * @\OpenApi\Annotations\Property(
     *     title="Date from",
     *     description="Date when user finished working in the company",
     *     format="string",
     *     type="string",
     *     example="2019-12"
     * )
     * @var string
     */
    public $date_to;
}
