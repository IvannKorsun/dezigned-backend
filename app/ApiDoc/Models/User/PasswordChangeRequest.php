<?php

/**
 * @\OpenApi\Annotations\Schema(
 *     description="User change password request.",
 *     type="object",
 *     title="User change password request"
 * )
 */

class PasswordChangeRequest
{

    /**
     * @\OpenApi\Annotations\Property(
     *     title="current_password",
     *     description="User password",
     *     format="string",
     *     example="currentPassword"
     * )
     * @var string
     */
    public $current_password;

    /**
     * @\OpenApi\Annotations\Property(
     *     title="new_password",
     *     description="New password",
     *     format="string",
     *     example="NewPassword"
     * )
     * @var string
     */
    public $new_password;

    /**
     * @\OpenApi\Annotations\Property(
     *     title="c_new_password",
     *     description="User confirm new password",
     *     format="string",
     *     example="SameNewPassword"
     * )
     * @var string
     */
    public $c_new_password;

}
