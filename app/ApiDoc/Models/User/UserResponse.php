<?php

/**
 * @\OpenApi\Annotations\Schema(
 *     description="User response",
 *     type="object",
 *     title="Example of user response"
 * )
 */

class UserResponse
{

    /**
     * @\OpenApi\Annotations\Property(
     *     title="id",
     *     description="User id",
     *     format="integer",
     *     example="1"
     * )
     * @var integer
     */
    public $id;

    /**
     * @\OpenApi\Annotations\Property(
     *     title="name",
     *     description="User name",
     *     format="string",
     *     example="name"
     * )
     * @var string
     */
    public $name;

    /**
     * @\OpenApi\Annotations\Property(
     *     title="first name",
     *     description="User first name",
     *     format="string",
     *     example="Name"
     * )
     * @var string
     */
    public $first_name;


    /**
     * @\OpenApi\Annotations\Property(
     *     title="last name",
     *     description="User last name",
     *     format="string",
     *     example="Name"
     * )
     * @var string
     */
    public $last_name;

    /**
     * @\OpenApi\Annotations\Property(
     *     title="owner name",
     *     description="User owner name",
     *     format="string",
     *     example="+18887779999"
     * )
     * @var string
     */
    public $phone_number;

    /**
     * @\OpenApi\Annotations\Property(
     *     title="email",
     *     description="User email",
     *     format="string",
     *     example="mail@example.com"
     * )
     * @var string
     */
    public $email;

    /**
     * @\OpenApi\Annotations\Property(
     *     title="active",
     *     description="User email activate",
     *     format="integer",
     *     example=1
     * )
     * @var integer
     */
    public $active;


    /**
     * @\OpenApi\Annotations\Property(
     *     title="facebook url",
     *     description="Url to user facebook account",
     *     format="string",
     *     example="https://facebook_url"
     * )
     * @var string
     */
    public $facebook_url;

    /**
     * @\OpenApi\Annotations\Property(
     *     title="instagram url",
     *     description="Url to user instagram account",
     *     format="string",
     *     example="https://instagram_url"
     * )
     * @var string
     */
    public $instagram_url;

    /**
     * @\OpenApi\Annotations\Property(
     *     title="instagram url",
     *     description="Url to user twitter account",
     *     format="string",
     *     example="https://twitter_url"
     * )
     * @var string
     */
    public $twitter_url;

    /**
     * @\OpenApi\Annotations\Property(
     *     title="description",
     *     description="Url account description",
     *     format="string",
     *     example="Short description"
     * )
     * @var string
     */
    public $description;

    /**
     * @\OpenApi\Annotations\Property(
     *     title="avatar url",
     *     description="Url to user avatar image",
     *     format="string",
     *     example="https://avatar_image_url"
     * )
     * @var string
     */
    public $avatar_url;

    /**
     * @\OpenApi\Annotations\Property(
     *     title="User experience",
     *     description="Years of useer experience",
     *     format="string",
     *     example="10 years"
     * )
     * @var string
     */
    public $experience;

    /**
     * @\OpenApi\Annotations\Property(
     *     title="education",
     *     description="User education",
     *     format="string",
     *     example="XYZ University Bachelor in Interior Design"
     * )
     * @var string
     */
    public $education;

    /**
     * @\OpenApi\Annotations\Property(ref="#/components/schemas/LocationModel")
     * @var array
     */
    public $location;

    /**
     * @\OpenApi\Annotations\Property(
     *     title="User address",
     *     description="User address",
     *     format="string",
     *     example="User address"
     * )
     * @var string
     */
    public $address;

    /**
     * @\OpenApi\Annotations\Property(
     *     title="User city",
     *     description="User city",
     *     format="string",
     *     example="User city"
     * )
     * @var string
     */
    public $city;

    /**
     * @\OpenApi\Annotations\Property(
     *     title="User zip code",
     *     description="User zip code",
     *     format="string",
     *     example="User zip code"
     * )
     * @var string
     */
    public $zip;

    /**
     * @\OpenApi\Annotations\Property(
     *     title="roles",
     *     type="array",
     *     @\OpenApi\Annotations\Items(
     *          @\OpenApi\Annotations\Property(
     *              title="name",
     *              property="name",
     *              type="string",
     *              format="string",
     *              example="member"
     *          )
     *     )
     * )
     * @var array
     */
    public $roles;
}


