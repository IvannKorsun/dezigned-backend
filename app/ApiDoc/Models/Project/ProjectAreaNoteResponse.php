<?php

/**
 * @\OpenApi\Annotations\Schema(
 *     description="Project area note repsponse",
 *     type="object",
 *     title="Project area note response example"
 * )
 */

class ProjectAreaNoteResponse
{
    /**
     * @\OpenApi\Annotations\Property(
     *     title="id",
     *     description="Note id",
     *     format="integer",
     *     example="1"
     * )
     * @var integer
     */
    public $id;

    /**
     * @\OpenApi\Annotations\Property(
     *      type="string",
     *      format="string",
     *      example="Project area note"
     * )
     * @var array
     */
    public $note;

    /**
     * @\OpenApi\Annotations\Property(
     *      type="string",
     *      format="string",
     *      example="10/10/2019"
     * )
     * @var array
     */
    public $created_at;
}
