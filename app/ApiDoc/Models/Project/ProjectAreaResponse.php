<?php

/**
 * @\OpenApi\Annotations\Schema(
 *     description="Project area repsponse",
 *     type="object",
 *     title="Project area response example"
 * )
 */
class ProjectAreaResponse
{
    /**
     * @\OpenApi\Annotations\Property(
     *      title="Project area id",
     *      type="integer",
     *      example="1"
     * )
     * @var integer
     */
    public $id;

    /** @\OpenApi\Annotations\Property(
     *     title="Product area name",
     *     description="Name of project area, must be unique for project",
     *     format="string",
     *     example="Project area name"
     * )
     * @var string
     */
    public $name;

    /**
     * @\OpenApi\Annotations\Property(
     *     title="description",
     *     description="Project area description",
     *     format="string",
     *     example="Short project area description"
     * )
     * @var string
     */
    public $description;

    /**
     * @\OpenApi\Annotations\Property(
     *     title="original project area image",
     *     description="Project area original image",
     *     format="string",
     *     example="https://cover_image_original_url"
     * )
     * @var string
     */
    public $cover_image_original;

    /**
     * @\OpenApi\Annotations\Property(
     *     title="thumbnail project area image",
     *     description="Project area thumbnail image",
     *     format="string",
     *     example="https://cover_image_thumbnail_url"
     * )
     * @var string
     */
    public $cover_image_thumbnail;

    /**
     * @\OpenApi\Annotations\Property(
     *      title="Project id",
     *      type="integer",
     *      example="1",
     *      description="ID of the project associated with the area"
     * )
     * @var integer
     */
    public $project_id;

}
