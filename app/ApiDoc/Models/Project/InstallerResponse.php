<?php

/**
 * @\OpenApi\Annotations\Schema(
 *     description="Example of installer response",
 *     type="object",
 *     title="Example of installer response"
 * )
 */
class InstallerResponse
{
    /**
     * @\OpenApi\Annotations\Property(
     *      title="Installer id",
     *      type="integer",
     *      example="1"
     * )
     * @var integer
     */
    public $id;

    /**
     * @\OpenApi\Annotations\Property(
     *        title="installer name",
     *        type="string",
     *        example="Installer Name"
     *    ),
     */
    public $name;


    /**
     * @\OpenApi\Annotations\Property(
     *        title="installer rewiews",
     *        type="object",
     *        allOf={
     *          @\OpenApi\Annotations\Schema(
     *              @\OpenApi\Annotations\Property(
     *                  property="rating",
     *                  type="integer",
     *                  example=4.5
     *              )
     *          ),
     *          @\OpenApi\Annotations\Schema(
     *              @\OpenApi\Annotations\Property(
     *                  property="reviews_count",
     *                  type="integer",
     *                  example=10
     *              )
     *          )
     *       }
     *    )
     */
    public $reviews;

}
