<?php

/**
 * @\OpenApi\Annotations\Schema(
 *     description="Project Image response",
 *     type="object",
 *     title="Project Image"
 * )
 */

class ProjectImageResponse
{
    /**
     * @\OpenApi\Annotations\Property(
     *      title="Image id",
     *      type="integer",
     *      example="1"
     * )
     * @var integer
     */
    public $id;

    /**
     * @\OpenApi\Annotations\Property(
     *      title="original image url",
     *      type="integer",
     *      example="https://url_original_url"
     * )
     * @var string
     */
    public $url_original;

    /**
     * @\OpenApi\Annotations\Property(
     *      title="Image thumbnail url",
     *      type="integer",
     *      example="https://image_thumbnail_url"
     * )
     * @var string
     */
    public $url_thumbnail;

    /**
     * @\OpenApi\Annotations\Property(
     *      title="Project id",
     *      type="integer",
     *      example="1"
     * )
     * @var integer
     */
    public $project_id;
}
