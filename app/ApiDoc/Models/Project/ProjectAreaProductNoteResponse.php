<?php

/**
 * @\OpenApi\Annotations\Schema(
 *     description="Project area product note repsponse",
 *     type="object",
 *     title="Project area product note response example"
 * )
 */

class ProjectAreaProductNoteResponse
{
    /**
     * @\OpenApi\Annotations\Property(
     *     title="id",
     *     description="Note id",
     *     format="integer",
     *     example="1"
     * )
     * @var integer
     */
    public $id;

    /**
     * @\OpenApi\Annotations\Property(
     *      type="string",
     *      format="string",
     *      example="Project area product note"
     * )
     * @var array
     */
    public $note;

    /**
     * @\OpenApi\Annotations\Property(
     *      type="string",
     *      format="string",
     *      example="10/10/2019 @ 3:15pm"
     * )
     * @var array
     */
    public $created_at;
}
