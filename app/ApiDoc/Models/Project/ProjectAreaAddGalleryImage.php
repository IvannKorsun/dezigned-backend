<?php

/**
 * @\OpenApi\Annotations\Schema(
 *     description="Project area add gallery images",
 *     type="object",
 *     title="Project area add gallery images example"
 * )
 */
class ProjectAreaAddGalleryImage
{
    /**
     * @\OpenApi\Annotations\Property(
     *      type="string",
     *      format="string",
     *      example="Image name"
     * )
     * @var array
     */
    public $name;

    /**
     * @\OpenApi\Annotations\Property(
     *      type="string",
     *      format="binary",
     *      example="data:image/jpeg;base64,/9j/4AAQSkZJRgA..."
     * )
     * @var array
     */
    public $image;
}
