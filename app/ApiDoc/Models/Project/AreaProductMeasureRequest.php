<?php

/**
 * @\OpenApi\Annotations\Schema(
 *     description="Project area product measure",
 *     type="object",
 *     required={"project_id", "area_id", "product_id"},
 *     title="Project area product measure request example"
 * )
 */
class AreaProductMeasureRequest
{
    /**
     * @\OpenApi\Annotations\Property(
     *      title="project id",
     *      type="integer",
     *      example="1"
     * )
     * @var integer
     */
    public $project_id;

    /**
     * @\OpenApi\Annotations\Property(
     *      title="area id",
     *      type="integer",
     *      example="1"
     * )
     * @var integer
     */
    public $area_id;

    /**
     * @\OpenApi\Annotations\Property(
     *      title="product id",
     *      type="integer",
     *      example="1"
     * )
     * @var integer
     */
    public $product_id;

}
