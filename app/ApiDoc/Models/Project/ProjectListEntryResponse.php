<?php

/**
 * @\OpenApi\Annotations\Schema(
 *     description="Project repsponse",
 *     type="object",
 *     title="Project response example"
 * )
 */

class ProjectListEntryResponse
{
    /**
     * @\OpenApi\Annotations\Property(
     *      title="Image id",
     *      type="integer",
     *      example="1"
     * )
     * @var integer
     */
    public $id;

    /** @\OpenApi\Annotations\Property(
     *     title="Product name",
     *     description="Name of project, must be unique for user",
     *     format="string",
     *     example="Project name"
     * )
     * @var string
     */
    public $name;

    /**
     * @\OpenApi\Annotations\Property(
     *     title="active",
     *     description="Project status (active/archieved)",
     *     format="integer",
     *     example=1
     * )
     * @var integer
     */
    public $active;

    /**
     * @\OpenApi\Annotations\Property(ref="#/components/schemas/ClientResponse")
     * @var object
     */
    public $client;

    /**
     * @\OpenApi\Annotations\Property(
     *     title="description",
     *     description="Project description",
     *     format="string",
     *     example="Short description"
     * )
     * @var string
     */
    public $description;

    /**
     * @\OpenApi\Annotations\Property(
     *     title="original project cover image",
     *     description="Project original cover image",
     *     format="string",
     *     example="https://cover_image_original_url"
     * )
     * @var string
     */
    public $cover_image_original;

    /**
     * @\OpenApi\Annotations\Property(
     *     title="thumbnail project cover image",
     *     description="Project thumbnail cover image",
     *     format="string",
     *     example="https://cover_image_thumbnail_url"
     * )
     * @var string
     */
    public $cover_image_thumbnail;

}
