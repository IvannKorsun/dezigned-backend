<?php

/**
 * @\OpenApi\Annotations\Schema(
 *     description="Add product to project area request",
 *     type="object",
 *     title="Add product to project area request example"
 * )
 */

class AddProductRequest
{

    /**
     * @\OpenApi\Annotations\Property(
     *     type="array",
     *     @\OpenApi\Annotations\Items(
     *          type="integer",
     *          format="integer"
     *     ),
     *     example={1,2}
     * )
     * @var array
     */
    public $area_ids;
    /** @\OpenApi\Annotations\Property(
     *     title="Product quantity",
     *     description="Number of products added to project area",
     *     format="integer",
     *     example="100"
     * )
     * @var integer
     */
    public $qty;

    /** @\OpenApi\Annotations\Property(
     *     title="BigCommerce product id",
     *     format="integer",
     *     example="300"
     * )
     * @var integer
     */
    public $bc_product_id;
}
