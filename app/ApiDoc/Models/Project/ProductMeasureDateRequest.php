<?php

/**
 * @\OpenApi\Annotations\Schema(
 *     description="Example of add measure date request",
 *     type="object",
 *     title="measure date request"
 * )
 */
class ProductMeasureDateRequest
{
    /**
     * @\OpenApi\Annotations\Property(
     *     title="Date from",
     *     description="Date when user started working in the company",
     *     format="string",
     *     type="string",
     *     example="2019-11-11 10:00pm"
     * )
     * @var string
     */
    public $date_from;

    /**
     * @\OpenApi\Annotations\Property(
     *     title="Date from",
     *     description="Date when user finished working in the company",
     *     format="string",
     *     type="string",
     *     example="2019-11-11 10:00am"
     * )
     * @var string
     */
    public $date_to;
}
