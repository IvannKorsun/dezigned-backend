<?php

/**
 * @\OpenApi\Annotations\Schema(
 *     description="Project area gallery images list",
 *     type="object",
 *     title="Project area gallery images list example"
 * )
 */
class ProjectAreaGalleryImagesList
{
    /**
     * @\OpenApi\Annotations\Property(
     *     title="id",
     *     description="Image id",
     *     format="integer",
     *     example="1"
     * )
     * @var integer
     */
    public $id;

    /**
     * @\OpenApi\Annotations\Property(
     *      type="string",
     *      format="string",
     *      example="Image name"
     * )
     * @var array
     */
    public $name;

    /**
     * @\OpenApi\Annotations\Property(
     *     title="url_thumbnail",
     *     description="Thumbnail image url",
     *     format="string",
     *     example="https://url_thumbnail"
     * )
     * @var string
     */
    public $url_thumbnail;

    /**
     * @\OpenApi\Annotations\Property(
     *     title="url_original",
     *     description="Original image url",
     *     format="string",
     *     example="https://url_original"
     * )
     * @var string
     */
    public $url_original;

    /**
     * @\OpenApi\Annotations\Property(
     *      type="string",
     *      format="string",
     *      example="10/10/2019"
     * )
     * @var array
     */
    public $created_at;
}
