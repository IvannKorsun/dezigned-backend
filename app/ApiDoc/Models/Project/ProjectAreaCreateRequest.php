<?php

/**
 * @\OpenApi\Annotations\Schema(
 *     description="Project area create request",
 *     type="object",
 *     title="Project area create request example"
 * )
 */
class ProjectAreaCreateRequest
{
    /** @\OpenApi\Annotations\Property(
     *     title="Product area name",
     *     description="Name of project area, must be unique for project",
     *     format="string",
     *     example="Project area name"
     * )
     * @var string
     */
    public $name;

    /**
     * @\OpenApi\Annotations\Property(
     *     title="description",
     *     description="Project area description",
     *     format="string",
     *     example="Short project area description"
     * )
     * @var string
     */
    public $description;
    /**
     * @\OpenApi\Annotations\Property(
     *     title="cover image",
     *     description="Project area cover image",
     *     format="base64",
     *     type="base64",
     *     example="base64 encoded image"
     * )
     * @var string
     */
    public $cover_image;
}
