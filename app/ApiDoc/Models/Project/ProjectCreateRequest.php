<?php

/**
 * @\OpenApi\Annotations\Schema(
 *     description="Create new project",
 *     type="object",
 *     title="Crete new project"
 * )
 */
class ProjectCreateRequest
{
    /** @\OpenApi\Annotations\Property(
     *     title="Product name",
     *     description="Name of project, must be unique for user",
     *     format="string",
     *     example="Project name"
     * )
     * @var string
     */

    public $name;

    /**
     * @\OpenApi\Annotations\Property(
     *     title="description",
     *     description="Project description",
     *     format="string",
     *     example="Short description"
     * )
     * @var string
     */
    public $description;

    /**
     * @\OpenApi\Annotations\Property(
     *     title="Client ID",
     *     description="Client ID existing in a uaer clients list",
     *     format="integer",
     *     example=1
     * )
     * @var integer
     */
    public $client_id;

    /**
     * @\OpenApi\Annotations\Property(
     *     title="cover image",
     *     description="Project cover image",
     *     format="base64",
     *     type="base64",
     *     example="base64 encoded image"
     * )
     * @var string
     */
    public $cover_image;
}
