<?php

/**
 * @\OpenApi\Annotations\Schema(
 *     description="Project measure details",
 *     type="object",
 *     title="Project measure details response example"
 * )
 */
class ProjectMeasureDetails
{
    /**
     * @\OpenApi\Annotations\Property(
     *      title="Measure id",
     *      type="integer",
     *      example="1"
     * )
     * @var integer
     */
    public $id;

    /**
     * @\OpenApi\Annotations\Property(
     *     title="installer",
     *     type="object",
     *     @\OpenApi\Annotations\Property(
     *        type="integer",
     *        property="id",
     *        example=1
     *    ),
     *     @\OpenApi\Annotations\Property(
     *        type="string",
     *        property="name",
     *        example="Installer Name"
     *    ),
     *     @\OpenApi\Annotations\Property(
     *        type="object",
     *        property="reviews",
     *        allOf={
     *          @\OpenApi\Annotations\Schema(
     *              @\OpenApi\Annotations\Property(
     *                  property="rating",
     *                  type="integer",
     *                  example=4.5
     *              )
     *          ),
     *          @\OpenApi\Annotations\Schema(
     *              @\OpenApi\Annotations\Property(
     *                  property="reviews_count",
     *                  type="integer",
     *                  example=10
     *              )
     *          )
     *       }
     *    )
     * )
     * @var object
     */
    public $installer;

    /**
     * @\OpenApi\Annotations\Property(
     *     title="description",
     *     description="Measure details",
     *     format="string",
     *     example="Short details"
     * )
     * @var string
     */
    public $details;

    /**
     * @\OpenApi\Annotations\Property(
     *     title="description",
     *     description="Measure product details",
     *     format="string",
     *     example="Short product details"
     * )
     * @var string
     */
    public $product_details;

    /**
     * @\OpenApi\Annotations\Property(
     *     title="fee",
     *     description="Measure fee",
     *     format="integer",
     *     example=250
     * )
     * @var integer
     */
    public $fee;

    /**
     * @\OpenApi\Annotations\Property(
     *     title="step",
     *     description="Measure step",
     *     format="integer",
     *     example=2
     * )
     * @var integer
     */
    public $step;


    /**
     * @\OpenApi\Annotations\Property(
     *     title="client",
     *     description="Client details",
     *     type="object",
     *     allOf={
     *         @\OpenApi\Annotations\Schema(
     *             @\OpenApi\Annotations\Property(
     *                  title="address",
     *                  property="address",
     *                  format="string",
     *                  example="Address, City, 62000"
     *             )
     *         ),
     *         @\OpenApi\Annotations\Schema(ref="#/components/schemas/LocationModel")
     *     }
     * )
     * @var object
     */
    public $client;
}
