<?php

/**
 * @\OpenApi\Annotations\Schema(
 *     description="Example of measure date response",
 *     type="object",
 *     title="Example of installer response"
 * )
 */
class ProductMeasureDatesResponse
{
    /**
     * @\OpenApi\Annotations\Property(
     *      title="Date id",
     *      type="integer",
     *      example="1"
     * )
     * @var integer
     */
    public $id;

    /**
     * @\OpenApi\Annotations\Property(
     *     title="Date from",
     *     description="Date when user started working in the company",
     *     format="string",
     *     type="string",
     *     example="2019-11-11 10:00pm"
     * )
     * @var string
     */
    public $date_from;

    /**
     * @\OpenApi\Annotations\Property(
     *     title="Date from",
     *     description="Date when user finished working in the company",
     *     format="string",
     *     type="string",
     *     example="2019-11-11 10:00am"
     * )
     * @var string
     */
    public $date_to;


    /**
     * @\OpenApi\Annotations\Property(
     *     title="status",
     *     description="Dates status",
     *     format="string",
     *     example="'received', 'approved', 'rejected', 'suggested', 'saved', 'canceled'"
     * )
     */
    public $status;
}
