<?php

/**
 * @\OpenApi\Annotations\Schema(
 *     description="Update product qty in project area request",
 *     type="object",
 *     title="Add product to project area request example"
 * )
 */

class UpdateProductRequest
{
    /** @\OpenApi\Annotations\Property(
     *     title="Product quantity",
     *     description="Number of products added to project area",
     *     format="integer",
     *     example="100"
     * )
     * @var integer
     */
    public $qty;

    /** @\OpenApi\Annotations\Property(
     *     title="BigCommerce product id",
     *     format="integer",
     *     example="300"
     * )
     * @var integer
     */
    public $bc_product;
}
