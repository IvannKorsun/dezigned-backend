<?php

/**
 * @\OpenApi\Annotations\Schema(
 *     description="Token response",
 *     type="object",
 *     title="Authorization token response"
 * )
 */

class TokenResponse
{
    /**
     * @\OpenApi\Annotations\Property(
     *     title="token type",
     *     description="Type of authorization token",
     *     format="string",
     *     example="Bearer"
     * )
     * @var string
     */
    public $token_type;

    /**
     * @\OpenApi\Annotations\Property(
     *     title="expires in",
     *     description="Type expires time in seconds",
     *     format="integer",
     *     example="3600"
     * )
     * @var integer
     */
    public $expires_in;

    /**
     * @\OpenApi\Annotations\Property(
     *     title="access token",
     *     description="Access token",
     *     format="string",
     *     example="eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6ImQzYTkxMmU4ZDBiZmI0YWE4NjMxZDQ0NjMyOWQ4MDdlYWYwMTJmM2FjOGFiZTViNTlkMTBiNzJlNzQ4ZWYxNjkyNzBhZWRjYjIwOGVmZDE0In0.eyJhdWQiOiIyIiwianRpIjoiZDNhOTEyZThkMGJmYjRhYTg2MzFkNDQ2MzI5ZDgwN2VhZjAxMmYzYWM4YWJlNWI1OWQxMGI3MmU3NDhlZjE2OTI3MGFlZGNiMjA4ZWZkMTQiLCJpYXQiOjE1Njk4MzI5NTgsIm5iZiI6MTU2OTgzMjk1OCwiZXhwIjoxNTY5OTE5MzU3LCJzdWIiOiIxMiIsInNjb3BlcyI6W119"
     * )
     * @var string
     */
    public $access_token;

    /**
     * @\OpenApi\Annotations\Property(
     *     title="access token",
     *     description="Access token",
     *     format="string",
     *     example="eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6ImQzYTkxMmU4ZDBiZmI0YWE4NjMxZDQ0NjMyOWQ4MDdlYWYwMTJmM2FjOGFiZTViNTlkMTBiNzJlNzQ4ZWYxNjkyNzBhZWRjYjIwOGVmZDE0In0.eyJhdWQiOiIyIiwianRpIjoiZDNhOTEyZThkMGJmYjRhYTg2MzFkNDQ2MzI5ZDgwN2VhZjAxMmYzYWM4YWJlNWI1OWQxMGI3MmU3NDhlZjE2OTI3MGFlZGNiMjA4ZWZkMTQiLCJpYXQiOjE1Njk4MzI5NTgsIm5iZiI6MTU2OTgzMjk1OCwiZXhwIjoxNTY5OTE5MzU3LCJzdWIiOiIxMiIsInNjb3BlcyI6W119"
     * )
     * @var string
     */
    public $refresh_token;
}

