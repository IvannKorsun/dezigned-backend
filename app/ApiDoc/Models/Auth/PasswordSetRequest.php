<?php

/**
 * @\OpenApi\Annotations\Schema(
 *     description="User set password request.",
 *     type="object",
 *     title="User set password request"
 * )
 */

class PasswordSetRequest
{
    /**
     * @\OpenApi\Annotations\Property(
     *     title="token",
     *     description="Token for successfull set password",
     *     format="string",
     *     example="eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsIm..."
     * )
     * @var string
     */
    public $token;

    /**
     * @\OpenApi\Annotations\Property(
     *     title="password",
     *     description="User password",
     *     format="string",
     *     example="somepassword"
     * )
     * @var string
     */
    public $password;

    /**
     * @\OpenApi\Annotations\Property(
     *     title="password",
     *     description="User confirm password",
     *     format="string",
     *     example="somepassword"
     * )
     * @var string
     */
    public $c_password;
}
