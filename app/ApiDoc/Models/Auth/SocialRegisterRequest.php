<?php

/**
 * @\OpenApi\Annotations\Schema(
 *     description="Social Register Request",
 *     type="object",
 *     title="Social Register Request"
 * )
 */

class SocialRegisterRequest
{
    /**
     * @\OpenApi\Annotations\Property(
     *     title="access_token",
     *     description="Access token from social network",
     *     format="string",
     *     example="eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsIm..."
     * )
     * @var string
     */
    public $access_token;

    /**
     * @\OpenApi\Annotations\Property(
     *     title="role",
     *     description="User role, designer or installer",
     *     format="string",
     *     example="designer"
     * )
     * @var string
     */
    public $role;

}
