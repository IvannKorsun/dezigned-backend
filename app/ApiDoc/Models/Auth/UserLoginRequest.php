<?php

/**
 * @\OpenApi\Annotations\Schema(
 *     description="User login request.",
 *     type="object",
 *     title="User login request"
 * )
*/

class UserLoginRequest
{
    /**
     * @\OpenApi\Annotations\Property(
     *     title="login",
     *     description="User name or email",
     *     format="string",
     *     example="mail@example.com"
     * )
     * @var string
    */
    public $login;

    /**
     * @\OpenApi\Annotations\Property(
     *     title="password",
     *     description="User password",
     *     format="string",
     *     example="somepassword"
     * )
     * @var string
    */
    public $password;

}
