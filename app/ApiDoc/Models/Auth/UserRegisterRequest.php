<?php

/**
 * @\OpenApi\Annotations\Schema(
 *     description="User register by form fields.",
 *     type="object",
 *     title="User register request"
 * )
 */

class UserRegisterRequest
{
    /**
     * @\OpenApi\Annotations\Property(
     *     title="name",
     *     description="User name",
     *     format="string",
     *     example="name"
     * )
     * @var string
     */
    public $name;

    /**
     * @\OpenApi\Annotations\Property(
     *     title="first name",
     *     description="User first name",
     *     format="string",
     *     example="Name"
     * )
     * @var string
     */
    public $first_name;


    /**
     * @\OpenApi\Annotations\Property(
     *     title="last name",
     *     description="User last name",
     *     format="string",
     *     example="Name"
     * )
     * @var string
     */
    public $last_name;

    /**
     * @\OpenApi\Annotations\Property(
     *     title="owner name",
     *     description="User owner name",
     *     format="string",
     *     example="+18887779999"
     * )
     * @var string
     */
    public $phone_number;

    /**
     * @\OpenApi\Annotations\Property(
     *     title="email",
     *     description="User email",
     *     format="string",
     *     example="mail@example.com"
     * )
     * @var string
     */
    public $email;

}
