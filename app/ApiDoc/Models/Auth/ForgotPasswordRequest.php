<?php

/**
 * @\OpenApi\Annotations\Schema(
 *     description="User forgot password request.",
 *     type="object",
 *     title="User forgot password request"
 * )
 */

class ForgotPasswordRequest
{
    /**
     * @\OpenApi\Annotations\Property(
     *     title="identifier",
     *     description="User name or email",
     *     format="string",
     *     example="mail@example.com or name"
     * )
     * @var string
     */
    public $identifier;

}
