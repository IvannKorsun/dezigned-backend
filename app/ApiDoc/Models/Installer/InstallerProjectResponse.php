<?php

/**
 * @\OpenApi\Annotations\Schema(
 *     description="Project repsponse",
 *     type="object",
 *     title="Project response example"
 * )
 */

class InstallerProjectResponse
{
    /**
     * @\OpenApi\Annotations\Property(
     *      title="Installer Project Id",
     *      type="integer",
     *      example="1"
     * )
     * @var integer
     */
    public $id;

    /**
     * @\OpenApi\Annotations\Property(
     *      title="Client Name",
     *      type="string",
     *      example="Brian Jackson"
     * )
     * @var string
     */
    public $client_name;

    /**
     * @\OpenApi\Annotations\Property(
     *     title="Project Location",
     *     description="Client Address",
     *     format="string",
     *     example="123 Anywhere Street, 20093"
     * )
     * @var string
     */

    public $address;
    /**
     * @\OpenApi\Annotations\Property(ref="#/components/schemas/LocationModel")
     * @var array
     */

    public $location;

    /**
     * @\OpenApi\Annotations\Property(
     *      title="Product Name",
     *      type="string",
     *      format="string",
     *      example="Product Name"
     * )
     * @var string
     */
    public $product_name;
    /**
     * @\OpenApi\Annotations\Property(
     *     title="Designer email",
     *     description="Client email",
     *     format="string",
     *     example="mail@example.com"
     * )
     * @var string
     */

    public $email;

    /**
     * @\OpenApi\Annotations\Property(
     *     title="Designer phone",
     *     description="Designer  phone",
     *     format="string",
     *     example="+18887779999"
     * )
     * @var string
     */

    public $phone_number;

    /**
     * @\OpenApi\Annotations\Property(
     *     title="Date Id",
     *     description="Installer Project Date Id ( nullable ) ",
     *     format="integer",
     *     example=15
     * )
     * @var string
     */

    /**
     * @\OpenApi\Annotations\Property(
     *     title="Dates",
     *     description="Measurements dates",
     *     type="array",
     *     @OpenApi\Annotations\Items(
     *          type="string",
     *          format="json",
     *          example="{""id"":""2"",""date_from"":""2012-12-12 12:00am"",""date_to"":""2012-12-12 12:00am"",""status"":""approved""}"
     *     )
     * )
     * @var array
     */
    public $dates = [];
}
