<?php

/**
 * @\OpenApi\Annotations\Schema(
 *     description="Project repsponse",
 *     type="object",
 *     title="Project response example"
 * )
 */

class InstallerProjectListEntryResponse
{
    /**
     * @\OpenApi\Annotations\Property(
     *      title="Installer Project Id",
     *      type="integer",
     *      example="1"
     * )
     * @var integer
     */
    public $id;

    /**
     * @\OpenApi\Annotations\Property(
     *      title="Client Name",
     *      type="string",
     *      example="Brian Jackson"
     * )
     * @var string
     */
    public $client_name;

    /**
     * @\OpenApi\Annotations\Property(
     *     title="Project Location",
     *     description="Client Address",
     *     format="string",
     *     example="123 Anywhere Street, 20093"
     * )
     * @var string
     */

    public $address;
    /**
     * @\OpenApi\Annotations\Property(ref="#/components/schemas/LocationModel")
     * @var array
     */

    public $location;

    /**
     * @\OpenApi\Annotations\Property(
     *      title="Product Name",
     *      type="string",
     *      format="string",
     *      example="Product Name"
     * )
     * @var string
     */
    public $product_name;
    /**
     * @\OpenApi\Annotations\Property(
     *     title="email",
     *     description="Client email",
     *     format="string",
     *     example="mail@example.com"
     * )
     * @var string
     */

    public $email;

    /**
     * @\OpenApi\Annotations\Property(
     *     title="owner name",
     *     description="User owner name",
     *     format="string",
     *     example="+18887779999"
     * )
     * @var string
     */

    public $phone_number;

    /**
     * @\OpenApi\Annotations\Property(
     *     title="status",
     *     description="Installer project status",
     *     format="string",
     *     example="scheduled"
     * )
     * @var string
     */

    public $status;

    /**
     * @\OpenApi\Annotations\Property(
     *     title="Date Id",
     *     description="Installer Project Date Id ( nullable ) ",
     *     format="integer",
     *     example=15
     * )
     * @var string
     */

    public $date_id;

    /**
     * @\OpenApi\Annotations\Property(
     *     title="Date",
     *     description="Installer Project Date ( nullable ) ",
     *     format="string",
     *     example="03/09/2019 @ 8:30am"
     * )
     * @var string
     */

    public $date_from;
}
