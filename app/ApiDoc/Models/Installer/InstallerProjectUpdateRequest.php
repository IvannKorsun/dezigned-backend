<?php

/**
 * @\OpenApi\Annotations\Schema(
 *     description="Update the installer project",
 *     type="object",
 *     title="Update the installer project"
 * )
 */
class InstallerProjectUpdateRequest
{
    /** @\OpenApi\Annotations\Property(
     *     title="Status",
     *     description="Status of installer project (declined,scheduled,awaited)",
     *     format="string",
     *     example="scheduled"
     * )
     * @var string
     */

    public $installer_status;

    /**
     * @\OpenApi\Annotations\Property(
     *     title="Date id",
     *     description="Id of chosen date",
     *     format="integer",
     *     example=1
     * )
     * @var integer
     */
    public $date_id;

    /** @\OpenApi\Annotations\Property(
     *     title="date status",
     *     description="Status Date",
     *     format="string",
     *     example="approved"
     * )
     * @var string
     */

    public $status;
}
