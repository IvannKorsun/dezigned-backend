<?php

/**
 * @\OpenApi\Annotations\Schema(
 *     description="Note repsponse",
 *     type="object",
 *     title="Note response example"
 * )
 */

class NoteResponse
{
    /**
     * @\OpenApi\Annotations\Property(
     *     title="id",
     *     description="Note id",
     *     format="integer",
     *     example="1"
     * )
     * @var integer
     */
    public $id;

    /**
     * @\OpenApi\Annotations\Property(
     *      type="string",
     *      format="string",
     *      example="Sample Note"
     * )
     * @var array
     */
    public $note;

    /**
     * @\OpenApi\Annotations\Property(
     *      type="string",
     *      format="string",
     *      example="10/10/2019 @ 3:15pm"
     * )
     * @var array
     */
    public $created_at;
}
