<?php

/**
 * @\OpenApi\Annotations\Schema(
 *     description="Update the installer project",
 *     type="object",
 *     title="Update the installer project"
 * )
 */
class DatesUpdateStatusRequest
{
    /** @\OpenApi\Annotations\Property(
     *     title="Status",
     *     description="Status of date",
     *     format="string",
     *     example="approved"
     * )
     * @var string
     */

    public $status;

    /**
     * @\OpenApi\Annotations\Property(
     *     title="Date id",
     *     description="Id of chosen date",
     *     format="integer",
     *     example=1
     * )
     * @var integer
     */
    public $date_id;
}
