<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAreaProductMeasureNotesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('area_product_measure_notes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('note');
            $table->unsignedBigInteger('area_product_measure_id');
            $table->timestamps();
            $table->foreign('area_product_measure_id')
                ->on('product_measures')
                ->references('id')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('area_product_measure_notes');
    }
}
