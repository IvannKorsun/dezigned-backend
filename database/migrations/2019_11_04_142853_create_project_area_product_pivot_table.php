<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectAreaProductPivotTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('project_area_product', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('project_area_id')->unsigned();
            $table->bigInteger('bc_product_id')->unsigned();
            $table->integer('qty')->unsigned();
            $table->foreign('project_area_id')
                ->references('id')
                ->on('project_areas')
                ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('project_area_product');
    }
}
