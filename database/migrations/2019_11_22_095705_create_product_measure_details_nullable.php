<?php

use Illuminate\Database\Migrations\Migration;

class CreateProductMeasureDetailsNullable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('ALTER TABLE product_measures MODIFY details TEXT NULL');
        DB::statement('ALTER TABLE product_measures MODIFY product_details TEXT NULL');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
