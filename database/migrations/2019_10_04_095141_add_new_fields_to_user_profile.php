<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNewFieldsToUserProfile extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->renameColumn('owner_name', 'first_name');
            $table->string('last_name')->after('owner_name');
            $table->string('description')->nullable()->after('remember_token');
            $table->string('avatar_url')->nullable()->after('remember_token');
            $table->string('experience', 20)->nullable()->after('remember_token');
            $table->string('education')->nullable()->after('remember_token');
            $table->string('facebook_url')->nullable()->after('remember_token');
            $table->string('instagram_url')->nullable()->after('remember_token');
            $table->string('twitter_url')->nullable()->after('remember_token');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->renameColumn('first_name','owner_name');
            $table->dropColumn(
                [
                    'last_name',
                    'description',
                    'avatar_url',
                    'experience',
                    'education',
                    'facebook_url',
                    'instagram_url',
                    'twitter_url',
                ]
            );
        });
    }
}
