<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameGalleryImagesToUserGalleryImages extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('gallery_images', function (Blueprint $table) {
            $table->dropForeign(['user_id']);
        });

        Schema::rename('gallery_images', 'user_gallery_images');

        Schema::table('user_gallery_images', function (Blueprint $table) {
            $table->foreign('user_id')
                ->on('users')
                ->references('id')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_gallery_images', function (Blueprint $table) {
            $table->dropForeign(['user_id']);
        });

        Schema::rename('user_gallery_images', 'gallery_images');

        Schema::table('gallery_images', function (Blueprint $table) {
            $table->foreign('user_id')
                ->on('users')
                ->references('id')
                ->onDelete('cascade');
        });
    }
}
