<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectAreaNotesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('project_area_notes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('note');
            $table->unsignedBigInteger('project_area_id');
            $table->timestamps();
            $table->foreign('project_area_id')
                ->on('project_areas')
                ->references('id')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('project_area_notes');
    }
}
