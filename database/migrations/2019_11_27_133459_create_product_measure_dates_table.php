<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductMeasureDatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_measure_dates', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamp('date_from');
            $table->timestamp('date_to');
            $table->enum('status', ['received', 'approved', 'rejected', 'suggested', 'saved', 'canceled']);
            $table->unsignedBigInteger('product_measure_id');
            $table->foreign('product_measure_id')
                ->on('product_measures')
                ->references('id')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_measure_dates');
    }
}
