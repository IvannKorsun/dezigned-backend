<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class ChangeLocationsColumnsToPointType extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('ALTER TABLE users CHANGE location locate JSON NULL;');

        Schema::table('users', function (Blueprint $table) {
            $table->point('location')->after('zip')->nullable();
        });

        DB::statement('UPDATE users set location = POINT(locate->>"$.lat", locate->>"$.lng")');

        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn(['locate']);
        });

        DB::statement('ALTER TABLE clients CHANGE location locate JSON NULL;');

        Schema::table('clients', function (Blueprint $table) {
            $table->point('location')->after('zip')->nullable();
        });

        DB::statement('UPDATE clients set location = POINT(locate->>"$.lat", locate->>"$.lng")');

        Schema::table('clients', function (Blueprint $table) {
            $table->dropColumn(['locate']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('ALTER TABLE users CHANGE location locate POINT NULL;');

        Schema::table('users', function (Blueprint $table) {
            $table->json('location')->after('zip')->nullable();
        });

        DB::statement('UPDATE users set location = JSON_OBJECT("lat", ST_X(locate),  "lng", ST_Y(locate)) WHERE locate IS NOT NULL;');

        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn(['locate']);
        });

        DB::statement('ALTER TABLE clients CHANGE location locate POINT NULL;');

        Schema::table('clients', function (Blueprint $table) {
            $table->json('location')->after('zip')->nullable();
        });

        DB::statement('UPDATE clients set location =  JSON_OBJECT("lat", ST_X(locate),  "lng", ST_Y(locate)) WHERE locate IS NOT NULL;');

        Schema::table('clients', function (Blueprint $table) {
            $table->dropColumn(['locate']);
        });
    }
}
