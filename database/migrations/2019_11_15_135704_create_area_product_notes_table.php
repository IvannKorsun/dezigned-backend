<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAreaProductNotesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::rename('project_area_product', 'project_area_products');

        Schema::create('project_area_product_notes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('note');
            $table->unsignedBigInteger('area_product_id');
            $table->timestamps();
            $table->foreign('area_product_id')
                ->on('project_area_products')
                ->references('id')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('project_area_product_notes');
        Schema::rename('project_area_products', 'project_area_product');
    }
}
