<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeProductsMeasureToMorphToMany extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::rename('product_measures', 'measurements');

        Schema::table('measurements', function (Blueprint $table) {
            $table->unsignedInteger('measurable_id')->after('installer_id');
            $table->string("measurable_type")->after('measurable_id');
            $table->index(['measurable_id', 'measurable_type']);
        });

        DB::statement("UPDATE measurements set measurable_id = area_product_id, measurable_type = 'App\\\Models\\\Project\\\ProjectAreaProduct'");

        Schema::table('measurements', function (Blueprint $table) {
            $table->dropForeign('product_measures_area_product_id_foreign');
            $table->dropColumn(['area_product_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::rename('measurements', 'product_measures');

        Schema::table('product_measures', function (Blueprint $table) {
            $table->unsignedBigInteger('area_product_id')->after('installer_id');
        });

        DB::statement("UPDATE product_measures set area_product_id = measurable_id where measurable_type = 'App\\\Models\\\Project\\\ProjectAreaProduct'");

        Schema::table('product_measures', function (Blueprint $table) {
            $table->foreign('area_product_id')
                ->on('project_area_products')
                ->references('id')
                ->onDelete('cascade');
            $table->dropMorphs('measurable', 'measurements_measurable_id_measurable_type_index');
        });

    }
}
