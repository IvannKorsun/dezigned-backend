<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductMeasuresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_measures', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('details');
            $table->text('product_details');
            $table->decimal('fee');
            $table->unsignedBigInteger('installer_id')->nullable();
            $table->unsignedBigInteger('area_product_id');
            $table->enum('step', [1,2,3,4,5])->default(1);
            $table->json('featured_installers')->nullable();
            $table->foreign('installer_id')
                ->on('users')
                ->references('id')
                ->onDelete('set null');
            $table->foreign('area_product_id')
                ->on('project_area_products')
                ->references('id')
                ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_measures');
    }
}
