### Dezigned API

[API documentation URL](https://api.dezigned.com/api/v1/docs)

### Prepare your dedicated server for Laravel  
##### Step 1.  
Create/purchase your dedicated server. In case of Digital Ocean it’s called Droplet, Amazon AWS call it EC2 instance etc.
##### Step 2. Install LEMP or LAMP stack.   
LAMP/LEMP stands for Linux (comes with server), web-server (Nginx for “E” and Apache for “A”), MySQL database and PHP.
- PHP version = 7.3.9
- Mysql version 8.0.16
- Nginx version 1.16.1
##### Step 3. Configure SSH access for yourself.  
You will probably deploy changes by SSHing to the machine and running commands like git pull, php artisan migrate etc. 
##### Step 4. Install/configure composer.   
[Follow the instructions from the official GetComposer.org website.](https://getcomposer.org/doc/00-intro.md#installation-linux-unix-macos)
##### Step 5. Install/configure git.   
Putting the code to the server will work by pulling it down from git repository. Probably [this official instruction will](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git) help.
##### Step 7. Configure MySQL.   
You need to create a database specific for your project, and a user to access it. You may create a separate user, granting only specific privileges.
##### Step 8. Configure Web-server. 
Prepare a specific folder for your website. Here’s an example Nginx config provided in [official Laravel documentation](https://laravel.com/docs/5.6/deployment#nginx).

#### Initial launch of the project
##### Step 1.   
Cloning repository to the server. We need to SSH into our server, navigate to the folder prepared for the project, and launch git clone command.  
##### Step 2. .env file.  
1.Copy that example file as our main .env file with this command:
```
$ cp .env.example .env
```
2.Edit that new .env file, with Linux editors like Vim:
```
$ vi .env
```

- Set Application Name `APP_NAME`.  

*This value is the name of your application. This value is used when the
framework needs to place the application's name in a notification or
any other location as required by the application or its packages.*  

- Set Application Environment `APP_ENV`.

*This value determines the "environment" your application is currently
running in. This may determine how you prefer to configure various
services the application utilizes. Set this in your ".env" file.*

- Set Application Debug Mode `APP_DEBUG`.

*When your application is in debug mode, detailed error messages with
stack traces will be shown on every error that occurs within your
application. If disabled, a simple generic error page is shown.*

- Set application log level `LOG_LEVEL` and log channel `LOG_CHANNEL`  
[Laravel Logging](https://laravel.com/docs/6.x/logging#building-log-stacks)

- Set Application URL `APP_URL`.

*This URL is used by the console to properly generate URLs when using
the Artisan command line tool. You should set this to the root of
your application so that it is used when running Artisan tasks.*

- Set Database connection config and credentials
```
DB_CONNECTION=
DB_HOST=
DB_PORT=
DB_DATABASE=
DB_USERNAME=
DB_PASSWORD=
```
[Laravel documentation for configuring database](https://laravel.com/docs/6.x/database)

- Set credentials to connect to AWS (to using AWS S3)
```
AWS_ACCESS_KEY_ID=
AWS_SECRET_ACCESS_KEY=
AWS_DEFAULT_REGION=
```
[Laravel AWS SDK package](https://github.com/aws/aws-sdk-php-laravel) 

- Set queues config 
```
QUEUE_CONNECTION=sync
QUEUE_DRIVER=sqs
SQS_KEY=${AWS_ACCESS_KEY_ID}
SQS_SECRET=${AWS_SECRET_ACCESS_KEY}
SQS_QUEUE=
SQS_REGION=
SQS_PREFIX=
```
[AWS SQS documentation. Get started.](https://aws.amazon.com/getting-started/tutorials/send-messages-distributed-applications/?nc1=h_ls)
[Laravel queues](https://laravel.com/docs/6.x/queues)
- Set queue name for sending emails 
```
EMAILS_QUEUE_NAME=
```

- Set email configuration variables (in current project uses SendGrid) [SendGrid documentation](https://sendgrid.com/docs/for-developers/)
```
MAIL_FROM=
MAIL_DRIVER=sendgrid
SENDGRID_API_KEY=
```

- Set AWS bucket names for images
```
AWS_BUCKET= for product images
AWS_AVATARS_BUCKET= for user avatars
AWS_USER_GALLERY= for users gallery
AWS_USER_CERTIFICATES=for user certificates
AWS_PROJECT_IMAGES= for project images
AWS_PROJECT_AREA_GALLERY= for project area gallery
AWS_PROJECT_AREA_COVER_IMAGES= for project area cover image
AWS_USERS_EXPERIENCE_COMP_IMG= for user experience company images
```
- Set BigCommerce API credentials 
```
BC_CLIENT_ID=
BC_CLIENT_SECRET=
BC_STORE_HASH=
BC_ACCESS_TOKEN=
```

Set redirect url after email confirmation(for set password)
```
SET_PASS_REDIRECT_URL=
```

- Set credentials for login registration via social networks (Facebook, Google, Linkedin)
``` 
FACEBOOK_CLIENT_ID=
FACEBOOK_CLIENT_SECRET=

GOOGLE_CLIENT_ID=
GOOGLE_CLIENT_SECRET=

LINKEDIN_CLIENT_ID=
LINKEDIN_CLIENT_SECRET=
LINKEDIN_REDIRECT= for login
LINKEDIN_REDIRECT_REG= for registration
```

- Set host to show base URL of API server in API documentation
```
L5_SWAGGER_CONST_HOST=http://localhost/api/v1
```


##### Step 3. Writable folders. 
Follow the instructions from the [official Laravel documentation](https://laravel.com/docs/5.6/installation#configuration)  
_Directories within the storage and the bootstrap/cache directories should be writable by your web server or Laravel will not run._
##### Step 4. Composer install. 
Run following command:
```
$ composer install
``` 
##### Step 5. Generate application key. 
Run following command:
```
$ php artisan key:generate
```
It generates a random key which is automatically added to .env file APP_KEY variable.
##### Step 6. Migrating DB schema. 
Run following command:
```
$ php artisan migrate
```
##### Step 7. Install laravel passport. 
Run following command:
```
$ php artisan passport:install
```
Copy password grant `Client ID` and `Client secret` to `CLIENT_ID` and `CLIENT_SECRET` env variables

##### Step 8. Add default user roles 
Run following command:
```
$ php artisan create:roles
```
##### Step 9. Generate api documentation
```
$ php artisan l5-swagger:generate
```
[Laravel package for generating API documentation](https://github.com/DarkaOnLine/L5-Swagger)

### Steps after pull new changes
- Update .env if needed
- Run following commands:
```
$ composer install
$ php artisan migrate
$ php artisan l5-swagger:generate 
```
- Run commands for clear cache (for `APP_ENV=production`)
```
$ php artisan cache:clear
$ php artisan config:clear
```
