<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::get('/test', function (){
//
//});

Route::group(
    [
        'middleware' => ['api'],
        'prefix' => 'v1',
        'namespace' => 'Api'
    ], function(){

    Route::group(['namespace' => 'Auth'], function (){

        Route::group(['prefix' => 'login'], function (){
            Route::post('/', 'LoginController@login');
            Route::post('/{provider}', 'SocialController@login');
        });

        Route::group(['prefix' => 'register'], function (){
            Route::post('/', 'RegisterController@register');
            Route::get('confirm/{token}', 'RegisterController@registerConfirm')->name('registerConfirm');
            Route::post('/{provider}', 'SocialController@register');
        });

        Route::group(['prefix' => 'password'], function (){
            Route::post('forgot', 'PasswordResetController@forgot');
            Route::get('find/{token}', 'PasswordResetController@find')->name('PasswordFind');
            Route::post('set', 'PasswordResetController@reset');
        });

        Route::group(['middleware' => 'auth:api'], function(){
            Route::get('logout', 'LoginController@logout');
            Route::post('refresh', 'LoginController@refreshToken');
        });
    });

    Route::get('user/profile/public/{token}', 'User\UserController@publicProfile');

    Route::group(['namespace' => 'User', 'middleware' => 'auth:api'], function (){
        Route::get('user/mission-statement', 'MissionStatementController@index');
        Route::post('user/mission-statement', 'MissionStatementController@update');
        Route::get('user/{userId}/gallery', 'GalleryController@index')->middleware(['role:member']);
        Route::get('user/{userId}/experience', 'UserExperienceController@index')->middleware(['role:member']);
        Route::apiResource('user/gallery', 'GalleryController')->only(['index', 'store', 'update', 'destroy']);
        Route::apiResource('user/experience', 'UserExperienceController')->only(['index', 'store', 'update', 'destroy']);
        Route::apiResource('user/certificates', 'UserCertificationsController')->only(['index', 'store', 'update', 'destroy']);

        Route::get('user/profile/share', 'UserController@share');
        Route::get('user', 'UserController@profile');
        Route::put('user', 'UserController@update');
        Route::post('user/change-password', 'UserController@changePassword');
    });

    Route::group(['namespace' => 'Product', 'middleware' =>['auth:api', 'role:member']], function (){
        Route::get('product/categories/tree', 'ProductController@categoriesTree');
        Route::get('product/search', 'SearchController@search');
        Route::apiResource('product', 'ProductController');
        Route::get('product-attributes', 'ProductController@getSelectableAttributes');
        Route::get('filters', 'FilterController@getFilters');
    });
    Route::group(['namespace' => 'Project', 'middleware' =>['auth:api', 'role:member']], function (){
        Route::apiResource('project/{projectId}/area/{areaId}/products/{productId}/notes', 'AreaProductNotesController')->only(['index', 'store', 'update', 'destroy']);
        Route::apiResource('project/{projectId}/area', 'ProjectAreaController')->only(['index', 'store', 'show', 'update', 'destroy']);
        Route::apiResource('project/{projectId}/area/{areaId}/images', 'ProjectAreaGalleryController')->only(['index', 'store', 'update', 'destroy']);
        Route::apiResource('project/{projectId}/area/{areaId}/notes', 'ProjectAreaNotesController')->only(['index', 'store', 'update', 'destroy']);
        Route::apiResource('project', 'ProjectController')->only(['index', 'store', 'show', 'update', 'destroy']);
        Route::apiResource('project/{projectId}/images', 'ProjectGalleryController')->only(['index', 'store', 'update', 'destroy']);

        Route::apiResource('project/{projectId}/area/{areaId}/products/{productId}/measurements/{measureId}/notes', 'AreaProductMeasureNotesController')
            ->only(['store', 'update', 'destroy']);
        Route::get('/notes/{measureId}', 'AreaProductMeasureNotesController@getNotesByMeasureId');

        Route::post('project/{projectId}/area/products', 'ProjectAreaController@addProduct');
        Route::get('project/{projectId}/area/{id}/products', 'ProjectAreaController@getProducts');
        Route::put('project/{projectId}/area/{id}/products', 'ProjectAreaController@updateProduct');
        Route::delete('project/{projectId}/area/{areaId}/products/{id}', 'ProjectAreaController@deleteProduct');

        Route::get('/project/area/products/measurements/get-installers', 'ProductMeasureController@getInstallers');
        Route::post('project/area/products/measurements', 'ProductMeasureController@addMeasure');
        Route::put('project/area/products/measurements/{measureId}', 'ProductMeasureController@updateMeasure');
    });

        Route::group(['namespace' => 'Project', 'middleware' => ['auth:api']], function (){

        Route::get('project/{projectId}/area/{areaId}/products/{productId}/measurements/{measureId}/notes', 'AreaProductMeasureNotesController@index');


        Route::get('project/{projectId}/area/{areaId}/products/{productId}/measurements/{measureId}/dates', 'ProductMeasureDatesController@index');
        Route::post('project/{projectId}/area/{areaId}/products/{productId}/measurements/{measureId}/dates', 'ProductMeasureDatesController@store')->middleware(['role:member']);

        Route::put('/dates/{id}', 'ProductMeasureDatesController@update');
        Route::get('/dates/{measureId}', 'ProductMeasureDatesController@getDatesByMeasureId');
    });

    Route::group(['namespace' => 'Client', 'middleware' =>['auth:api', 'role:member']], function (){
        Route::apiResource('client', 'ClientController')->only(['index', 'store', 'show', 'update', 'destroy']);
    });

    Route::group(['namespace' => 'Installer', 'middleware' => [ 'auth:api', 'role:installer']], function (){
        Route::apiResource('installer-projects', 'InstallerProjectController');
    });

});
